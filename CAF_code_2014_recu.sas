/******************************************************************/
/*PROJET   : MDR SCORE DATAMINING                                 */
/*NOMPGM   : calcul_score_modele.sas                             */
/*AUTEUR   : xxxx									              */
/*DATECRE  : 09/2014                                              */
/*DATEMAJ  : 													  */
/*OBJET    : Requ�te pour calcul score datamining DE : mod�le septembre 2014             */
/*		   : lanc� par lance_calcul_score   					  */
/*MOTCLE   :                                                      */
/******************************************************************/

/**************************************************************************************************/
/*****						Requ�te de scoring              							***********/
/*****				       du mod�le Datamining DE 2014					            	***********/
/*****                                                									***********/
/*****                                                									***********/
/**************************************************************************************************/

%let limit_test = (matricul ne .);


option obs=max compress = no ;
options nomprint nosymbolgen ; 
%global m1 mvDTREFFRE /* m1 = dernier mois trait�*/ ; 

%macro DM_DE;


/* INITIALISATION DES 18 MOIS A TRAITER */
%macro mp_18mmaa ; 
/* ATTENTION LES MOIS VONT DU PLUS RECENT AU PLUS ANCIEN */
	data _null_ ; 
			set basenat.xdref (where = (NOMTAB = 'FREMM')) ; 
			/******************* POUR FORCER LE MOIS DE REFERENCE  ********************/ 
			/*DFREF = MDY(8,1,2014) ;*/
			/**************************************************************************/
			Call symput("mvDTFREMM", DFREF)  ; 
	run ; 
	data _null_ ; 
		set basefrm.datall ; 
			call symput ("mvDTBASEFRM", DTBASE) ; 	
		run ;	
	data _null_   ; 
	%do i = 1 %to  18  ;
		nbMOIS = /*-18 + */ &i. ;  
		DDREF&i = INTnx ("MONTH", &mvDTFREMM., - nbMois +1 , "beginning") ; 
		mmaa&i. = 
compress(put(month(DDREF&i.),z2.)!!SUBSTR(put(year(DDREF&i.),z4.),3,2)) ; 
		%global m&i. ; 
		call symput( "m&i.", mmaa&i. ) ; 
	%end ; 
	run ; 
%mend ; 
%mp_18mmaa  ; 


/*Modification PAC nov2015 : ajout de conditions sur l'existence des variables PAC*/
		proc contents DATA =  basestat.fre&m1. noprint out = WORK.temp ;
		run ;
			proc sql noprint;
				select count(*) into: mv_pres_MTOTFOY
							from temp
							where name="MTOTFOY1";
			quit;
			proc sql noprint;
				select count(*) into: mv_pres_DTINCOAC
							from temp
							where name="DTINCOAC";
			quit;
			proc sql noprint;
				select count(*) into: mv_pres_PPAVERS
							from temp
							where name="PPAVERS";
			quit;
			proc sql noprint;
				select count(*) into: mv_pres_RSAACT
							from temp
							where name="RSAACT";
			quit;
			proc sql noprint;
				select count(*) into: mv_pres_RESTRRSA
							from temp
							where name="RESTRRSA";
			quit;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* * * * * * * * * * * * * * * * * * */
/*						Sommaire du programme  (rappel des titres dans le 
pg)					                                       */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* * * * * * * * * * * * * * * * * * */

/*Partie 1: Constitution de la base d information datamining (le programme est 
ordonn� par cat�gorie d'information)
		a)Initialisation de la table pour alimentation au fur et � mesure par 
jointures successives
		b)Cat�gorie d'indicateurs : Historique FRE indus 
ant�rieurs,+adresse,activit�,abattement et neutram (entrep�ts CAF seulement)
		c)Cat�gorie d'indicateurs : Historique gestion dossier sur conseil m�tier 
(FG...) (entrep�ts CAF seulement)
		d)Cat�gorie d'indicateurs : Historique de contact (entrep�ts CAF seulement)
		e)Cat�gorie d'indicateurs : DIVERS SUR BASE INFOCENRE (entrep�ts CAF 
seulement)
		f)Cat�gorie d'indicateurs : HISTORIQUE SITUATION CONTROL et VERIF (donn�es 
disponibles dans le ALLNAT au CSN)
		h)Cat�gorie d'indicateurs : DONNEES SDP
		i)Cat�gorie d'indicateurs : SITUATION DOSSIER FRE SUR LA DATE DE REFERENCE 
(donn�es disponibles dans le ALLNAT au CSN)
*/

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* * * * * * * * * * * * * * * * * * */
/*																												   */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* * * * * * * * * * * * * * * * * * */


/**********************************************************************************************************************/
/**********************************************************************************************************************/
/**********      Partie 1: Constitution de la base d information datamining     
***************************************/
/**********************************************************************************************************************/
/**********************************************************************************************************************/

/**********************************************************************************************************************/
/* a)Cr�ation de la table pour alimentation au fur et � mesure par jointures 
successives                              */
/**********************************************************************************************************************/

%macro mpINITAB ; 
	data work.final ; 
	set Basestat.fre&m1.(in=b keep = matricul mtpfvers catben sitdos dtreffre
						 where = ( mtpfvers>0 and sitdos ne 'R'  and catben ne "99" and &limit_test)); 
	by matricul;
			Call symputx("mvDTREFFRE", dtreffre)  ;
	run ; 
%mend ; 
%mpINITAB ; 

/**********************************************************************************************************************/
/* b)Cat�gorie d'indicateurs : Historique  FRE (ALLCAF)  : 	(entrep�ts CAF 
seulement)								  */
/*                         indus ant�rieurs + adresse + activit� + abattement 
et neutralisation                       */
/**********************************************************************************************************************/

/*MODIF DSER oct 2014 : r�int�gration variable SITFAM dans la macro HISTALCAF*/
%macro mpHISTALCAF ; 
	%do i= 1 %to 18 ; 
		data _null_ ; /* contr�le existence des tables */ 
		if  (exist("Basestat.fre&&m&i.",'data') or exist("Basestat.fre&&m&i.",'view'))
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
		run ; 
		%if &topexist. = O %then %do ; 
		Data work.ALLCAF&&m&i. ( drop = LILI4ADR LILI6ADR /**/  CATBEN);
		  merge work.final  (in = a keep =  MATRICUL ) 
				Basestat.fre&&m&i. (in = b keep = MATRICUL SINDTOT SINDFNAL  
SINDFNH SITFAM ACTCONJ ACTRESPD   
				LILI4ADR LILI6ADR /**/ CODEPOSD ABANEURE ABANEUCO MTLOYREM  
				CATBEN 	where = (CATBEN ne "99" ) );
				by MATRICUL; 
				if a = 1 and b = 1; 
		  rename 
		  	SINDTOT = SINDTOT&&m&i.  	SINDFNAL = SINDFNAL&&m&i.	
		  	SINDFNH = SINDFNH&&m&i.		SITFAM =FAM&&m&i. 			ACTCONJ=ACJ&&m&i. 
				ACTRESPD= ACR&&m&i. 		CODEPOSD=POST&&m&i. 		
			 	ABANEURE=ABNE&&m&i. 		ABANEUCO=ABNO&&m&i.	MTLOYREM=MTLOYREM&&m&i.; 
				ADRESS&&m&i. = COMPBL(LILI4ADR!!" "!!LILI6ADR ) ; /*modif ao�t 2016*/
		run ; 
		/* mise � jour de la table final avec donn�es existantes */	
		data work.final  ; 
			merge work.final ( in = a ) 
				  work.ALLCAF&&m&i. ; 
			by matricul ; 
			if a = 1 ; 
			/*if (&mm.="06" and &aa.="10") then ADRESS_dernier=ADRESS&mm.&aa.;*/
		run ; 
		
		/*Nettoyage de la work apr�s jointure*/
		proc datasets lib=work nolist; delete ALLCAF&&m&i. ; quit; run;
		%end ; 
	%end ; 
/*Cr�ation des variables explicatives de la cat�gorie historique indus 
ant�rieurs */ 
		data work.final /*(drop=ADR: SINDTOT: SINDFNAL: SINDFNH:  
							  FAM: ACJ: ACR: POST: ABNE: ABNO: )*/;
			set work.final;
			/*Vecteur des donn�es historiques du plus r�cents au plus anciens*/
				array INDTOT  %do i = 1 %to 18; SINDTOT&&m&i.  	%end ; ;
				array INDFNAL %do i = 1 %to 18; SINDFNAL&&m&i. 	%end ; ; 
				array INDFNH  %do i = 1 %to 18; SINDFNH&&m&i.  	%end ; ; 
				array ADR     %do i = 1 %to 18; ADRESS&&m&i.   	%end ; ;
				array fam 	  %do i = 1 %to 18; fam&&m&i.		%end ; ; 
				array post    %do i = 1 %to 18; post&&m&i.		%end ; ;
				array actcon  %do i = 1 %to 18; ACJ&&m&i. 		%end ; ;
				array actresp %do i = 1 %to 18; ACR&&m&i. 		%end ; ;
				array abno    %do i = 1 %to 18; ABNO&&m&i.		%end ; ;
				array abne    %do i = 1 %to 18; ABNE&&m&i. 		%end ; ;
				array mtloy   %do i = 1 %to 18; MTLOYREM&&m&i.	%end ; ;

			/*Initialisation des indicateurs*/
				attrib INDTOT18 length=3. label="NB INDUS TOTAL SUR 18 MOIS";
				attrib INDFNAL18 length=3. label="NB INDUS FNAL SUR 18 MOIS";
				attrib INDFNH18 length=3. label="NB INDUS FNH SUR 18 MOIS";
				attrib INDAL18 length=3. label="NB INDUS FNAL OU FNH SUR 18 MOIS";
				attrib NADRESS18 length=3. label="NB CHGT ADRESSE 18 MOIS";
				attrib MOIINDTOT length=3. label="NB MOIS DEPUIS DERNIER INDU";
				attrib MOIINDFNPF length=3. label="NB MOIS DEPUIS DERNIER INDU FNPF";
				attrib MOIINDFNAL length=3. label="NB MOIS DEPUIS DERNIER INDU AL";
				attrib MOIINDFNH length=3. label="NB MOIS DEPUIS DERNIER INDU FNH";
				attrib MOIINDAL length=3. label="NB MOIS DEPUIS DERNIER INDU FNH OU FNAL";
				attrib MOIADRESS length=3. label="NB MOIS DEPUIS CHGT ADRESSE";
				attrib MOIMODSIT length=3. label=
"NB MOIS  DEPUIS UNE MODIFICATION DE SITUATION FAMILLE";
				attrib NMODSIT12 length=3. label=
"NB MODIFICATIONS DE SITUATION FAMILLE SUR 12 MOIS";
				attrib TOPSIT12 length=3. label=
"AU MOINS UNE MODIFICATION DE SITUATION FAMILLE SUR 12 MOIS";
				attrib MOIMODACTR length=3. label=
"NB MOIS DEPUIS MODIF CODE ACTIVITE DU RESPDOS";
				attrib MOIMODACTC length=3. label=
"NB MOIS DEPUIS MODIF CODE ACTIVITE DU CONJOINT";
				attrib MOIMODACT length=3. label=
"NB MOIS DEPUIS MODIF CODE ACTIVITE DU RESPDOS OU DU CONJOINT";
				attrib NMODACT12R length=3. label=
"NB DE MODIFICATION ACTIVIT� SUR 12 MOIS (RESP)";
				attrib NMODACT12C length=3. label=
"NB DE MODIFICATION ACTIVIT� SUR 12 MOIS (CONJ)";
				attrib NMODACT12 length=3. label=
"NB DE MODIFICATION ACTIVIT� SUR 12 MOIS (RESP+CONJ)";
				attrib MACT12R length=3. label="NB MOIS EN ACTIVIT� SUR 12 MOIS (RESPDOS)";
				attrib MACT12C length=3. label="NB MOIS EN ACTIVIT� SUR 12 MOIS (CONJ)";
				attrib MACT12 length=3. label=
"NB MOIS EN ACTIVIT� SUR 12 MOIS (RESP et CONJ)";
				attrib MOIMODNEUC length=3. label=
"NB MOIS DEPUIS MODIF CODE ABATTEMENT OU NEUTRAL (HORS AAH) DU CONJ";
				attrib MOIMODNEU length=3. label=
"NB MOIS DEPUIS MODIF CODE ABATTEMENT OU NEUTRAL (HORS AAH) RESPDOS+CONJ";
				attrib MODNEUR12 length=3. label=
"NB DE MODIF CODE AB OU NEUTRAL (HORS AAH) DU RESPDOS SUR 12 MOIS";
				attrib MODNEUC12 length=3. label=
"NB DE MODIF CODE AB OU NEUTRAL (HORS AAH) DU CONJ SUR 12 MOIS";
				attrib MODNEU12  length=3. label=
"NB DE MODIF CODE AB OU NEUTRAL (HORS AAH) DU RESPDOS OU DU CONJ SUR 12 MOIS";
				attrib NMODAD12 length=3. label="NB MODIF CODEPOSTAL SUR 12 MOIS";
				attrib TOPAD12 length=3. label="TOP MODIFICATION CODE POSTAL SUR 12 MOIS";
				attrib NMODLOY6 length=3. label="TOP MODIFICATION CODE POSTAL SUR 6 MOIS";
				attrib NMODLOY12 length=3. label="TOP MODIFICATION CODE POSTAL SUR 12 MOIS";
				attrib NMODLOY18 length=3. label="TOP MODIFICATION CODE POSTAL SUR 18 MOIS";
			/*Code de cr�ation des indicateurs*/
				INDTOT18=0; do i=1 to 18; if (INDTOT(i) ne .  and INDTOT(i)>0) then do; 
INDTOT18=INDTOT18+INDTOT(i); end;end;
				INDFNAL18=0; do i=1 to 18; if (INDFNAL(i) ne .  and INDFNAL(i)>0) then do; 
INDFNAL18=INDFNAL18+INDFNAL(i); end;end;
				INDFNH18=0; do i=1 to 18; if (INDFNH(i) ne .  and INDFNH(i)>0) then do; 
INDFNH18=INDFNH18+INDFNH(i); end;end;
				INDAL18=INDFNH18+INDFNAL18;
				NADRESS18=0; 
				ADRESS_dernier=adress&m1.; 
				do i=1 to 18; 
					if (ADR(i) ne " " and ADR(i) ne ADRESS_dernier)  then do; 
						NADRESS18=NADRESS18+1; 
						ADRESDEP=ADR(i);  
						ADRESS_dernier=ADR(i); /*modif ao�t 2016 - correction adresse*/
					end;
				end;
				MOIINDTOT=0; do i=1 to 18; if (INDTOT(i) ne .  and INDTOT(i)>0) then do; 
MOIINDTOT=i; i=18; end;end;
				MOIINDFNPF=0; do i=1 to 18; if (INDFNPF(i) ne .  and INDFNPF(i)>0) then do; 
MOIINDFNPF=i; i=18; end;end;
				MOIINDFNAL=0; do i=1 to 18; if (INDFNAL(i) ne .  and INDFNAL(i)>0) then do; 
MOIINDFNAL=i; i=18; end;end;
				MOIINDFNH=0; do i=1 to 18; if (INDFNH(i) ne .  and INDFNH(i)>0) then do; 
MOIINDFNH=i; i=18; end;end;
				MOIINDAL=0; do i=1 to 18; if ( (INDFNAL(i) ne .  and INDFNAL(i)>0) or 
(INDFNH(i) ne .  and INDFNH(i)>0) ) then do; MOIINDAL=i; i=18; end;end;
				MOIADRESS=0; ADRESS_dernier=adress&m1.; 
				do i=1 to 18; 
					if  (ADR(i) ne " " and ADR(i) ne ADRESS_dernier) then do; 
						MOIADRESS=i; 
						i=18; 
					end;
				end;
				/*Situation familiale*/
				MOIMODSIT=0; histosit=fam&m1.; do i=1 to 18; if fam(i) ne histosit and 
fam(i) ne " " then do; MOIMODSIT=i-1; i=18; end;end;
				histosit=fam&m1.; NMODSIT12=0; do i=1 to 12; if fam(i) ne histosit and 
fam(i) ne " " then do; histosit=fam(i); NMODSIT12=NMODSIT12+1; end;end;
				TOPSIT12=NMODSIT12>0;
				/*loyers*/

				/*MODIF DSER octobre 2014 : MTLOYREM&m1. au lieu de MTLOY&m1.*/

				histoloy=mtloyrem&m1.; NMODloy6=0; do i=1 to 6; if mtloy(i) ne histoloy and 
mtloy(i) ne " " then do; histoloy=mtloy(i); NMODLOY6=NMODLOY6+1; end;end;
				histoloy=mtloyrem&m1.; NMODloy12=0; do i=1 to 12; if mtloy(i) ne histoloy and 
mtloy(i) ne " " then do; histoloy=mtloy(i); NMODLOY12=NMODLOY12+1; end;end;
				histoloy=mtloyrem&m1.; NMODloy18=0; do i=1 to 18; if mtloy(i) ne histoloy and 
mtloy(i) ne " " then do; histoloy=mtloy(i); NMODLOY18=NMODLOY18+1; end;end;
				/*Activit�*/
				histoactr=acr&m1.;  do i=1 to 18; if ACTRESP(i) ne histoactr and actresp(i) 
ne " " then do;  MOIMODACTR=i-1; i=18; end;end;
				histoactc=acj&m1.; do i=1 to 18; if ACTCON(i) ne histoactc and actcon(i) ne 
" " then do; MOIMODACTC=i-1; i=18; end;end;
				MOIMODACT=min(MOIMODACTC,MOIMODACTR); if MOIMODACT=. then MOIMODACT=0;
				histoactr=acr&m1.; NMODACT12R=0;  do i=1 to 12; if ACTRESP(i) ne histoactr 
and actresp(i) ne " " then do; histoactr=ACTRESP(i); NMODACT12R=NMODACT12R+1; 
end;end;
				histoactc=acj&m1.; NMODACT12C=0;  do i=1 to 12; if  ACTCON(i) ne histoactc 
and  actcon(i) ne " " then do; histoactc=ACTCON(i); NMODACT12C=NMODACT12C+1; 
end;end;
				NMODACT12=sum(NMODACT12R, NMODACT12C);
				MACT12R=0; do i=1 to 12; 
				if ACTRESP(i) in (/* Activit� normale */'ACR', 'AMA', 'AMT', 'APP', 'CCV', 
'CES', 'CIA', 'CIS', 'CGP', 'CSA', 'DNL', 'INT', 'MAL', 'MAT', 'MOA', 'PIL', 
'SAL', 'SFP', 'TSA', 'VRP' ,'ETS',
						  /*Activit� en milieu prot�g� avec AAH */ 'AAP', 'CAT' , 'RAC',
						  /* entrepreneur, travailleur ind�pendant */'ETI', 'CJT', 'EXP', 'MAR', 
'GSA' , 'EXS')  then do; MACT12R=MACT12R+1; end;end;
				MACT12C=0; do i=1 to 12; 
				if ACTCON(i) in  (/* Activit� normale */'ACR', 'AMA', 'AMT', 'APP', 'CCV', 
'CES', 'CIA', 'CIS', 'CGP', 'CSA', 'DNL', 'INT', 'MAL', 'MAT', 'MOA', 'PIL', 
'SAL', 'SFP', 'TSA', 'VRP' ,'ETS',
						  /*Activit� en milieu prot�g� avec AAH */ 'AAP', 'CAT' , 'RAC',
					      /* entrepreneur, travailleur ind�pendant */'ETI', 'CJT', 'EXP', 'MAR'
, 'GSA' , 'EXS')  then do; MACT12C=MACT12C+1; end;end;
				MACT12=MACT12R+MACT12C;
				
				/*code postal*/
				histoad=post&m1.; NMODAD12=0; do i=1 to 12; if post(i) ne histoad and 
post(i) ne . then do; histoad=post(i); NMODAD12=NMODAD12+1; end;end;
				topad12=nmodad12>0;	
		run;
	
%mend ; 
%mpHISTALCAF ; 


%macro mpFGHISTO ; 
	%do i= 1 %to 18 ; 
		data _null_ ; /* contr�le existence des tables */ 
			if  (exist("basefrm.FAI&&m&i.",'data') or exist("basefrm.FAI&&m&i.",'view')) 
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
		run ; 
		%if &topexist. = O %then %do ; 
			data _null_ ; /* r�cup de mois et de l'ann�e */
				call symput("mvMOIS", substr("&&m&i.",1,2)) ; 
				call symput("mvANNEE", substr("&&m&i.",3,2)) ; 
			run ; 

			Data work.FAI&&m&i. ;
			  merge work.final  (in = a keep =  MATRICUL ) 
					basefrm.FAI&&m&i. (in = b keep = MATRICUL DTSESLIQ FGELE ETATLIQ ETASSLIQ 
ORILIQ RGMOUFG );
					by MATRICUL; 
					if a = 1 and b = 1 ; 

				ATTRIB  FORINET&&m&i.	format = 8.		informat = 8.		length = 8	Label = 
					"NOMBRE DE FG D'ORIGINE INTERNET &&m&i." ;
				ATTRIB  FORIREC&&m&i.	format = 8.		informat = 8.		length = 8	Label = 
					"NOMBRE DE FG D'ORIGINE PGME RECUP (CDASP) &&m&i." ;

			if ETATLIQ in ("01" "03") and ETASSLIQ = "05" 
			     and MDY(&mvMOIS.,01,&mvANNEE.) <= DTSESLIQ <= INTNX('MONTH',MDY(&mvMOIS.,01,&mvANNEE.),0, "end")
			  then do ; 
			  	FGTOT&&m&i. = 1 ; 
				if FGELE = "SITFAM" then SITFAM&&m&i = 1 ; 
				if FGELE = "RESANN" then RESANN&&m&i = 1 ; 
				if FGELE in ("RESTRIAPI" "RESTRIRMI" "RESTRIRSA" "RESTRIAAH" /*Modification PAC nov2015 - Ajout du Fg "Restrippa" */ "RESTRIPPA") then RESTR&&m&i = 1 ; /*Ajout f�v.2014*/
				if FGELE = "SITPRO" then SITPRO&&m&i = 1 ; 

				if ORILIQ in ("NET") then  FORINET&&m&i. = 1 ; 
				if ORILIQ in ("REC") then  FORIREC&&m&i. = 1 ;
			  end ; 

				/* INDICATEUR QUI NE SERVIRA QUE SUR LES 6 DERNIERS MOIS */
				ATTRIB  FGRESALL&&m&i.	format = 8.		informat = 8.		length = 8	Label = 
					"NOMBRE DE FG RESANN D'ORIGINE ALLOCATAIRE &&m&i." ;
					if FGELE = 'RESANN' and ETATLIQ in ("01" "03") and ETASSLIQ = "05" and 
							ORILIQ = "ALL" then	FGRESALL&&m&i. = 1 ; 
			  run ; 

			  proc means data =work.FAI&&m&i. noprint nway ; 
			  	var SITFAM&&m&i.	RESANN&&m&i. RESTR&&m&i. SITPRO&&m&i. FGRESALL&&m&i. 
					FORINET&&m&i. FORIREC&&m&i.; 
				class  MATRICUL ; 
				output out = work.NBFG&&m&i. (drop = _type_ _freq_ )  sum() = ; 
			  run ; 

			/* mise � jour de la table final avec donn�es existantes */	
			data work.final  ; 
				merge work.final ( in = a ) 
					  work.NBFG&&m&i. ; 
				by matricul ; 
				if a = 1 ; 
			run ; 
		
			/*Nettoyage de la work apr�s jointure*/
			proc datasets lib=work nolist; delete NBFG&&m&i.  ; quit; run;
			proc datasets lib=work nolist; delete FAI&&m&i. temp; quit; run;
		%end ; 
	%end ; 
				/*Cr�ation des variables explicatives de la cat�gorie historique gestion 
dossier */ 
	data work.final (drop= i sitfam: fori: fgresall: sitpro: restr: resann: );
		set work.final;
						 

		/*Vecteur des donn�es historiques*/
			array FAISANN 		%do i = 1 %to 18; RESANN&&m&i. 		%end ; ; 
			array FAIRESTR 		%do i = 1 %to 18; RESTR&&m&i. 		%end ; ; 
			array FAIPRO 		%do i = 1 %to 18; SITPRO&&m&i. 		%end ; ; 
			array FGRESALL		%do i = 1 %to 18; FGRESALL&&m&i. %end ; ; 

			array FAIORINET		%do i = 1 %to 18; FORINET&&m&i. %end ; ; 
			array FAIORIREC		%do i = 1 %to 18; FORIREC&&m&i. %end ; ; 

		/*Initialisation des indicateurs*/
			attrib FGORINET12 length=3. label="NB FG D'ORIGINE INTERNET SUR 12 MOIS";
			attrib FGORIREC18 length=3. label="NB FG D'ORIGINE PGME RECUP (CDASP) SUR 18 MOIS";
			attrib FGRESTRI18 length=3. label="NB FG RESTRI (HORS ANNULES) SUR 18 MOIS";

			attrib MOIFGRESAN length=3. label="NB MOIS DEPUIS LE DERNIER FG RESANN (HORS ANNULES)";


		/*Code de cr�ation des indicateurs*/
			FGORINET12=0; 	do i=1 to 12; if (FAIORINET(i) ne .  and FAIORINET(i)>0) then do;
					FGORINET12=FGORINET12+FAIORINET(i); 
					end;end;
			FGORIREC18=0; 	do i=1 to 18; if (FAIORIREC(i) ne .  and FAIORIREC(i)>0) then do; 
					FGORIREC18=FGORIREC18+FAIORIREC(i); 
					end;end;
			FGRESTRI18=0; do i=1 to 18; if (FAIRESTR(i) ne .  and FAIRESTR(i)>0) then do; 
					FGRESTRI18=FGRESTRI18+FAIRESTR(i); 
					end;end;
			MOIFGRESAN=0; do i=1 to 18; if (FAISANN(i) ne .  and FAISANN(i)>0) then do; 
					MOIFGRESAN=i; i=18; 
					end;end;

		run;
%mend ;
%mpFGHISTO ;



/**********************************************************************************************************************/
/* d1)Cat�gorie d'indicateurs : Historique de contact (entrep�ts CAF seulement)							              */
/**********************************************************************************************************************/
%macro mp_HISTGCA ; 
	%do i= 1 %to 18 ; 
		data _null_ ; /* contr�le existence des tables */ 
			if  (exist("gra.gca&&m&i.",'data') or exist("gra.gca&&m&i.",'view')) 
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
		run ; 
	%if &topexist. = O %then %do ; 
			proc sort data = GRA.GCA&&m&i. out = work.GCA&&m&i. ; 
				by matricul ; 
			run ; 
			Data work.gca&&m&i. ;
					merge work.final  (in = a keep =  MATRICUL ) 
						  work.GCA&&m&i. (in = b keep = MATRICUL NATCONTA  );
					by MATRICUL; 
					if a = 1 and b = 1 ; 
				ATTRIB 
				VU&&m&i. format = 8. informat = 8. length = 8 
						LABEL = "NOMBRE DE CONTACTS PAR NATURE DE CONTACT ACCUEIL OU VISITE SUR &&m&i." 
				TEL&&m&i. format = 8. informat = 8. length = 8 
						LABEL = "NOMBRE DE CONTACTS PAR NATURE DE CONTACT TELEPHONE SUR &&m&i." 
				AUT&&m&i. format = 8. informat = 8. length = 8 
						LABEL = "NOMBRE DE CONTACTS PAR NATURE DE CONTACT AUTRES SUR &&m&i."  ; 
	
				if natconta in ("A" "I" "N" "P" "V" ) then VU&&m&i. = 1 ; 
				if natconta in ("D" "R" ) then TEL&&m&i. = 1 ; 
				if natconta in ("B" "T" ) then AUT&&m&i. = 1 ; 
			run ; 
			proc means data = work.gca&&m&i. noprint nway; 
				VAR VU&&m&i. TEL&&m&i. AUT&&m&i. ; 
				class matricul ; 
				output out = work.CONTA&&m&i. (drop= _type_ _freq_) sum() = ; 
			run ; 		  
			/* mise � jour de la table final avec donn�es existantes */	
			data work.final  ; 
				merge work.final ( in = a ) 
					  work.CONTA&&m&i. ; 
				by matricul ; 
				if a = 1 ; 
			run ; 
		%end ; 
		/*Nettoyage de la work apr�s jointure*/
		proc datasets lib=work nolist; delete GCA&&m&i. CONTA&&m&i.; quit; run;
	%end ; 
	/*Cr�ation des variables explicatives de la CATEGORIE DONNEES CONTACT */ 
	data work.final (drop= VU: TEL: AUT:  );
		set work.final;
	/*Vecteur des donn�es historiques*/
		array CONVU 	%do i = 1 %to 18; 	VU&&m&i. 	%end ; ; 
		array CONTEL	%do i = 1 %to 18;	TEL&&m&i.	%end ; ;
		array CONAUT 	%do i = 1 %to 18;	AUT&&m&i. 	%end ; ;
	/*Initialisation des indicateurs*/
		attrib NBONTATEL18 length=3. label="NOMBRE DE CONTACT TEL EN 18 MOIS";
		attrib NBONTAAUT18 length=3. label="NOMBRE DE CONTACT AUT EN 18 MOIS";
		attrib MOIONTAVU length=3. label="NB MOIS DEPUIS CONTACT ACCUEIL";
		attrib MOIONTATEL length=3. label="NB MOIS DEPUIS CONTACT TEL";
		attrib MOIONTAAUT length=3. label="NB MOIS DEPUIS CONTACT AUT";
	/*Code de cr�ation des indicateurs*/
		NBONTATEL18=0; do i=1 to 18; if (CONTEL(i) ne .  and CONTEL(i)>0) then do; 
			NBONTATEL18=NBONTATEL18+CONTEL(i); end;end;
		NBONTAAUT18=0; do i=1 to 18; if (CONAUT(i) ne .  and CONAUT(i)>0) then do; 
			NBONTAAUT18=NBONTAAUT18+CONAUT(i); end;end;
		MOIONTAVU=0; do i=1 to 18; if (CONVU(i) ne .  and CONVU(i)>0) then do; 
			MOIONTAVU=i; i=18; end;end;
		MOIONTATEL=0; do i=1 to 18; if (CONTEL(i) ne .  and CONTEL(i)>0) then do; 
			MOIONTATEL=i; i=18; end;end;
		MOIONTAAUT=0; do i=1 to 18; if (CONAUT(i) ne .  and CONAUT(i)>0) then do; 
			MOIONTAAUT=i; i=18; end;end;		
	run;
%mend ; 
%mp_HISTGCA ;


/**********************************************************************************************************************/
/* d2) Historique des connexions Web Caf.fr																	              */
/**********************************************************************************************************************/


%macro mp_HISTWEB ; /*Modification octobre 2016 --> fusion donn�es Web et bornes */
	%do i= 1 %to 18 ; 
			data _null_ ; /* contr�le existence des tables */ 
				if  (exist("gra.web&&m&i.",'data') or exist("gra.web&&m&i.",'view'))
				then  call symput ("topexist_web", "O") ; 	
				else  call symput ("topexist_web", "N") ; 
			run ; 

			data _null_ ; /* contr�le existence des tables BIW*/ 
				if  (exist("gra.biw&&m&i.",'data') or exist("gra.biw&&m&i.",'view'))
				then  call symput ("topexist_biw", "O") ; 	
				else  call symput ("topexist_biw", "N") ; 
			run ; 

				%if &topexist_web. = O and &topexist_biw. = O %then %do ; 

					proc sort data = GRA.WEB&&m&i. out = work.WEB&&m&i. nodupkey; 
							by matricul dtcontwb; 
					run ; 
					proc sort data = GRA.BIW&&m&i. out = work.BIW&&m&i. nodupkey; 
							by matricul dtcontbo; 
					run ; 


					Data work.WEB&&m&i. ;
								merge work.final  (in = a keep =  MATRICUL ) 
									  work.WEB&&m&i. (in = b keep = MATRICUL dtcontwb  )
									  work.BIW&&m&i. (in = c keep = MATRICUL dtcontbo  );
								by MATRICUL; 
								if a = 1 and (b = 1 or c = 1) ; 
							ATTRIB 
							WEB&&m&i. format = 8. informat = 8. length = 8 
												LABEL = "NOMBRE DE CONNEXIONS (JOURS) WEB SUR &&m&i";  
							WEB&&m&i.=1;
							/*if dtcontwb=. then dtcontwb=dtcontbo;*/
					run ; 

				%end;


				%if &topexist_web. = O and &topexist_biw.= N %then %do ; 

					proc sort data = GRA.WEB&&m&i. out = work.WEB&&m&i. nodupkey; 
							by matricul dtcontwb; 
					run ; 

					Data work.WEB&&m&i. ;
								merge work.final  (in = a keep =  MATRICUL ) 
									  work.WEB&&m&i. (in = b keep = MATRICUL dtcontwb  );
								by MATRICUL; 
								if a = 1 and b = 1 ; 
							ATTRIB 
							WEB&&m&i. format = 8. informat = 8. length = 8 
												LABEL = "NOMBRE DE CONNEXIONS (JOURS) WEB SUR &&m&i";  
							WEB&&m&i.=1;
						run ; 
					

				%end;

				%if &topexist_web. = O %then %do ; 
						
						proc means data = work.WEB&&m&i. noprint nway; 
							VAR WEB&&m&i. ; 
							class matricul ; 
							output out = work.CONTWB&&m&i. (drop= _type_ _freq_) sum() = ; 
						run ; 		  
						

					 	/* mise � jour de la table final avec donn�es existantes */	
						data work.final  ; 
							merge work.final ( in = a ) 
								  work.CONTWB&&m&i. ; 
							by matricul ; 
							if a = 1 ; 
						run ; 
				%end ;
		/*Nettoyage de la work apr�s jointure*/
		proc datasets lib=work nolist; delete WEB&&m&i. CONTWB&&m&i. temp; quit; run;
	%end ; 

	/*Cr�ation des variables explicatives de la CATEGORIE DONNEES CONNEXIONS WEB 
*/ 
	data work.final (drop= WEB: );
		set work.final;
	/*Vecteur des donn�es historiques*/
		array CONWEB 	%do i = 1 %to 18; 	WEB&&m&i. 	%end ; ; 
	/*Initialisation des indicateurs*/
		attrib NBCONWEB18 length=3. label="NOMBRE DE CONNEXIONS JOURNALIERES CAF.FR EN 18 MOIS";
	/*Code de cr�ation des indicateurs*/
		NBCONWEB18=0; do i=1 to 18; if (CONWEB(i) ne .  and CONWEB(i)>0) then do; 
			NBCONWEB18=NBCONWEB18+CONWEB(i); end;end;
	run;
%mend ; 
%mp_HISTWEB ;


/**********************************************************************************************************************/
/* e)Cat�gorie d'indicateurs : DIVERS SUR BASE INFOCENTRE (entrep�ts CAF 
seulement)						              */
/**********************************************************************************************************************/

/*DONNEES DOS **/
Data work.final ; 
	merge work.final ( in = a) 
		  basefrm.DOS  ( in = b keep = MATRICUL MODPAIDO DTINSCDO TITUREDO TITUPRDO) 
; 
	by matricul ; 
	if a = 1 and b = 1 ; 
	rename MODPAIDO = MODPAIDO_dernier 
		   DTINSCDO = DTINSCDO_dernier 
		   TITUREDO = TITUREDO_dernier 
		   TITUPRDO = TITUPRDO_dernier ; 
run ; 

/*Nettoyage de la work apr�s jointure*/
proc datasets lib=work nolist; delete  DOS ; 
quit; run;


/**************************************************************************************************/
/* f)Cat�gorie d'indicateurs : HISTORIQUE SITUATION CONTROLE ET VERIFICATION			          */
/**************************************************************************************************/
%macro mp_CTRLVERIF ; 
	%do i= 1 %to 18 ; 

		/*FVAP*//*Recup donn�es verif comptable apr*/
		data _null_ ; /* contr�le existence des tables */ 
			if  (exist("basestat.fvap&&m&i.",'data') or exist("basestat.fvap&&m&i.",'view'))
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
		run ; 
		%if &topexist. = O %then %do ; 
		data work.fvap&&m&i. ; 
	  			merge work.final  (in = a keep =  MATRICUL )  
				Basestat.fre&&m&i. (in = b keep = MATRICUL CATBEN where = (CATBEN ne "99" ))
				basestat.fvap&&m&i. (in = c keep = MATRICUL CIBVERAP MTFVERAP );
				by MATRICUL; 
				if a = 1 and b = 1 and c = 1 ; 
		  rename 
		  		CIBVERAP=CIBV&&m&i.  ; 
		  if first.matricul ; 
		run ; 
		/*proc sort data = work.fvap&&m&i. nodupkey ; by matricul ; run ; */
		data work.final ; 
			merge work.final ( in = a ) 
				  work.fvap&&m&i. ; 
			by matricul; 
			if a = 1 ; 
		run ;
		%end ; 
		/*Nettoyage de la work apr�s jointure*/
		proc datasets lib=work nolist; delete fvap&&m&i.; quit; run;
	%end ; 
	/*Cr�ation des variables explicatives de la CATEGORIE HISTORIQUE SITUATION 
DOSSIER ALLNAT */ 
	data work.final (drop= CIBV:  );
		set work.final;
	/*Vecteur des donn�es historiques*/
		array cibv	%do i = 1 %to 18; CIBV&&m&i.%end ; ;
	/*Initialisation des indicateurs*/
		attrib MOICONTVER length=3. label="NB MOIS DEPUIS DERNIER CONTR�LE VERIF";
	/*Cr�ation d'une dur�e depuis un contr�le caf v�rif*/
		do i=1 to 18; if cibv(i) >'0' then do; MOICONTVER=i; i=18; end;end;
	run;
%mend ; 
%mp_CTRLVERIF ; 



/**********************************************************************************************************************/
/* h)Cat�gorie d'indicateurs : DONNEES SDP																		      */
/**********************************************************************************************************************/

proc sort data=gra.sdsuiarr out=SDP; by matricul; run;

Data SDP ;
			merge 	SDP (in = a 	keep =  MATRICUL DTARRPIE ETATSDP IDNATPIE TYCREASD NATPIE RGCREASD
								where=(rgcreasd=1 and tycreasd in ("3" "5" "8" "6"))) 
					work.final (in = b keep = MATRICUL );
			by MATRICUL; 
			if a = 1 and b = 1 ; 

			
dtarrpie_mmaa=compress(put(month(dtarrpie),z2.)!!SUBSTR(put(year(dtarrpie),z4.),3,2))  ;

run;


%macro mp_comptesdp(mv_table,mv_natpie,mv_tabsortie,mv_libvar);
proc means data = &mv_table noprint; 
				VAR matricul ; 
				where (dtarrpie_mmaa="&&m&i." and &mv_natpie.);
				output out = &mv_tabsortie. (drop= _type_ _freq_) N = &mv_libvar.; 
				class matricul;
		run ; 
%mend;

%macro mp_SDP() ; 

	%do i= 1 %to 18 ; 
		%mp_comptesdp(SDP,		(tycreasd eq "6" And NATPIE eq "MELALL"),work.PIE_MAIL&&m&i.,	PIE_MAIL&&m&i..);
		%mp_comptesdp(SDP,		(tycreasd in ("3" "5" "8") And NATPIE IN ("NPAI","PND")), work.PIE_NPAI&&m&i.,	PIE_NPAI&&m&i..);/*MODIF DU 28/11/2016 - NPAI a �t� remplac� par PND*/
		%mp_comptesdp(SDP,		(NATPIE eq "DSIT"),	work.PIE_DSIT&&m&i.,	PIE_DSIT&&m&i..);
		%mp_comptesdp(SDP,		(NATPIE eq "NOTINT"),work.PIE_NOTINT&&m&i., PIE_NOTINT&&m&i..);
		%mp_comptesdp(SDP,		(NATPIE eq "DSFAM"),work.PIE_DSFA&&m&i.,	PIE_DSFA&&m&i..);

	/* mise � jour de la table final avec donn�es existantes */	
			data work.final  ; 
				merge work.final ( in = a ) 
						work.PIE_MAIL&&m&i. 
						work.PIE_NPAI&&m&i. work.PIE_DSIT&&m&i. 
						work.PIE_NOTINT&&m&i. work.PIE_DSFA&&m&i. ; 
				by matricul ; 
				if a = 1 ; 
			run ; 	

			/*Nettoyage de la work apr�s jointure*/
			proc datasets lib=work nolist; 
				delete 	PIE_TELE&&m&i. PIE_MAIL&&m&i. 
						PIE_NPAI&&m&i. PIE_DSIT&&m&i. 
						PIE_NOTINT&&m&i.  PIE_DSFA&&m&i.;  
			quit; run;
	%end;

/*Cr�ation des variables explicatives de la CATEGORIE DONNEES SDP */ 
	data work.final (drop= PIE: COU:);
	set work.final;
	/*Vecteur des donn�es historiques*/
		array PIE_MAIL 	%do i = 1 %to 18; 	PIE_MAIL&&m&i.. 		%end ;; 
		array PIE_NPAI 	%do i = 1 %to 18; 	PIE_NPAI&&m&i.. 	%end ;;
		array PIE_DSIT 	%do i = 1 %to 18; 	PIE_DSIT&&m&i..  	%end ;; 
		array PIE_NOTINT 	%do i = 1 %to 18; 	PIE_NOTINT&&m&i.. 	%end ;; 
		array PIE_DSFA 	%do i = 1 %to 18; 	PIE_DSFA&&m&i.. 	%end ;; 

	/*Initialisation des indicateurs*/
		attrib NBPIE_DSIT18 length=3. 	label="NOMBRE DE PIECES DSIT 18 MOIS";
		attrib NBPIE_MAIL18 length=3. 	label="NOMBRE DE MAILS 18 MOIS";

		attrib MOIPIE_NOTINT 	length=3. 	label="NB MOIS DEPUIS DEBUT NOTE INTERNE";
		attrib MOIPIE_NPAI 		length=3. 	label="NB MOIS DEPUIS DEBUT NPAI";
		attrib MOIPIE_DSIT 		length=3. 	label="NB MOIS DEPUIS DEBUT DSIT";
		attrib MOIPIE_MAIL 		length=3. 	label="NB MOIS DEPUIS DEBUT MAIL";

	/*Code de cr�ation des indicateurs*/
		NBPIE_MAIL18=0; do i=1 to 18; 	if (PIE_MAIL(i) ne .  and PIE_MAIL(i)>0) then do; 	
			NBPIE_MAIL18=sum(NBPIE_MAIL18,PIE_MAIL(i)); end;end;
		NBPIE_DSIT18=0; do i=1 to 18; 	if (PIE_DSIT(i) ne .  and PIE_DSIT(i)>0) then do; 	
			NBPIE_DSIT18=sum(NBPIE_DSIT18,PIE_DSIT(i)); end;end;

		attrib MOIPIE_MAIL 		length=3. 	label="NB MOIS DEPUIS PIECE MAIL";
		attrib MOIPIE_NPAI 		length=3. 	label="NB MOIS DEPUIS PIECE MNPAI";
		attrib MOIPIE_DSIT 		length=3. 	label="NB MOIS DEPUIS PIECE MDSIT";
		attrib MOIPIE_DSFA 		length=3. 	label="NB MOIS DEPUIS PIECE MDSITFAM";
		attrib MOIPIE_NOTINT 	length=3. 	label="NB MOIS DEPUIS PIECE NOTE INT.";

			MOIPIE_MAIL=0; do i=1 to 18; 	if (PIE_MAIL(i) ne .  and PIE_MAIL(i)>0) then do; 
				MOIPIE_MAIL=i; i=18; end;end;		
			MOIPIE_NPAI=0; do i=1 to 18; 	if (PIE_NPAI(i) ne .  and PIE_NPAI(i)>0) then do; 
				MOIPIE_NPAI=i; i=18; end;end;		
			MOIPIE_DSIT=0; do i=1 to 18; 	if (PIE_DSIT(i) ne .  and PIE_DSIT(i)>0) then do; 
				MOIPIE_DSIT=i; i=18; end;end;		
			MOIPIE_DSFA=0; do i=1 to 18; 	if (PIE_DSFA(i) ne .  and PIE_DSFA(i)>0) then do; 
				MOIPIE_DSFA=i; i=18; end;end;		
			MOIPIE_NOTINT=0; do i=1 to 18; 	if (PIE_NOTINT(i) ne .  and PIE_NOTINT(i)>0) then do; 
				MOIPIE_NOTINT=i; i=18; end;end;		
	run;
%mend;

%mp_SDP() ;

/************************************************************************************/
/* i)Cat�gorie d'indicateurs : SITUATION DOSSIER FRE SUR LA DATE DE REFERENCE 		*/
/************************************************************************************/

data work.final ;
	merge work.final ( in = a ) 
		  Basestat.fre&m1. ( keep= CATBEN AAHVERS AAHPERE ABANEUCO	ABANEURE	ACTCONJ	ACTRESPD															
					ALFVERS	ALSVERS	APLVERS ANNNEN1-ANNNEN12 MOINEN1-MOINEN12 QUALEN1-QUALEN12	
					ASFVERS	COMPLAAH	DTNAICON	DTNAIRES	DTREFFRE	MATRICUL
					MTLOGCAL	MTLOYREM	MTPAJCAV MTPFVERS	MTREMRSV	MTREVBRU	NAUTLOGV
					NBENFCHA	NBENLEFA	NBNAIMOI	NECHALOG	NORDALLC	NUMCAF		PAJEVERS PERSCOUV	PPRPPU
					PRESCONJ	RESALTDO	RESTRACE	RESTRACO revactmo revactme	RESTRAC2	RSASOCL	RSAVERS	RSDTDORI	
					SEXE SITFAM	TIEPAALI	NATTUT	STATUETU	
					/*MODIF DSER oct 2014 : variables revactmo revactme restrac2 n'�taient pas dans le KEEP*/	

					/*Modification PAC nov2015 : ajout des nouvelles variables et suppression des anciennes */
					%if (&mv_pres_RSAACT. gt 0) %then %do; RSAACT %end;
					%if (&mv_pres_RESTRRSA. gt 0) %then %do; RESTRRSA %end;
					%if (&mv_pres_MTOTFOY. gt 0) %then %do; MTOTFOY1 MTOTFOY2 MTOTFOY3 %end;
					%if (&mv_pres_DTINCOAC. gt 0) %then %do; DTINCOAC %end;
					%if (&mv_pres_PPAVERS. gt 0) %then %do; PPAVERS %end;
						
					   	where = (catben ne "99"));
	by matricul ; 
	if a = 1 ; 
	if first.matricul ; 
run;





/**********************************************************************************************************************/
/**********************************************************************************************************************/
/**********      Partie 2 : Recodages et cr�ations de nouvelles variables	     **************************************/
/**********************************************************************************************************************/
/**********************************************************************************************************************/


/* FORMATS DATAMINING*/
proc format;
	picture sepmil   low-<0='000 000 000 009' (prefix='-')  0-high='000 000 000 009' (prefix=' ');
	picture sepmild  low-<0='000 000 000 009,0' (decsep=',' prefix='-')  0-high='000 000 000 009,0' (decsep=',' prefix=' ');
	value $_act
		/* Cong� mater*/ "MAT","CSS","CAC","SAB","CCV"="CON"
		/* Activit� normale */"ACR", "AMA", "AMT", "APP", "CES", "CIA", "CIS", "CGP", "CSA", "DNL", "INT", "MOA", "PIL", "SAL", "SFP", "TSA", "VRP" ,"ETS",
		/*Activit� en milieu prot�g� avec AAH */ "AAP", "CAT" , "RAC"="ACT"
		/* entrepreneur, travailleur ind�pendant */"ETI", "CJT", "EXP", "MAR", "GSA" , "EXS"="ETI"
		/* Ch�mage*/ "CAR" ,"CHO","ABA", "AFC", "ADA", "CDA", "CPL", "FDA", "MMC" ,"ASP","ASS", "AFD", "AIN", "ADN", "CDN", "CNI", "ANI", "FDN" , "MMI"="CHO"
		/* Retraite*/ "RET"="RET"
		/*ETUDIANT*/ "EBO" , "ETU" = "ETU"
		/*INACTIFetinconnu*/ "SSA", "AFA", "MOC", "SNR",  "SAV", "SIN", "MAL"="INA"
		"00","000","99"="0CO"
		"XXX"="XXX"
		/*AUTRES*/ other="AUT";

	value $_actr /*AJOUT D'UN CODAGE SPECIFIQUE PR L'ACTIVITE DU CONJOINT*/
		/* Cong� mater*/ "MAT","CSS","CAC","SAB","CCV"="CON"
		/* Activit� normale */"ACR", "AMA", "AMT", "APP", "CES", "CIA", "CIS", "CGP", "CSA", "DNL", "INT", "MOA", "PIL", "SAL", "SFP", "TSA", "VRP" ,"ETS",
		/*Activit� en milieu prot�g� avec AAH */ "AAP", "CAT" , "RAC"="ACT"
		/* entrepreneur, travailleur ind�pendant */"ETI", "CJT", "EXP", "MAR", "GSA" , "EXS"="ETI"
		/* Ch�mage*/ "CAR" ,"CHO","ABA", "AFC", "ADA", "CDA", "CPL", "FDA", "MMC" ,"ASP","ASS", "AFD", "AIN", "ADN", "CDN", "CNI", "ANI", "FDN" , "MMI"="CHO"
		/* Retraite*/ "RET"="RET"
		/*ETUDIANT*/ "EBO" , "ETU" = "ETU"
		/*INACTIFetinconnu*/ "SSA", "AFA", "MOC", "SNR",  "SAV", "SIN", "MAL"="INA"
		"XXX"="XXX"
		/*AUTRES*/ other="AUT";


	value MTLOGC_F(fuzz=0.005 ) 0-<1='0' 1-<150 ='de 1 � 150' 150 -< 300 ='de 150 � 300' 300-high ='sup�rieur � 300';

	value MTPAJCAV_quant
		0="0"
		0.001-<361="0 � 361"
		361-high="361 et+"
		.="MISSAUT"
		other="MISSAUT"	;

	value agecon_quant
		60-high="60 et plus"
		.="MISSAUT"
		other="MISSAUT";

	value ageresp_quant
		0-<34="00 � 33"
		other="MISSAUT";


	value $OCLOG_P 	"20", "21", "22", "23", "24", "40", "41", "80", "81", "82",   
					"83", "84","70", "71", "72", "73", "74", "75", "76"   ="ACCESSION" 
					other="AUTRE";

	value DTINSCDO_dernier_quant
	0-<12936="0 � <12936"
	12936-<14302="12936 � <14302"
	14302-<15941="14302 � <15941"
	15941-<17976="15941 � <17976"
	17976-19417 ="17976 � <=19417"
	.="MISSAUT"
	other="MISSAUT";

	value NBM_ODMS_quant
	0-<1="0 � <1"
	1-<13="1 � <13"
	13-<32="13 � <32"
	32-500="32 � 500"
	/*60-291 ="60 � <=291"*/
	.="MISSAUT"
	other="MISSAUT";

	
	value TXEFF_P(fuzz=1.110223E-16 ) 
		-100000 - <35='0aLettxeff 0a35' 
		35-high='35etplus';

	value NBWEB18_quant
	0-<4="0 � <4"
	/*1-<4="1 � <4"*/
	4-<12="4 � <12"
	12-<34="12 � <34"
	34-high ="34 � <=504"
	.="MISSAUT"
	other="MISSAUT";

	value RUC_RED_quant
	/*0-<0.16="0 � <0.16"*/
	0-<577="0 � <577"
	577-<942="577 � <942"
	942-<1483.33="942 � <1483.33"
	1483.33-high ="1483.33 � <=19745" /*Modification nov2015 pour tenir compte des RUC >19745 */
	.="MISSAUT"
	other="MISSAUT";


	value MOINSCDO_quant
	0-<12="0 � <12"
	12-<49="12 � <49"
	49-high ="49 et plus"
	.="MISSAUT"
	other="MISSAUT";

	value MTPFUC_F(fuzz=1.776357E-15 ) 
	0-<213.69591859 ='de 1 � 213.7 ' 
	213.69591859 -< 454.43788174 ='de 213.7 � 454.4 ' 
	454.43788174 -< 695.17984489 ='de 454.4 � 695.2 ' 
	695.17984489 -high ='sup�rieur � 695.2 ';

	value $prescj 
	'0'=" SANS_CONJOINT" 
	'2'="AV_CONJOINT";

run;


/********************************************************************************/
/* 		ETAPES DE CREATION DES VARIABLES A PARTIR DES DONNES BRUTES				*/
/********************************************************************************/

data work.Final ;
set work.Final;
		/*********************************************************************************/
		/*0- Maintenance des mod�les: Modification de variables suite � �volution du DII */
		/*********************************************************************************/

			/*Modification PAC nov2015 : Remplacement de RSDTDORI par DTINCOAC*/
			%if (&mv_pres_DTINCOAC. eq 0) %then %do; if RSDTDORI>0 then NBM_ODMS=INTCK("MONTH",RSDTDORI,dtreffre); else NBM_ODMS=0; %end;
			%if (&mv_pres_DTINCOAC. gt 0) %then %do; if DTINCOAC>0 then NBM_ODMS=max(0,INTCK("MONTH",DTINCOAC,dtreffre)); else NBM_ODMS=0; %end; /*Corr juin 2016*/ 
			label NBM_ODMS= "NB MOIS DEPUIS OD MS (API ou RMI)";


		/************************************************************************/
		/*1- Gestion des donn�es manquantes - Recodage direct de variables      */
		/************************************************************************/

		/*GESTION DES REVENUS INCONNUS*/
		if MTREVBRU=. then mtrevbru=999999;
		if MTLOYREM>9000 then mtloyrem=9999;
		/*DATE NAISSANCE INCONNUE*/
		if dtnaires=. then dtnaires=mdy(1,1,1970);





		/************************************************************/
		/*2- CREATION DE VARIABLES DIRECTEMENT SUR DONNEES SOURCES  */
		/************************************************************/

		if aahvers>'0' or complaah>'0' then AAH_COMPAAH="AAH"; else AAH_COMPAAH="0"; 
		label AAH_COMPAAH= "DROIT AAH";

		if aahvers>'0' or complaah>'0' then AAH_COMPAAH_P="AAH"; else AAH_COMPAAH_P="0"; 
		label AAH_COMPAAH_P="TOP AAH OU COMPLEMENT";


		/*Abattement ou neutral*/
		if (abaneure>0 or abaneuco>0) then ABANEU="Ab. ou neutral."; else ABANEU="Aucun";
		label ABANEU= "TOP ABATTEMENT OU NEUTRAL POUR ALLOC OU CONJOINT";


		ACTCONJ_f=put(ACTCONJ,$_act.);
		label ACTCONJ_f= "ACTIVIT� CONJOINT";

		ACTRESPD_f=put(ACTRESPD,$_actr.); 
		label ACTRESPD_f= "ACTIVIT� RESP DOS.";

		if dtnaicon ne . then agecon=round ( INTCK("MONTH",dtnaicon,dtreffre) /12 );
		label agecon= "AGE CONJOINT";

		if dtnaires ne . then ageresp=round ( INTCK("MONTH",dtnaires,dtreffre) /12 );
		label ageresp= "AGE RESP. DOS";

			agecon_quant=put(agecon,agecon_quant.); label agecon_quant="AGE CONJOINT";
			ageresp_quant=put(ageresp,ageresp_quant.); label ageresp_quant="AGE RESP. DOS";


		if  ASFVERS>'0' then ASF="ASF";else ASF="0";
		label ASF= "DROIT ASF";


		if ((ALFVERS>'0')  or (ALSVERS>'0') or  (APLVERS>'0')) then AL="AL"; else AL="0";
		label AL= "DROIT AUX AL";



		MTLOGCAL_f= put(MTLOGCAL,MTLOGC_f.);
		label MTLOGCAL_f= "MONTANT D AIDE AU LOGEMENT VERSABLE";

		attrib Nbenf02 label="NB ENFANTS DE 0 A 2 ANS REVOLUS" format=1. informat=1.;
		attrib Nbenf35 label="NB ENFANTS DE 3 A 5 ANS REVOLUS" format=1. informat=1.;
		attrib Nbenf611 label="NB ENFANTS DE 6 A 11 ANS REVOLUS" format=1. informat=1.;
		attrib Nbenf1218 label="NB ENFANTS DE 12 A 18 ANS REVOLUS" format=1. informat=1.;
		attrib Nbenf18p label="NB ENFANTS DE PLUS DE 18 ANS REVOLUS" format=1. informat=1.;
		label TOPEN19= "Top enfant >19 ans";
		attrib datenai label="DATENAI TEMP" format=yymmdd10.;
		array mois[12] moinen1-moinen12;
		array annee[12] annnen1-annnen12;
		Nbenf02=0; Nbenf35=0; Nbenf611=0; Nbenf1218=0; Nbenf18p=0;TOPEN19=0;
		do i=1 to min(nbenlefa,12);
		    if annee[i] in (0,.) then datenai=.;
		    else do;
		        if mois[i] in (0,.) then mois[i]=6;
		           datenai=mdy(mois[i],1,annee[i]);
		           age=int(datdif(datenai,dtreffre,'ACT/ACT')/365);
		        end;
		if (age < 3) then Nbenf02=Nbenf02+1;
		if (age >= 3 and age < 6) then Nbenf35=Nbenf35+1;
		if (age >= 6 and age < 11) then Nbenf611=Nbenf611+1;
		if (age >= 12 and age < 18) then Nbenf1218=Nbenf1218+1;
		if (age >= 18) then Nbenf18p=Nbenf18p+1;
		if (age >= 19) then TOPEN19=1;
		end;
		Nbenf02=sum(Nbenf02,nbnaimoi);

		if nbenf02 > 0 then TOPENF02='1'; else TOPENF02='0'; 
		if nbenf02 > 0 then TOPENF02_P='1'; else TOPENF02_P='0'; 
		label TOPENF02_P="TOP PRESENCE ENFANT DE 0 A 2 ANS";

		if nbenf1218 > 0 then TOPENF1218='1'; else TOPENF1218='0'; 
		if nbenf1218 > 0 then TOPENF1218_P='1'; else TOPENF1218_P='0'; 
		label TOPENF1218_P="TOP PRESENCE ENFANT DE 12 A 18 ANS";

		presconj_f=put(presconj,$prescj.);
		label presconj_f= "SITUATION FAMILIALE";

		if (RSAVERS ne '0' /*Modification PAC nov2015 : ajout PPA */  %if (&mv_pres_PPAVERS. gt 0) %then %do; or PPAVERS ne "0" %end; )
																				 then MS="MS (+ARE)"; else MS="0";
		label MS= "DROIT � UN MINIMUM SOCIAL ou RSA Activit�/PPA";



			NBUC=0; 
			array tab{12} annnen1 annnen2 annnen3 annnen4 annnen5 annnen6 annnen7 annnen8 annnen9 annnen10 annnen11 annnen12;
			n13=nbnaimoi;n14=0;n611=0;n1214=0;n1518=0;
			do i=1 to 12;
			    select(tab{i});
			    when (0);
			    otherwise do;
			        if year(dtreffre)-tab{i} <= 13 then n13=n13+1;
			        if year(dtreffre)-tab{i} >13 then n14=n14+1;
			        end;end;end;
			If (PRESCONJ='0') then do;
			    If (n13+n14=0) then NBUC=1.0;
			    else NBUC=1.2 +(0.3*n13) +(0.5*(n14));
			end; 
			else NBUC=1.0 +(0.3*n13) +(0.5 * (1+ n14)); 
			label NBUC="NB UNITE CONSOMMATION";


			MTPAJCAV_quant=put(MTPAJCAV,MTPAJCAV_quant.); 
				label MTPAJCAV_quant="MONTANT PAJE COMPLEMENT ACTIVITE VERSABLE";


			if (pajevers>'0' ) then PAJE_AFEAGED="PAJE";else PAJE_AFEAGED="0";
			label PAJE_AFEAGED= "DROIT PAJE";

/* Calcul des revenus mensuels hors PF*/
/* suppression Donn�es RMI et API  : RESTRAPI RESTRRMI*/
/*Maintenance : Ajout code pour RESTRRSA*/
/*Modification PAC nov2015 : remplacement de RESTRRSA par MTOTFOY1-2-3 */
if (RSAVERS ne '0' %if (&mv_pres_PPAVERS. gt 0) %then %do; or PPAVERS ne "0" %end;) 
							then do; 

							%if (&mv_pres_mtotfoy. gt 0) 	%then %do;
														if sum(mtotfoy1,mtotfoy2,mtotfoy3)<=9000 then revmens=sum(mtotfoy1,mtotfoy2,mtotfoy3); 
														else if sum(mtotfoy1,mtotfoy2,mtotfoy3)>9000 then revmens=99999; 
														%end;

							%if (&mv_pres_mtotfoy. eq 0) 	%then %do;
														if restrrsa<=9000 then revmens=max(RESTRRSA,MTREMRSV*3)/3; 
														else if restrrsa>9000 then revmens=99999; 
														%end;
							end;

 else if mtrevbru>900000 then REVMENS = 99999; 
 else revmens=round(mtrevbru/12); 
label REVMENS= "REVENUS MENSUALISES TRIM OU AN2";





			if NATTUT > '0' then tut_P="TUTELLE"; else tut_P="0";
			label tut_P="TOP TUTELLE";

/*************************************************************************************************************/
/*3- CREATION DE VARIABLES RECOMBINEES (� partir de variables cr��es en 1) - class�es par ordre alphab�tique)*/
/*************************************************************************************************************/


		if DTINSCDO_dernier=. then MOINSCDO=0; else MOINSCDO=intck("MONTH",DTINSCDO_dernier,DTREFFRE);
		If MOINSCDO=-1 then MOINSCDO=0; /* Modification nov.2015 pour prise en compte de la valeur -1*/
		label MOINSCDO="NB MOIS DEPUIS AFFILIATION PAR RAPPORT dernier mois";


		MOINSCDO_quant=put(MOINSCDO,MOINSCDO_quant.); 
			label MOINSCDO_quant="NB MOIS DEPUIS AFFILIATION PAR RAPPORT dernier mois";


		/*Montant de prestation par UC*/
		mtprest=sum(mtpfvers); if mtprest=. then mtprest=0;
		label MTPREST="MONTANT DE PRESTATION PERCUE";

		mtprestuc=mtprest/nbuc;
		label MTPRESTUC="Montant de prestation ver�e par UC";
		mtprestuc_f=put(mtprestuc,mtpfuc_f.);
		label MTPRESTUC_f="Montant de prestation ver�e par UC";


		/*Anciennet� en classes*/
		NBM_ODMS_quant=put(NBM_ODMS,NBM_ODMS_quant.); 
		label NBM_ODMS_quant="NB MOIS DEPUIS OD MS (API ou RMI)";

/*************************************************************************************************/
/*4- Redressement des donn�es manquantes et cr�ation de variables �partir des donn�es redress�es */
/*************************************************************************************************/

		/*Redressement de la variable revmens et cr�ation de variables*/
		TOP_1conj0ms0al =((PRESCONJ_f="AV_CONJOINT") and (MS="0") and (AL="0") );
		TOP_1conj0ms1al=( (PRESCONJ_f="AV_CONJOINT") and (MS="0") and (AL="AL") );
		TOP_1conj1ms0al =( (PRESCONJ_f="AV_CONJOINT") and (MS="MS (+ARE)") and (AL="0") );
		TOP_1conj1ms1al =( (PRESCONJ_f="AV_CONJOINT") and (MS="MS (+ARE)") and (AL="AL") );
		TOP_0conj0ms0al =((PRESCONJ_f="SANS_CONJOINT") and (MS="0") and (AL="0"));
		TOP_0conj0ms1al =((PRESCONJ_f="SANS_CONJOINT") and (MS="0") and (AL="AL"));
		TOP_0conj1ms0al=((PRESCONJ_f="SANS_CONJOINT") and (MS="MS (+ARE)") and (AL="0") );
		TOP_0conj1ms1al=((PRESCONJ_f="SANS_CONJOINT") and (MS="MS (+ARE)") and (AL="AL") );
		toppaje=(PAJE_AFEAGED="PAJE");
		topasf=(ASF="ASF");
		topaah=(AAH_COMPAAH="AAH");
		femme=(SEXE="2");
		activresp=(ACTRESPD_f="ACT");
		activconj=(ACTCONJ_f="ACT");
		chomage=((ACTRESPD_f="CHO") or (ACTCONJ_f="CHO"));

		/*    Mod�le de redressement de la variable revmens    */
		/*    sur 9290 observations : revmens connus et mtprest> *//*    R�=0,622783 dl=17 */
		if REVMENS=99999 then do;
		    REVMENS_RED=max(0,189.972886  + activconj * (679.110808 ) + activresp * (449.804327 ) + ageresp * (9.074702 ) + chomage * (214.096473 ) + femme * (82.879216 ) +
		    mtprest * (-0.50641 ) + NBENFCHA * (182.126509 ) + TOP_0conj0ms0al * (-127.991336 ) +
		    TOP_0conj0ms1al * (-253.894658 ) + TOP_0conj1ms0al * (-577.076753 ) + TOP_0conj1ms1al * (-482.715358 ) + TOP_1conj0ms0al * (1317.410976 ) +
		    TOP_1conj1ms0al * (-684.857514 ) + TOP_1conj1ms1al * (-536.331104 ) + topaah * (-183.599325 ) + topasf * (-124.570829 ) + toppaje * (266.586659 ));
		end;
		else REVMENS_RED=REVMENS;
		label revmens_red="REVENUS MENSUALISES (yc PF, priorit� DTR sur ANNUELS) REDRESSES";


		if (MS="MS (+ARE)") then restri_red=revmens_red; else restri_red=99999;
		if (MS="MS (+ARE)") then restriuc_red=revmens_red/nbuc; else restriuc_red=99999;
		RUC_RED=REVMENS_RED/NBUC; label RUC_RED="RUC Mensuel hors PF";

		RUC_RED_quant=put(RUC_RED,RUC_RED_quant.); 
			label RUC_RED_quant="RUC Mensuel hors PF";

/*Redressement de la variable MTREVBRU et cr�ation de variables*/

		/*Mod�le de redressement de la variable mtrevbru*/
		/*sur 9166 observations : mtrevbru connus *//*R�=0,595 dl=17 */
		if MTREVBRU>=9999998 then do;
		    MTREVBRU_RED=max(0,1380.700333  + NBENFCHA * (1836.558261 ) + ageresp * (109.650043 ) + mtprest * (-4.632265 ) + 
		    TOP_1conj0ms0al * (16291.540008 ) + TOP_1conj1ms0al * (-6522.248891 ) + TOP_1conj1ms1al * (-4408.085236 ) + 
		    TOP_0conj0ms1al * (-2569.735729 ) + TOP_0conj1ms0al * (-5630.168175 ) + TOP_0conj1ms1al * (-4575.663174 ) +
		    chomage * (3652.423686 ) + toppaje * (3042.850353 ) + topasf * (-1836.292169 ) + topaah * (-3369.087472 ) + 
		    femme * (773.741889 ) + activresp * (5482.94001 ) + activconj * (8673.047286 ));
		end;
		else MTREVBRU_RED=MTREVBRU;
		label mtrevbru_red="REVENUS ANNUELS N-2 BRUTS REDRESSES";

		/*Taux d'effort logement sur revenus mensualis�s redress�s*/
		if mtlogcal>0 then do;

		    charge=49.14+ 11.12*(nechalog+nautlogv);
		    loyer=sum(charge,mtloyrem);
		    if mtloyrem=9999 then txeff=-1;
		    else if (revmens_red/*+mtprest-mtlogcal*/)=0 then txeff=-1;
		    else txeff=((loyer-mtlogcal)/(revmens_red/*+mtprest-mtlogcal*/))*100;
		    restaviv=max(0,revmens_red+mtprest-loyer);
		end;

		else do; txeff=-2; restaviv=-2; loyer=0; charge=0; end; 
		label TXEFF="TAUX EFFORT LOGEMENT APRES AIDE";
		label restaviv="RESTE A VIVRE APRES PAIEMENT DU LOYER ET VERSEMENT AIDE LOGEMENT";

		TXEFF_P=put(txeff,TXEFF_P.);
		label TXEFF_P="TAUX D EFFORT LOGEMENT APRES AIDE";

/*************************************************************************/
/** 			5- Cr�ation de nouveaux indicateurs 					**/
/*************************************************************************/
		
		if statuetu in ("1" "2") then statuetu ="1";
		else if statuetu in ("3" "4") then statuetu ="3";

		/*AAHINTA*/
		attrib TYPAAH length=$1. label="AAH TRIM ou ANNUEL ";
		if AAHPERE='2' and AAHVERS ne '0' then TYPAAH='1'; else 
		if AAHVERS ne '0' then TYPAAH='2';else TYPAAH='0';


/*Cr�ation de nouveaux indicateurs*/
		/*Top revenus inconnus*/
		attrib TOPREVINCAN length=$1. label="TOP REV AN REF INCONNUS ";
		attrib TOPREVINCTRIM length=$1. label="TOP REV DTR M-2 INCONNUS ";
		if mtrevbru>999998 then TOPREVINCAN='1';else TOPREVINCAN='0';
		if restrac2>9998 then TOPREVINCTRIM='1';else TOPREVINCTRIM='0';

/*Titulaire compte versement & prelvt*/
		attrib TITUBANC length=$1. label="COMPAR RIB";
		TITUBANC='1';
		if TITUREDO_dernier ne "" then do;
			if sexe="1" and (index(TITUREDO_dernier,"O")=0) then TITUBANC='1';
			if sexe="2" and (index(TITUREDO_dernier,"E")=0) then TITUBANC='2';
		end;
		if TITUREDO_dernier ne "" and TITUPRDO_dernier ne "" then do;
			if TITUREDO_dernier ne TITUPRDO_dernier then TITUBANC='2';
			if index(TITUREDO_dernier,"O")>0 and index(TITUPRDO_dernier,"O")=0 then TITUBANC='4';
			if index(TITUREDO_dernier,"E")>0 and index(TITUPRDO_dernier,"E")=0 then TITUBANC='4';
		end;


/*Comparaison Revenus d'activit� en annuel / RSA /AAH trim*/
		attrib COMPANTRIM length=$7. label="EVO REV AN REV TRIM";
		COMPANTRIM="AUTRE";
		if presconj ne '0' then do;
			if revactmo=0 and restraco=0 then COMPANTRIM="STABLE0";else 
			if revactme=0 and restrace=0 then COMPANTRIM="STABLE0";else 
			if 0<sum(restraco,restrace)<99999 and sum(revactmo,revactme)=0 then COMPANTRIM="HAUSSE"; else /*ajout  24/06/2016 */
			if sum(restraco,restrace)<99999 and sum(revactmo,revactme)<99999 then do;

				if (sum(restraco,restrace)*4) / sum(revactmo,revactme)>1.6 then COMPANTRIM="HAUSSE";else 
				if (sum(restraco,restrace)*4) / sum(revactmo,revactme)<0.4 then COMPANTRIM="BAISSE";else
				COMPANTRIM="STABLE";
			end;
		end;
		if presconj = '0' then do;
			if sexe='1' then do;
				if revactmo=0 and restraco=0 then COMPANTRIM="STABLE0"; else 

				if revactmo<99999 and restraco<99999 then do;
				if revactmo=0 and 0<restraco<99999  then COMPANTRIM="HAUSSE"; else /*ajout 24/06/2016 */
					if (restraco*4) / revactmo>1.6 then COMPANTRIM="HAUSSE";else 
					if (restraco*4) / revactmo<0.4 then COMPANTRIM="BAISSE";else 
					COMPANTRIM="STABLE";
				end;
			end;
			else do;
				if revactme=0 and restrace=0 then COMPANTRIM="STABLE0"; else 
				if revactme=0 and 0<restrace<99999  then COMPANTRIM="HAUSSE"; else /*ajout  24/06/2016 */
				if revactme<99999 and restrace<99999 then do;

					if (restrace*4) / revactme>1.6 then COMPANTRIM="HAUSSE";else 
					if (restrace*4) / revactme<0.4 then COMPANTRIM="BAISSE";else 
					COMPANTRIM="STABLE";
				end;
			end;
		end;

	/*Agr�gation indicateurs historiques WEB + Bornes*/
		attrib NBWEB18 length=3. label="NB CONNEXIONS WEB+BIW EN 18 MOIS";
		NBWEB18=NBCONWEB18; /*Modif octobre 2016 : NBCONWEB18 contient toutes les connexions Web + bornes*/

		NBWEB18_quant=put(NBWEB18,NBWEB18_quant.); 
			label NBWEB18_quant="NOMBRE DE CONNEXIONS  WEB+BIW EN 18 MOIS";



RUN;



/******************************************************/
/* RECUEIL DES DONNEES POUR LA CREATION DES SEQUENCES */
/******************************************************/

/*MODIF DSER octobre 2014 : - ajout de CATBEN NE '99' pour �viter d'aller chercher des matricules doublons dans les s�quences
- retrait SITFAM car on l'a d�j� initialis�e avec la macro HISTALCAF */
%macro codemens(date);
	%do i=1 %to 18 %by 1;
			data work.final;
			merge work.final (in=a)
				  Basestat.fre&&m&i. (in=b
										keep=MATRICUL CATBEN RSAVERS RESTRAC2 AAHPERE AAHINTA NBENLEFA ASFVERS /*SITFAM ACTRESPD ACTCONJ*/
										/*Modification PAC nov2015 : suppression de RSAACT, inutile ici  */
										where=(CATBEN&&m&i. NE '99')
									   	rename=(CATBEN=CATBEN&&m&i. RSAVERS=RSAVERS&&m&i.  RESTRAC2=RESTRAC2&&m&i. AAHPERE=AAHPERE&&m&i. AAHINTA=AAHINTA&&m&i.
										ASFVERS=ASFVERS&&m&i. /*SITFAM=FAM&&m&i. ACTRESPD=ACR&&m&i. ACTCONJ=ACJ&&m&i.*/))
										/*Modification PAC nov2015 : suppression de RSAACT, inutile ici */	;
			if a=1;
			by matricul;
			run;
	%end;
%mend;

%codemens();


/*************************************************************************/
/** 			5- Cr�ation des s�quences 								**/
/*************************************************************************/



%macro MP_SEQUENCES();
Data final2 (drop= /*ACJ: ACR:*/FAM:  Restrac2: aahinta: aahpere: asfvers:  Qualen: Annnen: Moinen:)
										/*Modification PAC nov2015 : suppression de RSAACT, inutile ici */;
set final;


	/*S�quences SITFAM et indicateur*/
		array vFAM %do i = 1 %to 18; FAM&&m&i. 	%end ; ;
		attrib SEQFAM length=$5. label="SEQUENCES FAM";
		SEQFAM1=vFAM(1); SEQFAM2="X";SEQFAM3="X";SEQFAM4="X";
		cpt=1;
		do i=2 to 18;
			cpt=i+1; 
			if vFAM(i) ne SEQFAM1 then do; SEQFAM2=vFAM(i); i=18;end;
		end;
		if cpt<=18 then do i=cpt to 18;
			cpt=i+1; 
			if vFAM(i) ne SEQFAM2 then do; SEQFAM3=vFAM(i); i=18;end;
		end;
		if cpt<=18 then do i=cpt to 18;
			cpt=i+1;
			if vFAM(i) ne SEQFAM3 then do; SEQFAM4=vFAM(i);end;
		end;
		/*Recodage SEQ manquante*/
		if SEQFAM1="" then SEQFAM1="0";
		if SEQFAM2="" then SEQFAM2="0";
		if SEQFAM3="" then SEQFAM3="0";
		if SEQFAM4="" then SEQFAM4="0";

		/*Indicateur de s�quence FAM*/
		array vseqfam SEQFAM1 SEQFAM2 SEQFAM3 SEQFAM4;
		nbX=0;
		do i=1 to 4;
			if vseqfam(i)="X" then nbX=nbX+1;
		end;

		if SEQFAM1="1" and nbX=3 then SEQFAM="1_XXX"; else
		if SEQFAM1="1" and SEQFAM2="0" and nbX=2 then SEQFAM="1_XXX"; else
		if SEQFAM1 in ("2" "3") and nbX=3 then SEQFAM="23XXX"; else 
		if SEQFAM1 in ("2" "3") and SEQFAM2="0" and nbX=2 then SEQFAM="23XXX"; else 
		if SEQFAM1="4" and nbX=3 then SEQFAM="4_XXX"; else
		if SEQFAM1="4" and SEQFAM2="0" and nbX=2 then SEQFAM="4_XXX"; else 
		if SEQFAM1 in ("5" "6") and nbX=3 then SEQFAM="56XXX"; else 
		if SEQFAM1 in ("5" "6") and SEQFAM2="0" and nbX=2 then SEQFAM="56XXX"; else 
		if SEQFAM1="7" and nbX=3 then SEQFAM="7_XXX"; else 
		if SEQFAM1="7" and SEQFAM2="0" and nbX=2 then SEQFAM="7_XXX"; else 
		if SEQFAM1 in ("1") and nbX=2 then SEQFAM="1__XX"; else 
		if SEQFAM1 in ("4" "5" "6") and nbX=2 then SEQFAM="456XX"; else 
		if SEQFAM1 in ("2" "3" "7") and nbX=2 then SEQFAM="237XX"; else  
		if nbX=1 then SEQFAM="__X__";else 
		SEQFAM="AUTRE";
		/*Recodage car pas assez d'obs mod�les*/
		if SEQFAM="237XX" then SEQFAM="RISQ1";
		if SEQFAM="1_XXX" then SEQFAM="RISQ1";
		if SEQFAM="1__XX" then SEQFAM="RISQ1";
		if SEQFAM="56XXX" then SEQFAM="RISQ1";
		if SEQFAM="AUTRE" then SEQFAM="456XX";



	/*S�quences ACR et indicateur*/
		array vACR %do i = 1 %to 18; ACR&&m&i. 	%end ; ;
		attrib SEQACR length=$9. label="SEQUENCES ACR";
		SEQACR1=vACR(1); SEQACR2="XXX";SEQACR3="XXX";SEQACR4="XXX";
		cpt=1;
		do i=2 to 18;
			cpt=i+1; 
			if vACR(i) ne "" and vACR(i) ne SEQACR1 then do; SEQACR2=vACR(i); i=18;end;
		end;
		if cpt<=18 then do i=cpt to 18;
			cpt=i+1; 
			if vACR(i) ne "" and vACR(i) ne SEQACR2 then do; SEQACR3=vACR(i); i=18;end;
		end;
		if cpt<=18 then do i=cpt to 18;
			cpt=i+1;
			if vACR(i) ne "" and vACR(i) ne SEQACR3 then do; SEQACR4=vACR(i);end;
		end;
		/*Recodage SEQACR*/
		SEQACRfmt1=put(SEQACR1,$_ACTR.);
		SEQACRfmt2=put(SEQACR2,$_ACTR.);
		SEQACRfmt3=put(SEQACR3,$_ACTR.);
		SEQACRfmt4=put(SEQACR4,$_ACTR.);

	/*Indicateur de s�quence ACR*/
		array vseqACR SEQACRfmt1 SEQACRfmt2 SEQACRfmt3 SEQACRfmt4;
		nbXXX=0;
		do i=1 to 4;
			if vseqACR(i)="XXX" then nbXXX=nbXXX+1;
		end;

		if nbXXX=3 then do;
			if SEQACRfmt1 in ("ACT" "ETI" "ETU")  then SEQACR="ACT_X__X_";else 
			if SEQACRfmt1="AUT" then SEQACR="AUT_X__X_";else 
			if SEQACRfmt1="CHO" then SEQACR="CHO_X__X_";else 
			if SEQACRfmt1="CON" then SEQACR="CON_X__X_";else 
			if SEQACRfmt1="INA" then SEQACR="INA_X__X_";else 
			if SEQACRfmt1="RET" then SEQACR="RET_X__X_";
		end;
		else if nbXXX=2 then do;
			if SEQACRfmt1 in ("ACT" "ETU" "ETI") and countc(SEQACRfmt2,"CHO","INA")>1 then SEQACR="_+__-__X_"; else 
			if SEQACRfmt1 in ("ACT" "ETU" "ETI") and countc(SEQACRfmt2,"CHO","INA")=0 then SEQACR="_+__=__X_"; else 
			if SEQACRfmt1 in ("RET" "AUT" "CON") and countc(SEQACRfmt2,"CHO","INA")>1 then SEQACR="_=__-____"; else 
			if SEQACRfmt1 in ("RET" "AUT" "CON") and countc(SEQACRfmt2,"CHO","INA")=0 then SEQACR="_=__=__X_"; else 
			if SEQACRfmt1 in ("CHO" "INA"      ) and countc(SEQACRfmt2,"CHO","INA")>1 then SEQACR="_-__-__X_"; else
			if SEQACRfmt1 in ("RET" "AUT" "CON") and countc(SEQACRfmt2,"CHO","INA")=0 then SEQACR="_-__+__X_"; 
		end;
		else if nbXXX=1 then do;
			if SEQACRfmt1 in ("ACT" "ETU" "ETI") and countc(SEQACRfmt2,"CHO","INA")>1 then SEQACR="_+__-____"; else 
			if SEQACRfmt1 in ("ACT" "ETU" "ETI") and countc(SEQACRfmt2,"CHO","INA")=0 then SEQACR="_+__=____"; else 
			if SEQACRfmt1 in ("RET" "AUT" "CON") and countc(SEQACRfmt2,"CHO","INA")>1 then SEQACR="_=__-____"; else 
			if SEQACRfmt1 in ("RET" "AUT" "CON") and countc(SEQACRfmt2,"CHO","INA")=0 then SEQACR="_=__=____"; else 
			if SEQACRfmt1 in ("CHO" "INA"      ) and countc(SEQACRfmt2,"CHO","INA")>1 then SEQACR="_-__-____"; else
			if SEQACRfmt1 in ("RET" "AUT" "CON") and countc(SEQACRfmt2,"CHO","INA")=0 then SEQACR="_-__+____"; 
		end;
		else SEQACR="????????";
		/*Recodage car manque d'obs pour les mod�les*/
		if SEQACR="RET_X__X_" then SEQACR="ACT_X__X_";
		if SEQACR="AUT_X__X_" then SEQACR="ACT_X__X_";
		if SEQACR="CON_X__X_" then SEQACR="????????";
		if SEQACR="_+__-__X_" then SEQACR="????????";
		if SEQACR="_=__-____" then SEQACR="????????";
		if SEQACR="CHO_X__X_" then SEQACR="????????";
		if SEQACR="INA_X__X_" then SEQACR="????????";

		if SEQACR="_-__-__X_" then SEQACR="_-__-____";

run;
%MEND;
%MP_SEQUENCES;

/* Mise en classe et discr�tisation autres variables historiques*/

proc format;

	value MOICONTVER_quant
	0-<1="0 � <1"
	1-<4="1 � <4"
	4-<7="4 � <7"
	7-<13="7 � <13"
	13-18 ="13 � <=18"
	.="MISSAUT"
	other="MISSAUT";

	value NBCONWEB18_quant
	0-<1="0 � <1"
	1-<4="1 � <4"
	4-<11="4 � <11"
	11-<33="11 � <33"
	33-high ="+ de 33 � <=503" /*Modification nov 2015 pour tenir compte des valeurs >503*/
	.="MISSAUT"
	other="MISSAUT";

	value NADRESS18_quant
	0-<1="0 � <1"
	1-<4="1 � <4"
	4-high="4 � +"
	.="MISSAUT"
	other="MISSAUT";

	value FGRESTRI18_quant
	0-<1="0 � <1"
	1-<7="1 � <7"
	7-<11="7 � <11"
	11-high="11 � +"
	/*4-<6="4 � <6"*/
	/*6-<7="6 � <7"*/
	/*7-100 ="7 � 30"*/
	/*.="MISSAUT"*/
	other="0 � <1";

	value MOIPIE_NOTINT_quant
	/*0-<1="0 � <1"
	1-<5="1 � <5"*/
	5-18="5 � 18"
	other="MISSAUT";

	value MOIPIE_NPAI_quant
	/*0-<1="0 � <1"
	1-<5="1 � <5"
	5-<10="5 � <10"
	10-<14="10 � <14"
	14-18 ="14 � <=18"*/
	/*0-4="00 � 04"*/
	0-<1="00"
	1-<5="1 � <5"
	5-18="05 � 18"
	other="00";


RUN;



data work.final2;
set work.final2;

		FGRESTRI18_quant=put(FGRESTRI18,FGRESTRI18_quant.); label FGRESTRI18_quant="NB FG RESTRI (HORS ANNULES) SUR 18 MOIS";
		MOICONTVER_quant=put(MOICONTVER,MOICONTVER_quant.); label MOICONTVER_quant="NB MOIS DEPUIS DERNIER CONTR�LE VERIF";
		MOIPIE_NOTINT_quant=put(MOIPIE_NOTINT,MOIPIE_NOTINT_quant.); label MOIPIE_NOTINT_quant="NB MOIS DEPUIS PIECE NOTINT";
		MOIPIE_NPAI_quant=put(MOIPIE_NPAI,MOIPIE_NPAI_quant.); label MOIPIE_NPAI_quant="NB MOIS DEPUIS PIECE MNPAI";
		NADRESS18_quant=put(NADRESS18,NADRESS18_quant.); label NADRESS18_quant="NB CHGT ADRESSE 18 MOIS";

*------------------------------------------------------------*;
* Computed Code;
*------------------------------------------------------------*;


		*------------------------------------------------------------*;
		* TRANSFORM: MACT12 , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_MACT12 = 'Transformed MACT12';
		length OPT_MACT12 $19;
		if (MACT12 < 0.5) then
		OPT_MACT12 = "01:low -0.5, MISSING";
		else
		if (MACT12 >= 0.5 and MACT12 < 14.5) then
		OPT_MACT12 = "02:0.5-14.5";
		else
		if (MACT12 >= 14.5) then
		OPT_MACT12 = "03:14.5-high";

		*------------------------------------------------------------*;
		* TRANSFORM: NMODLOY18 , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_NMODLOY18 = 'Transformed: NB MOD MT LOY EN 18 MOIS';
		length OPT_NMODLOY18 $19;
		if (NMODLOY18 < 0.5) then
		OPT_NMODLOY18 = "01:low -0.5, MISSING";
		else
		if (NMODLOY18 >= 0.5 and NMODLOY18 < 4.5) then
		OPT_NMODLOY18 = "02:0.5-4.5";
		else
		if (NMODLOY18 >= 4.5) then
		OPT_NMODLOY18 = "03:4.5-high";


		*------------------------------------------------------------*;
		* TRANSFORM: FGORINET12 , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_FGORINET12 = 'Transformed FGORINET12';
		length OPT_FGORINET12 $19;
		if (FGORINET12 < 0.5) then
		OPT_FGORINET12 = "01:low -0.5, MISSING";
		else
		if (FGORINET12 >= 0.5 and FGORINET12 < 2.5) then
		OPT_FGORINET12 = "02:0.5-2.5";
		else
		if (FGORINET12 >= 2.5 and FGORINET12 < 5.5) then
		OPT_FGORINET12 = "03:2.5-5.5";
		else
		if (FGORINET12 >= 5.5) then
		OPT_FGORINET12 = "04:5.5-high";

		*------------------------------------------------------------*;
		* TRANSFORM: FGORIREC18 , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_FGORIREC18 = 'Transformed FGORIREC18';
		length OPT_FGORIREC18 $19;
		if (FGORIREC18 < 0.5) then
		OPT_FGORIREC18 = "01:low -0.5, MISSING";
		else
		if (FGORIREC18 >= 0.5) then
		OPT_FGORIREC18 = "02:0.5-high";



		*------------------------------------------------------------*;
		* TRANSFORM: MOIFGRESAN , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_MOIFGRESAN = 'Transformed MOIFGRESAN';
		length OPT_MOIFGRESAN $11;
		if (MOIFGRESAN eq .) then OPT_MOIFGRESAN='_MISSING_';
		else
		if (MOIFGRESAN < 0.5) then
		OPT_MOIFGRESAN = "01:low -0.5";
		else
		if (MOIFGRESAN >= 0.5 and MOIFGRESAN < 4.5) then
		OPT_MOIFGRESAN = "02:0.5-4.5";
		else
		if (MOIFGRESAN eq . ) or (MOIFGRESAN >= 4.5) then
		OPT_MOIFGRESAN = "03:4.5-high, MISSING";



		*------------------------------------------------------------*;
		* TRANSFORM: MOIONTAVU , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_MOIONTAVU = 'Transformed MOIONTAVU';
		length OPT_MOIONTAVU $19;
		if (MOIONTAVU < 0.5) then
		OPT_MOIONTAVU = "01:low -0.5, MISSING";
		else
		if (MOIONTAVU >= 0.5 and MOIONTAVU < 1.5) then
		OPT_MOIONTAVU = "02:0.5-1.5";
		else
		if (MOIONTAVU >= 1.5) then
		OPT_MOIONTAVU = "03:1.5-high";


		*------------------------------------------------------------*;
		* TRANSFORM: MOIPIE_DSIT , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_MOIPIE_DSIT = 'Transformed MOIPIE_DSIT';
		length OPT_MOIPIE_DSIT $19;
		if (MOIPIE_DSIT < 5.5) then
		OPT_MOIPIE_DSIT = "01:low -5.5, MISSING";
		else
		if (MOIPIE_DSIT >= 5.5) then
		OPT_MOIPIE_DSIT = "02:5.5-high";


		*------------------------------------------------------------*;
		* TRANSFORM: MOIPIE_MAIL , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_MOIPIE_MAIL = 'Transformed MOIPIE_MAIL';
		length OPT_MOIPIE_MAIL $19;
		if (MOIPIE_MAIL < 2.5) then
		OPT_MOIPIE_MAIL = "01:low -2.5, MISSING";
		else
		if (MOIPIE_MAIL >= 2.5 and MOIPIE_MAIL < 4.5) then
		OPT_MOIPIE_MAIL = "02:2.5-4.5";
		else
		if (MOIPIE_MAIL >= 4.5) then
		OPT_MOIPIE_MAIL = "03:4.5-high";


		*------------------------------------------------------------*;
		* TRANSFORM: NBPIE_DSIT18 , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_NBPIE_DSIT18 = 'Transformed NBPIE_DSIT18';
		length OPT_NBPIE_DSIT18 $19;
		if (NBPIE_DSIT18 < 0.5) then
		OPT_NBPIE_DSIT18 = "01:low -0.5, MISSING";
		else
		if (NBPIE_DSIT18 >= 0.5) then
		OPT_NBPIE_DSIT18 = "02:0.5-high";

		*------------------------------------------------------------*;
		* TRANSFORM: NBPIE_MAIL18 , Optimal Binning(4);
		*------------------------------------------------------------*;
		label OPT_NBPIE_MAIL18 = 'Transformed NBPIE_MAIL18';
		length OPT_NBPIE_MAIL18 $19;
		if (NBPIE_MAIL18 < 1.5) then
		OPT_NBPIE_MAIL18 = "01:low -1.5, MISSING";
		else
		if (NBPIE_MAIL18 >= 1.5) then
OPT_NBPIE_MAIL18 = "02:1.5-high";

RUN;

data newtableB_scoree_&m1.;
set final2 (Keep = MATRICUL NORDALLC NUMCAF MS 	NADRESS18_quant	OPT_MACT12	OPT_NMODLOY18	OPT_MACT12	FGRESTRI18_quant	
				OPT_FGORINET12	OPT_FGORIREC18					
				OPT_MOIFGRESAN	MOICONTVER_quant	
				NBWEB18_quant	OPT_MOIONTAVU	MOIPIE_NOTINT_quant	
				MOIPIE_NPAI_quant	OPT_MOIPIE_DSIT		
				OPT_MOIPIE_MAIL		OPT_NBPIE_DSIT18	OPT_NBPIE_MAIL18	
				ABANEU	agecon_quant	ageresp_quant		MOINSCDO_quant	
				MTPAJCAV_quant	NBM_ODMS_quant	RUC_RED_quant	STATUETU	TITUBANC	
				topen19	topen19	TOPENF1218_P	tut_P	TXEFF_P	TYPAAH	
				COMPANTRIM	SEQACR	SEQFAM	);
run;


/********************************************************************************/
/********************************************************************************/
/*				DERNIERE ETAPE : SCORING 										*/
/********************************************************************************/
/********************************************************************************/

/*MODIF DSER octobre 2014 : Nouveaux param�trages de scoring apr�s nouvelle mod�lisation
Le nom de la variable cible change*/

%macro scoreglob2;
*************************************;
*** begin scoring code for regression;
*************************************;

length _WARN_ $4;
label _WARN_ = 'Warnings' ;

length I_cible $ 13;
label I_cible = 'Into: cible' ;
*** Target Values;
array REGDRF [2] $13 _temporary_ ('INDSOLD4M200E' '0REGUL' );
label U_cible = 'Unnormalized Into: cible' ;
length U_cible $ 13;
*** Unnormalized target values;
array REGDRU[2] $ 13 _temporary_ ('INDSOLD4M200E'  '0REGUL       ' );

drop _DM_BAD;
_DM_BAD=0;

*** Check topen19 for missing values ;
if missing( topen19 ) then do;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;

*** Generate dummy variables for ABANEU ;
drop _1_0 ;
if missing( ABANEU ) then do;
   _1_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm15 $ 15; drop _dm15 ;
   %DMNORMCP( ABANEU , _dm15 )
   if _dm15 = 'AUCUN'  then do;
      _1_0 = -1;
   end;
   else if _dm15 = 'AB. OU NEUTRAL.'  then do;
      _1_0 = 1;
   end;
   else do;
      _1_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for COMPANTRIM ;
drop _2_0 _2_1 _2_2 _2_3 ;
*** encoding is sparse, initialize to zero;
_2_0 = 0;
_2_1 = 0;
_2_2 = 0;
_2_3 = 0;
if missing( COMPANTRIM ) then do;
   _2_0 = .;
   _2_1 = .;
   _2_2 = .;
   _2_3 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm7 $ 7; drop _dm7 ;
   %DMNORMCP( COMPANTRIM , _dm7 )
   if _dm7 = 'AUTRE'  then do;
      _2_0 = 1;
   end;
   else if _dm7 = 'STABLE0'  then do;
      _2_0 = -1;
      _2_1 = -1;
      _2_2 = -1;
      _2_3 = -1;
   end;
   else if _dm7 = 'BAISSE'  then do;
      _2_1 = 1;
   end;
   else if _dm7 = 'STABLE'  then do;
      _2_3 = 1;
   end;
   else if _dm7 = 'HAUSSE'  then do;
      _2_2 = 1;
   end;
   else do;
      _2_0 = .;
      _2_1 = .;
      _2_2 = .;
      _2_3 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for FGRESTRI18_quant ;
drop _3_0 _3_1 _3_2 ;
if missing( FGRESTRI18_quant ) then do;
   _3_0 = .;
   _3_1 = .;
   _3_2 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm7 $ 7; drop _dm7 ;
   %DMNORMCP( FGRESTRI18_quant , _dm7 )
   if _dm7 = '0 � <1'  then do;
      _3_0 = 1;
      _3_1 = 0;
      _3_2 = 0;
   end;
   else if _dm7 = '1 � <7'  then do;
      _3_0 = 0;
      _3_1 = 1;
      _3_2 = 0;
   end;
   else if _dm7 = '7 � <11'  then do;
      _3_0 = -1;
      _3_1 = -1;
      _3_2 = -1;
   end;
   else if _dm7 = '11 � +'  then do;
      _3_0 = 0;
      _3_1 = 0;
      _3_2 = 1;
   end;
   else do;
      _3_0 = .;
      _3_1 = .;
      _3_2 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for MOICONTVER_quant ;
drop _4_0 _4_1 _4_2 _4_3 ;
*** encoding is sparse, initialize to zero;
_4_0 = 0;
_4_1 = 0;
_4_2 = 0;
_4_3 = 0;
if missing( MOICONTVER_quant ) then do;
   _4_0 = .;
   _4_1 = .;
   _4_2 = .;
   _4_3 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm9 $ 9; drop _dm9 ;
   %DMNORMCP( MOICONTVER_quant , _dm9 )
   if _dm9 = 'MISSAUT'  then do;
      _4_0 = -1;
      _4_1 = -1;
      _4_2 = -1;
      _4_3 = -1;
   end;
   else if _dm9 = '7 � <13'  then do;
      _4_3 = 1;
   end;
   else if _dm9 = '4 � <7'  then do;
      _4_2 = 1;
   end;
   else if _dm9 = '13 � <=18'  then do;
      _4_1 = 1;
   end;
   else if _dm9 = '1 � <4'  then do;
      _4_0 = 1;
   end;
   else do;
      _4_0 = .;
      _4_1 = .;
      _4_2 = .;
      _4_3 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for MOINSCDO_quant ;
drop _5_0 _5_1 ;
if missing( MOINSCDO_quant ) then do;
   _5_0 = .;
   _5_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm10 $ 10; drop _dm10 ;
   %DMNORMCP( MOINSCDO_quant , _dm10 )
   if _dm10 = '49 ET PLUS'  then do;
      _5_0 = -1;
      _5_1 = -1;
   end;
   else if _dm10 = '12 � <49'  then do;
      _5_0 = 0;
      _5_1 = 1;
   end;
   else if _dm10 = '0 � <12'  then do;
      _5_0 = 1;
      _5_1 = 0;
   end;
   else do;
      _5_0 = .;
      _5_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for MOIPIE_NOTINT_quant ;
drop _6_0 ;
if missing( MOIPIE_NOTINT_quant ) then do;
   _6_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm7 $ 7; drop _dm7 ;
   %DMNORMCP( MOIPIE_NOTINT_quant , _dm7 )
   if _dm7 = 'MISSAUT'  then do;
      _6_0 = -1;
   end;
   else if _dm7 = '5 � 18'  then do;
      _6_0 = 1;
   end;
   else do;
      _6_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for MOIPIE_NPAI_quant ;
drop _7_0 _7_1 ;
if missing( MOIPIE_NPAI_quant ) then do;
   _7_0 = .;
   _7_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm7 $ 7; drop _dm7 ;
   %DMNORMCP( MOIPIE_NPAI_quant , _dm7 )
   if _dm7 = '00'  then do;
      _7_0 = 1;
      _7_1 = 0;
   end;
   else if _dm7 = '05 � 18'  then do;
      _7_0 = 0;
      _7_1 = 1;
   end;
   else if _dm7 = '1 � <5'  then do;
      _7_0 = -1;
      _7_1 = -1;
   end;
   else do;
      _7_0 = .;
      _7_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for MTPAJCAV_quant ;
drop _8_0 _8_1 ;
if missing( MTPAJCAV_quant ) then do;
   _8_0 = .;
   _8_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm7 $ 7; drop _dm7 ;
   %DMNORMCP( MTPAJCAV_quant , _dm7 )
   if _dm7 = '0'  then do;
      _8_0 = 1;
      _8_1 = 0;
   end;
   else if _dm7 = '361 ET+'  then do;
      _8_0 = -1;
      _8_1 = -1;
   end;
   else if _dm7 = '0 � 361'  then do;
      _8_0 = 0;
      _8_1 = 1;
   end;
   else do;
      _8_0 = .;
      _8_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for NADRESS18_quant ;
drop _9_0 _9_1 ;
if missing( NADRESS18_quant ) then do;
   _9_0 = .;
   _9_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm7 $ 7; drop _dm7 ;
   %DMNORMCP( NADRESS18_quant , _dm7 )
   if _dm7 = '0 � <1'  then do;
      _9_0 = 1;
      _9_1 = 0;
   end;
   else if _dm7 = '4 � +'  then do;
      _9_0 = -1;
      _9_1 = -1;
   end;
   else if _dm7 = '1 � <4'  then do;
      _9_0 = 0;
      _9_1 = 1;
   end;
   else do;
      _9_0 = .;
      _9_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for NBM_ODMS_quant ;
drop _10_0 _10_1 _10_2 ;
if missing( NBM_ODMS_quant ) then do;
   _10_0 = .;
   _10_1 = .;
   _10_2 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm8 $ 8; drop _dm8 ;
   %DMNORMCP( NBM_ODMS_quant , _dm8 )
   if _dm8 = '0 � <1'  then do;
      _10_0 = 1;
      _10_1 = 0;
      _10_2 = 0;
   end;
   else if _dm8 = '32 � 500'  then do;
      _10_0 = -1;
      _10_1 = -1;
      _10_2 = -1;
   end;
   else if _dm8 = '13 � <32'  then do;
      _10_0 = 0;
      _10_1 = 0;
      _10_2 = 1;
   end;
   else if _dm8 = '1 � <13'  then do;
      _10_0 = 0;
      _10_1 = 1;
      _10_2 = 0;
   end;
   else do;
      _10_0 = .;
      _10_1 = .;
      _10_2 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for NBWEB18_quant ;
drop _11_0 _11_1 _11_2 ;
if missing( NBWEB18_quant ) then do;
   _11_0 = .;
   _11_1 = .;
   _11_2 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm10 $ 10; drop _dm10 ;
   %DMNORMCP( NBWEB18_quant , _dm10 )
   if _dm10 = '0 � <4'  then do;
      _11_0 = 1;
      _11_1 = 0;
      _11_2 = 0;
   end;
   else if _dm10 = '4 � <12'  then do;
      _11_0 = -1;
      _11_1 = -1;
      _11_2 = -1;
   end;
   else if _dm10 = '12 � <34'  then do;
      _11_0 = 0;
      _11_1 = 1;
      _11_2 = 0;
   end;
   else if _dm10 = '34 � <=504'  then do;
      _11_0 = 0;
      _11_1 = 0;
      _11_2 = 1;
   end;
   else do;
      _11_0 = .;
      _11_1 = .;
      _11_2 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;


*** Generate dummy variables for OPT_FGORINET12 ;
drop _14_0 _14_1 _14_2 ;
if missing( OPT_FGORINET12 ) then do;
   _14_0 = .;
   _14_1 = .;
   _14_2 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm19 $ 19; drop _dm19 ;
   %DMNORMCP( OPT_FGORINET12 , _dm19 )
   if _dm19 = '01:LOW -0.5, MISSIN'  then do;
      _14_0 = 1;
      _14_1 = 0;
      _14_2 = 0;
   end;
   else if _dm19 = '02:0.5-2.5'  then do;
      _14_0 = 0;
      _14_1 = 1;
      _14_2 = 0;
   end;
   else if _dm19 = '03:2.5-5.5'  then do;
      _14_0 = 0;
      _14_1 = 0;
      _14_2 = 1;
   end;
   else if _dm19 = '04:5.5-HIGH'  then do;
      _14_0 = -1;
      _14_1 = -1;
      _14_2 = -1;
   end;
   else do;
      _14_0 = .;
      _14_1 = .;
      _14_2 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for OPT_FGORIREC18 ;
drop _15_0 ;
if missing( OPT_FGORIREC18 ) then do;
   _15_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm19 $ 19; drop _dm19 ;
   %DMNORMCP( OPT_FGORIREC18 , _dm19 )
   if _dm19 = '01:LOW -0.5, MISSIN'  then do;
      _15_0 = 1;
   end;
   else if _dm19 = '02:0.5-HIGH'  then do;
      _15_0 = -1;
   end;
   else do;
      _15_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for OPT_MACT12 ;
drop _16_0 _16_1 ;
if missing( OPT_MACT12 ) then do;
   _16_0 = .;
   _16_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm19 $ 19; drop _dm19 ;
   %DMNORMCP( OPT_MACT12 , _dm19 )
   if _dm19 = '01:LOW -0.5, MISSIN'  then do;
      _16_0 = 1;
      _16_1 = 0;
   end;
   else if _dm19 = '02:0.5-14.5'  then do;
      _16_0 = 0;
      _16_1 = 1;
   end;
   else if _dm19 = '03:14.5-HIGH'  then do;
      _16_0 = -1;
      _16_1 = -1;
   end;
   else do;
      _16_0 = .;
      _16_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for OPT_MOIFGRESAN ;
drop _17_0 _17_1 ;
if missing( OPT_MOIFGRESAN ) then do;
   _17_0 = .;
   _17_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm11 $ 11; drop _dm11 ;
   %DMNORMCP( OPT_MOIFGRESAN , _dm11 )
   if _dm11 = '03:4.5-HIGH'  then do;
      _17_0 = -1;
      _17_1 = -1;
   end;
   else if _dm11 = '02:0.5-4.5'  then do;
      _17_0 = 0;
      _17_1 = 1;
   end;
   else if _dm11 = '01:LOW -0.5'  then do;
      _17_0 = 1;
      _17_1 = 0;
   end;
   else do;
      _17_0 = .;
      _17_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for OPT_MOIONTAVU ;
drop _18_0 _18_1 ;
if missing( OPT_MOIONTAVU ) then do;
   _18_0 = .;
   _18_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm19 $ 19; drop _dm19 ;
   %DMNORMCP( OPT_MOIONTAVU , _dm19 )
   if _dm19 = '01:LOW -0.5, MISSIN'  then do;
      _18_0 = 1;
      _18_1 = 0;
   end;
   else if _dm19 = '03:1.5-HIGH'  then do;
      _18_0 = -1;
      _18_1 = -1;
   end;
   else if _dm19 = '02:0.5-1.5'  then do;
      _18_0 = 0;
      _18_1 = 1;
   end;
   else do;
      _18_0 = .;
      _18_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for OPT_MOIPIE_DSIT ;
drop _19_0 ;
if missing( OPT_MOIPIE_DSIT ) then do;
   _19_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm19 $ 19; drop _dm19 ;
   %DMNORMCP( OPT_MOIPIE_DSIT , _dm19 )
   if _dm19 = '01:LOW -5.5, MISSIN'  then do;
      _19_0 = 1;
   end;
   else if _dm19 = '02:5.5-HIGH'  then do;
      _19_0 = -1;
   end;
   else do;
      _19_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for OPT_MOIPIE_MAIL ;
drop _20_0 _20_1 ;
if missing( OPT_MOIPIE_MAIL ) then do;
   _20_0 = .;
   _20_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm19 $ 19; drop _dm19 ;
   %DMNORMCP( OPT_MOIPIE_MAIL , _dm19 )
   if _dm19 = '01:LOW -2.5, MISSIN'  then do;
      _20_0 = 1;
      _20_1 = 0;
   end;
   else if _dm19 = '03:4.5-HIGH'  then do;
      _20_0 = -1;
      _20_1 = -1;
   end;
   else if _dm19 = '02:2.5-4.5'  then do;
      _20_0 = 0;
      _20_1 = 1;
   end;
   else do;
      _20_0 = .;
      _20_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for OPT_NBPIE_DSIT18 ;
drop _21_0 ;
if missing( OPT_NBPIE_DSIT18 ) then do;
   _21_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm19 $ 19; drop _dm19 ;
   %DMNORMCP( OPT_NBPIE_DSIT18 , _dm19 )
   if _dm19 = '01:LOW -0.5, MISSIN'  then do;
      _21_0 = 1;
   end;
   else if _dm19 = '02:0.5-HIGH'  then do;
      _21_0 = -1;
   end;
   else do;
      _21_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for OPT_NBPIE_MAIL18 ;
drop _22_0 ;
if missing( OPT_NBPIE_MAIL18 ) then do;
   _22_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm19 $ 19; drop _dm19 ;
   %DMNORMCP( OPT_NBPIE_MAIL18 , _dm19 )
   if _dm19 = '01:LOW -1.5, MISSIN'  then do;
      _22_0 = 1;
   end;
   else if _dm19 = '02:1.5-HIGH'  then do;
      _22_0 = -1;
   end;
   else do;
      _22_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for OPT_NMODLOY18 ;
drop _23_0 _23_1 ;
if missing( OPT_NMODLOY18 ) then do;
   _23_0 = .;
   _23_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm19 $ 19; drop _dm19 ;
   %DMNORMCP( OPT_NMODLOY18 , _dm19 )
   if _dm19 = '01:LOW -0.5, MISSIN'  then do;
      _23_0 = 1;
      _23_1 = 0;
   end;
   else if _dm19 = '02:0.5-4.5'  then do;
      _23_0 = 0;
      _23_1 = 1;
   end;
   else if _dm19 = '03:4.5-HIGH'  then do;
      _23_0 = -1;
      _23_1 = -1;
   end;
   else do;
      _23_0 = .;
      _23_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for RUC_RED_quant ;
drop _24_0 _24_1 _24_2 ;
if missing( RUC_RED_quant ) then do;
   _24_0 = .;
   _24_1 = .;
   _24_2 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm17 $ 17; drop _dm17 ;
   %DMNORMCP( RUC_RED_quant , _dm17 )
   if _dm17 = '0 � <577'  then do;
      _24_0 = 1;
      _24_1 = 0;
      _24_2 = 0;
   end;
   else if _dm17 = '942 � <1483.33'  then do;
      _24_0 = -1;
      _24_1 = -1;
      _24_2 = -1;
   end;
   else if _dm17 = '577 � <942'  then do;
      _24_0 = 0;
      _24_1 = 0;
      _24_2 = 1;
   end;
   else if _dm17 = '1483.33 � <=19745'  then do;
      _24_0 = 0;
      _24_1 = 1;
      _24_2 = 0;
   end;
   else do;
      _24_0 = .;
      _24_1 = .;
      _24_2 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for SEQACR ;
drop _25_0 _25_1 _25_2 ;
if missing( SEQACR ) then do;
   _25_0 = .;
   _25_1 = .;
   _25_2 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm9 $ 9; drop _dm9 ;
   %DMNORMCP( SEQACR , _dm9 )
   if _dm9 = 'ACT_X__X_'  then do;
      _25_0 = 0;
      _25_1 = 1;
      _25_2 = 0;
   end;
   else if _dm9 = '????????'  then do;
      _25_0 = 1;
      _25_1 = 0;
      _25_2 = 0;
   end;
   else if _dm9 = '_-__-____'  then do;
      _25_0 = -1;
      _25_1 = -1;
      _25_2 = -1;
   end;
   else if _dm9 = '_+__-____'  then do;
      _25_0 = 0;
      _25_1 = 0;
      _25_2 = 1;
   end;
   else do;
      _25_0 = .;
      _25_1 = .;
      _25_2 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;


*** Generate dummy variables for SEQFAM ;
drop _27_0 _27_1 _27_2 _27_3 _27_4 ;
*** encoding is sparse, initialize to zero;
_27_0 = 0;
_27_1 = 0;
_27_2 = 0;
_27_3 = 0;
_27_4 = 0;
if missing( SEQFAM ) then do;
   _27_0 = .;
   _27_1 = .;
   _27_2 = .;
   _27_3 = .;
   _27_4 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm5 $ 5; drop _dm5 ;
   %DMNORMCP( SEQFAM , _dm5 )
   if _dm5 = 'RISQ1'  then do;
      _27_4 = 1;
   end;
   else if _dm5 = '23XXX'  then do;
      _27_0 = 1;
   end;
   else if _dm5 = '4_XXX'  then do;
      _27_2 = 1;
   end;
   else if _dm5 = '7_XXX'  then do;
      _27_3 = 1;
   end;
   else if _dm5 = '456XX'  then do;
      _27_1 = 1;
   end;
   else if _dm5 = '__X__'  then do;
      _27_0 = -1;
      _27_1 = -1;
      _27_2 = -1;
      _27_3 = -1;
      _27_4 = -1;
   end;
   else do;
      _27_0 = .;
      _27_1 = .;
      _27_2 = .;
      _27_3 = .;
      _27_4 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for STATUETU ;
drop _28_0 _28_1 ;
if missing( STATUETU ) then do;
   _28_0 = .;
   _28_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm1 $ 1; drop _dm1 ;
   %DMNORMCP( STATUETU , _dm1 )
   if _dm1 = '0'  then do;
      _28_0 = 1;
      _28_1 = 0;
   end;
   else if _dm1 = '1'  then do;
      _28_0 = 0;
      _28_1 = 1;
   end;
   else if _dm1 = '3'  then do;
      _28_0 = -1;
      _28_1 = -1;
   end;
   else do;
      _28_0 = .;
      _28_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for TITUBANC ;
drop _29_0 _29_1 ;
if missing( TITUBANC ) then do;
   _29_0 = .;
   _29_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm1 $ 1; drop _dm1 ;
   %DMNORMCP( TITUBANC , _dm1 )
   if _dm1 = '1'  then do;
      _29_0 = 1;
      _29_1 = 0;
   end;
   else if _dm1 = '2'  then do;
      _29_0 = 0;
      _29_1 = 1;
   end;
   else if _dm1 = '4'  then do;
      _29_0 = -1;
      _29_1 = -1;
   end;
   else do;
      _29_0 = .;
      _29_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for TOPENF1218_P ;
drop _30_0 ;
if missing( TOPENF1218_P ) then do;
   _30_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm1 $ 1; drop _dm1 ;
   %DMNORMCP( TOPENF1218_P , _dm1 )
   if _dm1 = '0'  then do;
      _30_0 = 1;
   end;
   else if _dm1 = '1'  then do;
      _30_0 = -1;
   end;
   else do;
      _30_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for TXEFF_P ;
drop _31_0 ;
if missing( TXEFF_P ) then do;
   _31_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm15 $ 15; drop _dm15 ;
   %DMNORMCP( TXEFF_P , _dm15 )
   if _dm15 = '0ALETTXEFF 0A35'  then do;
      _31_0 = 1;
   end;
   else if _dm15 = '35ETPLUS'  then do;
      _31_0 = -1;
   end;
   else do;
      _31_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for TYPAAH ;
drop _32_0 _32_1 ;
if missing( TYPAAH ) then do;
   _32_0 = .;
   _32_1 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm1 $ 1; drop _dm1 ;
   %DMNORMCP( TYPAAH , _dm1 )
   if _dm1 = '0'  then do;
      _32_0 = 1;
      _32_1 = 0;
   end;
   else if _dm1 = '2'  then do;
      _32_0 = -1;
      _32_1 = -1;
   end;
   else if _dm1 = '1'  then do;
      _32_0 = 0;
      _32_1 = 1;
   end;
   else do;
      _32_0 = .;
      _32_1 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for agecon_quant ;
drop _33_0 ;
if missing( agecon_quant ) then do;
   _33_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm10 $ 10; drop _dm10 ;
   %DMNORMCP( agecon_quant , _dm10 )
   if _dm10 = 'MISSAUT'  then do;
      _33_0 = -1;
   end;
   else if _dm10 = '60 ET PLUS'  then do;
      _33_0 = 1;
   end;
   else do;
      _33_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for ageresp_quant ;
drop _34_0 ;
if missing( ageresp_quant ) then do;
   _34_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm7 $ 7; drop _dm7 ;
   %DMNORMCP( ageresp_quant , _dm7 )
   if _dm7 = 'MISSAUT'  then do;
      _34_0 = -1;
   end;
   else if _dm7 = '00 � 33'  then do;
      _34_0 = 1;
   end;
   else do;
      _34_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** Generate dummy variables for tut_P ;
drop _35_0 ;
if missing( tut_P ) then do;
   _35_0 = .;
   substr(_warn_,1,1) = 'M';
   _DM_BAD = 1;
end;
else do;
   length _dm7 $ 7; drop _dm7 ;
   %DMNORMCP( tut_P , _dm7 )
   if _dm7 = '0'  then do;
      _35_0 = 1;
   end;
   else if _dm7 = 'TUTELLE'  then do;
      _35_0 = -1;
   end;
   else do;
      _35_0 = .;
      substr(_warn_,2,1) = 'U';
      _DM_BAD = 1;
   end;
end;

*** If missing inputs, use averages;
if _DM_BAD > 0 then do;
   _P0 = 0.110033884;
   _P1 = 0.889966116;
   goto REGDR1;
end;

*** Compute Linear Predictor;
drop _TEMP;
drop _LP0 ;
_LP0 = 0;

***  Effect: ABANEU ;
_TEMP = 1;
_LP0 = _LP0 + (    0.23232764382303) * _TEMP * _1_0;

***  Effect: COMPANTRIM ;
_TEMP = 1;
_LP0 = _LP0 + (     0.2213879461932) * _TEMP * _2_0;
_LP0 = _LP0 + (   -0.23758864170547) * _TEMP * _2_1;
_LP0 = _LP0 + (    0.10014517695226) * _TEMP * _2_2;
_LP0 = _LP0 + (    0.28214016541774) * _TEMP * _2_3;

***  Effect: FGRESTRI18_quant ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.25528218660984) * _TEMP * _3_0;
_LP0 = _LP0 + (   -0.03407843771333) * _TEMP * _3_1;
_LP0 = _LP0 + (    0.24828745194934) * _TEMP * _3_2;

***  Effect: MOICONTVER_quant ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.21768333198141) * _TEMP * _4_0;
_LP0 = _LP0 + (    0.09656226810839) * _TEMP * _4_1;
_LP0 = _LP0 + (     0.3753744654792) * _TEMP * _4_2;
_LP0 = _LP0 + (   -0.21053899338984) * _TEMP * _4_3;

***  Effect: MOINSCDO_quant ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.37223848913268) * _TEMP * _5_0;
_LP0 = _LP0 + (    0.22711099427676) * _TEMP * _5_1;

***  Effect: MOIPIE_NOTINT_quant ;
_TEMP = 1;
_LP0 = _LP0 + (     0.4409911195616) * _TEMP * _6_0;

***  Effect: MOIPIE_NPAI_quant ;
_TEMP = 1;
_LP0 = _LP0 + (    0.10801749528109) * _TEMP * _7_0;
_LP0 = _LP0 + (    0.30748094150248) * _TEMP * _7_1;

***  Effect: MTPAJCAV_quant ;
_TEMP = 1;
_LP0 = _LP0 + (    0.19975598506804) * _TEMP * _8_0;
_LP0 = _LP0 + (    0.02474201858734) * _TEMP * _8_1;

***  Effect: NADRESS18_quant ;
_TEMP = 1;
_LP0 = _LP0 + (    0.32426185988357) * _TEMP * _9_0;
_LP0 = _LP0 + (   -0.45225170547949) * _TEMP * _9_1;

***  Effect: NBM_ODMS_quant ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.39791200043493) * _TEMP * _10_0;
_LP0 = _LP0 + (   -0.20817360952199) * _TEMP * _10_1;
_LP0 = _LP0 + (    0.32033067809115) * _TEMP * _10_2;

***  Effect: NBWEB18_quant ;
_TEMP = 1;
_LP0 = _LP0 + (    0.02477883999277) * _TEMP * _11_0;
_LP0 = _LP0 + (   -0.11336860212259) * _TEMP * _11_1;
_LP0 = _LP0 + (   -0.05326803815191) * _TEMP * _11_2;

***  Effect: OPT_FGORINET12 ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.16773363709676) * _TEMP * _14_0;
_LP0 = _LP0 + (    0.01415184255654) * _TEMP * _14_1;
_LP0 = _LP0 + (   -0.13428969940581) * _TEMP * _14_2;

***  Effect: OPT_FGORIREC18 ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.15511757943741) * _TEMP * _15_0;

***  Effect: OPT_MACT12 ;
_TEMP = 1;
_LP0 = _LP0 + (    0.27730925279732) * _TEMP * _16_0;
_LP0 = _LP0 + (    0.32522331141171) * _TEMP * _16_1;

***  Effect: OPT_MOIFGRESAN ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.81202850635466) * _TEMP * _17_0;
_LP0 = _LP0 + (    0.61923386123076) * _TEMP * _17_1;

***  Effect: OPT_MOIONTAVU ;
_TEMP = 1;
_LP0 = _LP0 + (    0.13627815941635) * _TEMP * _18_0;
_LP0 = _LP0 + (   -0.04374801754763) * _TEMP * _18_1;

***  Effect: OPT_MOIPIE_DSIT ;
_TEMP = 1;
_LP0 = _LP0 + (    -0.3597481037668) * _TEMP * _19_0;
***  Effect: OPT_MOIPIE_MAIL ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.15537483693343) * _TEMP * _20_0;
_LP0 = _LP0 + (    0.37315035245438) * _TEMP * _20_1;

***  Effect: OPT_NBPIE_DSIT18 ;
_TEMP = 1;
_LP0 = _LP0 + (    0.29525355588199) * _TEMP * _21_0;

***  Effect: OPT_NBPIE_MAIL18 ;
_TEMP = 1;
_LP0 = _LP0 + (    0.13623828752591) * _TEMP * _22_0;

***  Effect: OPT_NMODLOY18 ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.54676525776417) * _TEMP * _23_0;
_LP0 = _LP0 + (   -0.31601088054791) * _TEMP * _23_1;

***  Effect: RUC_RED_quant ;
_TEMP = 1;
_LP0 = _LP0 + (    0.33484019169728) * _TEMP * _24_0;
_LP0 = _LP0 + (   -0.79175797044132) * _TEMP * _24_1;
_LP0 = _LP0 + (    0.43094626466292) * _TEMP * _24_2;

***  Effect: SEQACR ;
_TEMP = 1;
_LP0 = _LP0 + (    0.11009895761839) * _TEMP * _25_0;
_LP0 = _LP0 + (    0.01494576600753) * _TEMP * _25_1;
_LP0 = _LP0 + (   -0.35411923214215) * _TEMP * _25_2;

***  Effect: SEQFAM ;
_TEMP = 1;
_LP0 = _LP0 + (    0.02688244090902) * _TEMP * _27_0;
_LP0 = _LP0 + (    0.66925439399747) * _TEMP * _27_1;
_LP0 = _LP0 + (   -0.63687561103914) * _TEMP * _27_2;
_LP0 = _LP0 + (   -0.49280197200267) * _TEMP * _27_3;
_LP0 = _LP0 + (      0.125581957456) * _TEMP * _27_4;

***  Effect: STATUETU ;
_TEMP = 1;
_LP0 = _LP0 + (    0.26847813628468) * _TEMP * _28_0;
_LP0 = _LP0 + (   -0.49231217017174) * _TEMP * _28_1;

***  Effect: TITUBANC ;
_TEMP = 1;
_LP0 = _LP0 + (    -0.2042472331848) * _TEMP * _29_0;
_LP0 = _LP0 + (   -0.30962992811117) * _TEMP * _29_1;

***  Effect: TOPENF1218_P ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.08779929863942) * _TEMP * _30_0;

***  Effect: TXEFF_P ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.09635968487988) * _TEMP * _31_0;

***  Effect: TYPAAH ;
_TEMP = 1;
_LP0 = _LP0 + (   -0.21025373930299) * _TEMP * _32_0;
_LP0 = _LP0 + (    0.72818206088848) * _TEMP * _32_1;

***  Effect: agecon_quant ;
_TEMP = 1;
_LP0 = _LP0 + (    0.27722626019663) * _TEMP * _33_0;

***  Effect: ageresp_quant ;
_TEMP = 1;
_LP0 = _LP0 + (    0.11334632521637) * _TEMP * _34_0;

***  Effect: topen19 ;
_TEMP = topen19 ;
_LP0 = _LP0 + (    0.89385926213357 * _TEMP);

***  Effect: tut_P ;
_TEMP = 1;
_LP0 = _LP0 + (    0.60450601197118) * _TEMP * _35_0;

*** Naive Posterior Probabilities;
drop _MAXP _IY _P0 _P1;
drop _LPMAX;
_LPMAX= 0;
_LP0 =    -1.73654727320913 + _LP0;
if _LPMAX < _LP0 then _LPMAX = _LP0;
_LP0 = exp(_LP0 - _LPMAX);
_LPMAX = exp(-_LPMAX);
_P1 = 1 / (_LPMAX + _LP0);
_P0 = _LP0 * _P1;
_P1 = _LPMAX * _P1;

REGDR1:


*** Posterior Probabilities and Predicted Level;
label P_cibleINDSOLD4M200E = 'Predicted: cible=INDSOLD4M200E' ;
label P_cible0REGUL = 'Predicted: cible=0REGUL' ;
P_cibleINDSOLD4M200E = _P0;
_MAXP = _P0;
_IY = 1;
P_cible0REGUL = _P1;
if (_P1 - _MAXP > 1e-8) then do;
   _MAXP = _P1;
   _IY = 2;
end;
I_cible = REGDRF[_IY];
U_cible = REGDRU[_IY];


label SCOREGLOB2="Score mod�le global 2";
SCOREGLOB2=P_cibleINDSOLD4M200E;
WARN_GLOB=_WARN_;
%mend;


*************************************;
*************************************;
*************************************;
*************************************;
*************************************;
***** end scoring code for regression;
*************************************;
*************************************;
*************************************;
*************************************;
*************************************;






data newtableB_scoree_&m1 (keep=numcaf matricul nordallc scoreglob2 scoressrsa2 scoresit2 scoresitpro2 scorelog2 SCOREMAX2
													warn_glob warn_rsa warn_sit warn_sitpro warn_log
				MATRICUL NORDALLC NUMCAF MS 	NADRESS18_quant	OPT_MACT12	OPT_NMODLOY18	OPT_MACT12	FGRESTRI18_quant	
				OPT_FGORINET12	OPT_FGORIREC18		 	
				OPT_MOIFGRESAN	MOICONTVER_quant	
				NBWEB18_quant	OPT_MOIONTAVU	MOIPIE_NOTINT_quant	
				MOIPIE_NPAI_quant	OPT_MOIPIE_DSIT		
				OPT_MOIPIE_MAIL	OPT_NBPIE_DSIT18	OPT_NBPIE_MAIL18	
				ABANEU	agecon_quant	ageresp_quant			MOINSCDO_quant	
				MTPAJCAV_quant	NBM_ODMS_quant	RUC_RED_quant	STATUETU	TITUBANC	
				topen19	topen19	TOPENF1218_P	tut_P	TXEFF_P	TYPAAH	
				COMPANTRIM	SEQACR	SEQFAM	);
	set newtableB_scoree_&m1 ;
%scoreglob2;


run;


data WARNING;
set newtableB_scoree_&m1;
where warn_glob ne "";
run;

proc datasets lib=work nolist; delete FINAL FINAL2; quit; run;


/* MODIF CNEDI POUR INTEGRATION NOUVEAU MODELE DANS FLUX EXISTANT */

/* modif 08/2011 baseloc en work*/
/* tri par scoremax et renomage des score comme les anciens utilis�s dans l'application affinage de al politique de contr�le*/
proc sort data=newtableB_scoree_&m1 ( rename=(scoreglob2=scorglob));
by descending SCORGLOB ;
run;



/*Liste des variables � conserver  */
/*Liste des variables mod�le (sept2014) */
%let frescore2=NUMCAF DTREFFRE DTCRE MATRICUL NORDALLC ABANEU AGECON_quant AGERESP_quant COMPANTRIM
 FGRESTRI18_quant   MOICONTVER_quant  MOINSCDO_quant
MOIPIE_NOTINT_quant MOIPIE_NPAI_quant MTPAJCAV_quant NADRESS18_quant NBM_ODMS_quant  
NBWEB18_quant OPT_FGORINET12 OPT_FGORIREC18  
OPT_MACT12   OPT_MOIFGRESAN 
OPT_MOIONTAVU  OPT_MOIPIE_DSIT  OPT_MOIPIE_MAIL 
OPT_NBPIE_DSIT18 
OPT_NBPIE_MAIL18 OPT_NMODLOY18 RUC_RED_quant SEQACR  SEQFAM  STATUETU TITUBANC 
TOPEN19 TOPENF1218_P TUT_P TXEFF_P TYPAAH warn_glob; 


/*Sortie et azrchivage des tables FRESCORE et FRE du mois de r�f�rence du scoring*/
options compress =  yes ; 

data work.FRESCOR2&m1. (keep= &frescore2.);
set  work.newtableB_scoree_&m1  ;
			DTREFFRE = &mvDTREFFRE. ; 
			DTCRE = date() ; 
		
run;



%mend;

%DM_DE;



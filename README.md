# Algorithme de scoring de la CAF

Nous présentons ici le code source envoyé par la CAF des deux versions de l'algorithme descoring utilisées respectivement entre 2010 et 2014 puis 2014 et 2018 ainsi que le fichier utilisé pour simuler les scores de risques tels que présentés dans [cet article](https://www.laquadrature.net/2023/11/27/notation-des-allocataires-lindecence-des-pratiques-de-la-caf-desormais-indeniable).

Nous présentons ici les détails méthodologiques relatifs à [notre article](https://www.laquadrature.net/2023/11/27/notation-des-allocataires-lindecence-des-pratiques-de-la-caf-desormais-indeniable) présentant le code source de l'algorithme de notation des allocataires de la CAF. 

## Des documents acquis de haute lutte

Cet article se base sur le code source de deux versions de l'algorithme. La première a été utilisée entre 2010 et 2014, la seconde entre 2014 et 2018. 

L'accès à ces deux versions nous a pris plusieurs mois de travail et d'attente. Elles ont été obtenues via des demandes dites CADA, du nom de la [Commission d'Accès aux Documents Administratifs](https://www.cada.fr/). Après plusieurs demandes sans succès faites auprès de la CAF, nous avons saisi la CADA qui a donné un avis favorable à la communication des deux versions antérieures de l'algorithme. Elle nous a par contre refusé l'accès à la version utilisée actuellement, suivant les demandes de la CAF qui arguait que cela aurait pu aider d'éventuels fraudeurs.

Durant ces longs mois, la CAF aura tout essayé pour nous refuser cet accès. Son principal argument : la communication du code source serait une *« atteinte à la sécurité publique »* car *« en identifiant les critères constituant des facteurs de ciblage, des fraudeurs pourraient organiser et monter des dossiers frauduleux »*.

Selon le même argumentaire, elle a d'ailleurs masqué 6 variables du modèle 2010 et 3 du modèle 2014 au motif que ces variables sont encore utilisées dans le modèle actuel. **Un argumentaire nauséabond à deux titres.** Premièrement, la CAF assume pleinement les logiques policières de ses pratiques de contrôle : pour ses dirigeant·es, les allocataires sont devenu·es des *« risques »* dont il faut se protéger à tout prix.

Deuxièment, comme nous l'expliquions [ici](https://www.laquadrature.net/2022/12/23/notation-des-allocataires-febrile-la-caf-senferme-dans-lopacite/), **cet argument est méprisant et insultant. La CAF croit-elle vraiment que des millions d'allocataires précaires - au coeur du ciblage de l'algorithme - pourraient décider, d’un coup, de sortir de la pauvreté afin d’augmenter leur note et de « flouer » l’algorithme ?**


## Les modèles communiqués et leur entraînement

Les modèles 2010 et 2014 sont des modèles de [régression logistique](https://en.wikipedia.org/wiki/Logistic_regression). Le score attribué par le modèle, compris entre zéro et un,  correspond à la probabilité qu'un·e allocataire ait reçu des indus (trop-perçus). Ainsi plus le score est proche de un, plus l'algorithme juge qu'un·e allocataire est suspect·e. Cet algorithme sert ainsi à sélectionner les personnes que la CAF juge indignes de confiance et qui feront l'objet d'un contrôle. Le contrôle est déclenché si la note se dégrade trop, c'est à dire si sa valeur se rapproche de un.

La création de l'algorithme passe par une phase « d'entraînement » lors de laquelle l'algorithme « apprend » à reconnaître des *&*. Cet entraînement se fait sur la base de l'enquête « d’évaluation du paiement à bon droit »[^Collinet]. Cette base comprend plus de 10 000 dossiers ayant fait l'objet d'un contrôle. L'algorithme est entraîné en « comparant » les dossiers pour lesquels des indus ont été détectés et les autres.

Techniquement, l'algorithme est calibré pour cibler le risque d’indus de plus 6 mois et d’un montant supérieur mensuel à 600€.

## Etendue des données personnelles traitées pour l'utilisation de ces modèles

Une remarque importante : dans sa stratégie de communication, la CAF cherche à minimiser l'étendue des données personnelles traitées pour l'utilisation de son algorithme. Dans un <a href=https://www.caf.fr/sites/default/files/medias/371/CNAF%20Supports/Controles%20et%20datamining.pdf>Vrai/Faux</a> assez trompeur, la CAF écrit notamment qu'il serait faux de dire que le modèle utilise plus de « 1000 données concernant les allocataires ». Certes les modèles utilisés contiennent une trentaine de variables. Mais deux choses sont sciemment omises par la CAF.

**Premièrement, l'entraînement du modèle - la sélection des variables retenues - utilise une « base d'apprentissage » contenant plus de 1000 données par allocataire**. C'est un directeur de la lutte contre la fraude de la CAF qui <a href=https://www.cairn.info/revue-informations-sociales-2013-4-page-129.htm>l'explique le mieux</a>: *«Après le travail de récupération des informations, les statisticiens chargés de la modélisation disposaient d’environ un millier d’informations par allocataire contrôlé. Pour rechercher des éléments déterminants qui signalent le risque d’apparition d’un indu, la base d’apprentissage contient toutes les données habituelles des fichiers statistiques, à savoir les informations sur la situation familiale de l’allocataire, son activité, ses ressources et ses droits aux prestations légales versées par les Caf »*. Ainsi le modèle nécessite bien plus de variables que la trentaine retenues à la fin : ces dernières sont une sélection de variables choisies en fonction de leur capacité à prédire les indus.

**Deuxièmement, les variables utilisées dans la formule sont construites à partir d'un nombre bien plus grand de variables comme le montre le [code]().** Un exemple parmi d'autres : pour construire la variable « Nb mois en activité sur 12 mois du responsable et du conjoint », la CAF va aller vérifier mois par mois la variable activité pour deux personnes. Sa construction nécessite donc le traitement de 24 variables. Ceci se répète pour l'ensemble des variables prenant en compte un historique.

**Il est donc faux - et trompeur - d'avancer, comme le fait la CAF dans le Vrai/Faux mentionné ci-avant, que le modèle ne nécessite *« qu'une quarantaine [de variables qui] sont utilisées pour calculer le score de risque. »*.**

## Les variables et leur impact

Le modèle 2010 comprend 37 variables, dont 6 ont été masquées par la CAF, et se décline en 5 sous-modèles : risque familial, de logement, professionnel, global et ressources. Le modèle 2014 comprend un unique modèle et 35 variables dont 3 ont été masquées par la CAF.

Vous trouverez ci-dessous une liste des variables communiquées par la CAF pour chaque modèle, classées par grand types. Pour avoir une idée de leur impact sur la note ainsi que les différentes modalités de ces variables, nous publions aussi les documents envoyés par la CAF présentant les « [odds ratios](https://fr.wikipedia.org/wiki/Odds_ratio) ». Il est à noter cependant que certains calculs d'odds ratios par la CAF semblent erronés. Nous corrigerons ceci dès que possible.

- [Variables et modalités modèle 2010](https://www.laquadrature.net/wp-content/uploads/sites/8/2023/11/modele-2010_variables.xls)
- [Variables et modalités modèle 2014](https://www.laquadrature.net/wp-content/uploads/sites/8/2023/11/modele-2014_variables.xls)

### Variables masquées et ciblage des allocataires vivant dans des quartiers « défavorisés »

Concernant les variables occultées, nous disposons d'informations tierces pouvant nous aider à savoir lesquelles elles sont. Un courrier envoyé par la CNAF à la CADA dans le cadre de notre demande d'accès indique notamment que les informations prises en compte concernent: des « données internes aux Caf relatives à la gestion des dossiers des allocataires, notamment les données relatives aux prestations reçues, les données concernant la gestion du dossier, les éléments sur l’historique du dossier, les déclarations de changement de situation, l’existence d’éventuels contentieux et des caractéristiques sociaux-économiques sur la commune de résidence de l’allocataire ».

Si nous ne pouvons savoir avec certitude si la façon dont cette variable impact le score - puisque nous n'avons pas sa pondération ni ses modalités -, il semble raisonnable de penser qu'elle contribue à une augmentation du score de suspicion et donc de la probabilité d'être contrôlé·e.


## Méthodologie et limites de nos simulations par profils types

Pour la suite on se concentrera sur le modèle 2014 qui est celui à partir duquel a été réalisé l'analyse par profils.

### Limites

Nos profils-types ont été construits à partir du modèle 2014. Nous tenons à être transparent·es sur notre méthodologie et ses limites.

En premier lieu, comme nous l'avons expliqué ci-dessus, 3 variables sont manquantes dans le code source communiqué par la CAF. Les scores calculés ne les prennent donc pas en compte. Deuxièment, pour la construction de ces profils-types, nous avons dû faire des hypothèses fortes pour chacune des variables utilisées par l'algorithme.

Deuxièment, certaines de ces variables sont très techniques et/ou dépendent d'un grand nombre de facteurs et/ou sont dépendantes d'autres variables de l'algorithme. Les variables liées au « Faits générateurs » sont particulièrement difficiles à apprécier. 

Enfin, la lecture du code source n'est pas aisée. Certaines variables - comme celle liée à la séquence familiale (séparation récente...) - sont difficilement compréhensibles à partir du seul fichier fourni par la CAF. 

**Ainsi, il est fort probable que nous ayons fait des erreurs ou que nos profils-types ne soient pas aussi représentatifs que nous le souhaitions. Les scores calculés pour l'analyse par profils-types sont donc donnés à titre indicatif seulement.**

**Il est toutefois à noter que nos analyses sont cohérentes avec les analyses de Vincent Dubois basées sur des statistiques aggrégées (voir le livre « Contrôler les Assistés » ).** La seule différence porte sur les allocataires de l'AAH, dont Vincent Dubois notait un sous-contrôle. Nous tenons à préciser que ceci est vérifié dans nos analyses, mais seulement pour les personnes bénéficiant de l'AAH annuelle. Ce sont pour celles qui travailllent, et bénéficient donc de l'AAH trimestrielle, que nous obtenus des scores relativement élevés, suggérant un sur-contrôles des personnes à l'AAH trimestrielle.  


Vous pouvez nous contacter à algos@laquadrature.net pour toute suggestion/remontée d'erreurs !

### Profils-types

Pour rappel, nos 5 profils sont censés représenter :
1. Une famille « aisée » aux revenus stables et élevés,
2. Une famille « modeste » dont les deux parents gagnent le SMIC,
3. Un parent célibataire gagnant aussi le SMIC,
4. Une famille dont les deux parents sont bénéficiaires des minima sociaux,
5. Une famille dont un des parents est travailleur·se en situation de handicap: pour ce profil, nous simulons le score de la personne bénéficiant de l’AAH trimestrialisée.


Pour chacun de ces profils, nous faisons l'hypothèse que les foyers comprennent deux enfants à charge : un entre 12 et 18 ans et le second ayant plus de 19 ans. Puis, nous faisons des hypothèses concernant les prestations sociales dont bénéficient chacun des profils :
- Les profils 1, 2 et 3 bénéficient uniquement des allocations familiales.
- Dans le profil 4, les deux parents bénéficient du RSA et des allocations familiales
- Dans le profil 5, le parent célibataire bénéficie de l'AAH et des allocations familiales. L'AAH est trimestrialisée puisqu'iel exerce une activité professionnelle.


### Paramètres variant d'un profil à l'autre

Seules 9 variables sont susceptibles de différer entre les profils, les autres étant supposée égales. Nous nous sommes concentré·es sur des différentes variables directement liées aux prestations reçues ou aux revenus. En limitant le nombre de paramètres variant d'un profil à l'autre, nous avons essayé de minimiser notre risque d'erreurs et de nous concentrer sur l'impact des facteurs socio-économiques.

Nous mettons en ligne [ici](https://www.laquadrature.net/wp-content/uploads/sites/8/2023/11/Profils_hypotheses_publi_methodo.xlsx) un fichier détaillant l'ensemble de nos hypothèses, accompagnées d'un commentaire, pour chacun des profils.

Ces variables sont:
- **Nombre faits générateurs (FG) "Ressources Trimestrielles" sur 18 mois** : les FG sont liés aux déclarations de ressources trimestrielles liées aux allocations AAH/RSA/PPA. Pour ces dernières nous nous limitons à 6 FG sur 18 mois, soit les 6 trimestres. Cette hypothèse semble conservatrice (pas d'erreurs, pas de cumul...). Cette variable augmente le score des personnes au RSA/AAH trimestrialisée/PPA.
- **Nombre faits générateurs (FG) d'origine internet sur 12 mois** : on les suppose générés seulement par les déclarations de ressources trimestrielles, égales à 4 donc pour les profils 4 et 5. On exclut donc tout autre fait générateur (naissance, décès, changement activité...) ce qui paraît aussi raisonnable.
- **AAH** : Une variable précisant si l'allocataire bénéficie de l'AAH, de manière annualiseé ou non. Cette variable prend la modalité "AAH trimestrielle" pour le profil 5. 
- **Taux d'effort logement** : Proportion des revenus dédiés au logement. Il est supposé supérieur à 35% pour tous les profils sauf le profil 1. Ceci augmente comparativement le score des autres profils.
- **Revenus par unité de consommation** : Vise à mesurer les revenus par membres du foyer. Sa formule exacte est disponible [ici](https://caf.fr/sites/default/files/medias/698/Nous%20connaitre/stats%20communales/explications_stats_externes2021.pdf). 
- **Délai depuis ouverture des droits au minimum social**: Cette variable est supérieure à zéro pour le profil 4. 
- **Situation familiale sur 18 mois**: La CAF prend en compte la situation familiale (célibataire, marié, pacs...). L'encodage de cette variable est difficilement compréhensible à partir du seul code source fourni par la CAF. Nous nous sommes appuyés sur le fichier présentant les odds ratios envoyé lui aussi par la CAF.
- **Nombre de mois en activité sur 12 mois, responsable du dossier et conjoint**: Cette variable, comprise en 0 et 24, implique que les personnes seules (profil 3) - qui n'auront au maximum que 12 mois d'activité - ou sans travail voient leur score augmenter par rapport aux autres profils. Elle est égale à zéro pour le profil 4, 12 pour les profils 5 et 3 et 24 pour les profils 1 et 2. 
- **Délai depuis l'ouverture de droit au minimum social**: délai depuis l'obtention du RSA.


Nous discutons succintement des différents profils-types ci-après.

### Profil 2: Impact des variables directement liées au revenu

L'augmentation du score de suspicion entre le profil 1 (couple aisé) et le profil 2 (couple au smic) est liée aux variables *« Revenus par unité de consommation »* et *« taux d'effort logement »*. Il illustre l'impact des variables financières.

### Profil 3: Impact des variables liées aux familles monoparentales

L'augmentation du score entre le profil 2 (couple au SMIC) et le profil 3 (parent célibataire au SMIC) est liée aux variables *« Nombre de mois en activité sur 12 mois, responsable du dossier et conjoint »* - égal à 12 pour une personne seule contre 24 pour un couple - et le *« Revenus par unité de consommation »* qui diminue le montant total des revenus du foyer étant plus faible pour le profil 3. Il semble illustrer le ciblage des familles monoparentale.

Une variable non prise en compte est celle liée au délai depuis la séparation. Si la séparation a eu lieu dans les 18 derniers mois, cela devrait augmenter encore le score de suspicion du profil 3.


### Profil 4: Impact des variables liées à l'AAH

Le profil 4 correspond à un allocataire bénéficiant de l'AAH trimestrialisée marié·e. La différence avec le profil 2 (couple au SMIC) est donc le fait de bénéficier de l'AAH. L'augmentation comparative du score entre les deux profils s'explique:
- Par la variable « AAH » qui prend une valeur « AAH trimestrialisée »,
- Par les variables « Nombre faits générateurs (FG) "Ressources Trimestrielles" sur 18 mois » et « Nombre faits générateurs (FG) d'origine internet sur 12 mois ». On suppose ici que la première est fixée à 6, l'allocataire devant déclarer ses ressources trimestriellement et 4 pour la seconde par effet mécanique en supposant que ces déclarations sont faites sur internet. 


### Profil 5: Impact des variables liées au RSA

Le profil 5 correspond à un couple au RSA depuis longtemps. Il illustre le ciblage lié aux revenus et celui lié aux minima sociaux. L'augmentation comparative du score entre ce profil et le profil 2 (couple au SMIC) s'explique:
- Par la variable « Délai depuis l'ouverture de droit au minimum social »,
- Par les variables « Nombre faits générateurs (FG) "Ressources Trimestrielles" sur 18 mois » et « Nombre faits générateurs (FG) d'origine internet sur 12 mois ». Comme pour le profil 4, on suppose ici que la première est fixée à 6, l'allocataire devant déclarer ses ressources trimestriellement et 4 pour la seconde par effet mécanique en supposant que ces déclarations sont faites sur internet. 
- Par la variable « Revenus par unité de consommation » qui est plus faible pour ce profil que dans le cas où les parents gagnent le SMIC.

[^Collinet]: Pour des détails techniques sur l’algorithme et son entraînement voir l’article de Pierre Collinet « Le datamining dans les caf : une réalité, des perspectives », écrit en 2013 et disponible [ici](https://www.cairn.info/revue-informations-sociales-2013-4-page-129.htm). Il y explique notamment que l’entraînement de l’algorithme mobilise une base contenant plus de 1000 informations par allocataire. Le modèle final, après entraînement et sélection des variables les plus « intéressantes », se base sur quelques dizaines de variables. Y est aussi expliqué le fait que l’algorithme est entraîné pour détecter les indus et non les cas de fraude.
Auteurice: La Quadrature du Net

Date: 20230921

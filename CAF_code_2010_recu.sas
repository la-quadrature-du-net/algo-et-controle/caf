/************************************************************************************************/
/**************				Requ�te de cr�ation de la base d'�tude					*************/
/**************				et scoring mod�les Datamining juin 2010					*************/
/**************				Auteurs : xxxxx											*************/
/************************************************************************************************/


/* RECUPERATION DE LA DATE DE REFERENCE DES TABLES BASEFRM  */
data _null_ ; 
	set basefrm.datall ; 
	call symput ("mvDTBASEFRM", DTBASE) ; 
	call symput ("mvMOISFRM", put(month(DTBASE),z2.)) ; 
	call symput ("mvANNEEFRM", substr(put(year(DTBASE),$4.),3,2)) ; 
	call symput ("mvFREM", "Basestat.FRE" !! "&mvMOISFRM." !! "&mvANNEEFRM") ; 
run ;	


%let mvMOISFRM=06;
%let mvANNEEFRM=10;
%let mvFREM=basestat.fre0610;

/************************************************************************************************/
/************************************************************************************************/
/**************				PARTIE I- Cr�ation de la base d'�tude 			*************/
/************************************************************************************************/
/************************************************************************************************/


/************************************************************/
/* CREATION DE LA TABLE FINAL A BLANC 						*/
/* POUR ALIMENTATION AU FUR ET A MESURE 					*/
/************************************************************/
%macro mpINITAB ; 
	data _null_;  
			RETAIN ORDRE  0 ;
			%do i = 7 %to 12 ; 
				ORDRE = ORDRE + 1  ; 
				CALL SYMPUT(COMPRESS("M"!!ORDRE),"%sysfunc(putn(&i.,z2.))08") ; 
			%end ; 
			%do i = 1 %to 12 ; 
				ORDRE = ORDRE + 1  ; 
				CALL SYMPUT(COMPRESS("M"!!ORDRE),"%sysfunc(putn(&i.,z2.))09") ; 
			%end ; 
			%do i = 1 %to 6 ; 
				ORDRE = ORDRE + 1  ; 
				CALL SYMPUT(COMPRESS("M"!!ORDRE),"%sysfunc(putn(&i.,z2.))10") ; 
			%end ; 
		run ;
data work.final ; 
	set &mvFREM (keep = matricul presfres
						where = (presfres in (2 4 5 6)));
	ATTRIB 
	%do i = 1 %to  24 ; 
	SINDTOT&&m&i format = 3. informat = 3. length = 3 LABEL = "SITUATION INDUS TOTAL SUR &&m&i"  
	SINDFNAL&&m&i format = 3. informat = 3. length = 3 LABEL = "SITUATION INDUS FNAL SUR &&m&i" 
	SINDFNPF&&m&i format = 3. informat = 3. length = 3 LABEL = "SITUATION INDUS FNPF SUR &&m&i" 
	SINDFNH&&m&i format = 3. informat = 3. length = 3 LABEL = "SITUATION INDUS FNH SUR &&m&i" 

	MTRAREGC&&m&i format = 8.2 informat = 8.2 length = 8 LABEL = "MONTANT THEORIQUE RAPPEL CONTROLE APRES RDS SUR &&m&i"
	MTRAPSRC&&m&i format = 8.2 informat = 8.2 length = 8 LABEL = "MT RAPPEL PERIODE SUSPENSION CONTROLE APRES RDS SUR &&m&i"
	DDRAREGC&&m&i format = 6.  informat = 6.  length = 6 LABEL = "DATE DEBUT RAPPEL CONTROLE SUR &&m&i"

	%end ; 
	
	MODEREC_dernier format = $MODEREC. informat = $2. length = $2. LABEL = "MODE DE RECOUVREMENT DE L'INDU LE PLUS RECENT" 
	TYPEBAI_dernier format = $TYPEBAI. informat = $1. length = $1. LABEL = "TYPE BAILLEUR SUR dernier mois"
	NIRNONCER_dernier format = 3. informat = 3. length = 3 
		LABEL = "NOMBRE DE NIR NON CERTIFIES SUR dernier mois (CONJOINTS, ENFANTS, ET AUTRES PERSONNES DU DOSSIER"
	%do i = 1 %to  24 ; 
	FGTOT&&m&i format = 8. INFORMAT = 8. length = 8. LABEL = "NB TOTAL DE FG PASSES SUR LE DOSSIER SUR &&m&i" 
	SITFAM&&m&i format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG SITFAM PASSES SUR LE DOSSIER SUR &&m&i" 
	CHASITFAM&&m&i format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG CHASITFAM PASSES SUR LE DOSSIER SUR &&m&i" 
	SITENFAUT&&m&i format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG SITENFAUT PASSES SUR LE DOSSIER SUR &&m&i" 
	RESCHCAF&&m&i format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG RESCHCAF PASSES SUR LE DOSSIER SUR &&m&i" 
	RESANN&&m&i format = 8. INFORMAT = 8. length = 8. LABEL = "NB RESANN FG PASSES SUR LE DOSSIER SUR &&m&i" 
	RESTR&&m&i format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG RESTRIRMI, RESTRIAPI OU RESTRIRSA PASSES SUR LE DOSSIER SUR &&m&i" 
	SITPRO&&m&i format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG SITPRO PASSES SUR LE DOSSIER SUR &&m&i"
	%end ; 
	DTSITFACAD	format = ddmmyy10. informat = ddmmyy10. length = 6
						Label = "DATE DU DERNIER PASSAGE D'UN FAIT GENERATEUR SITFAM CAD (CONFIRME ET VALIDE)" 
	DTSITFALL	format = ddmmyy10. informat = ddmmyy10. length = 6
						Label = "DATE DU DERNIER PASSAGE D'UN FAIT GENERATEUR SITFAM ALL (CONFIRME ET VALIDE)"  
	DTRESADGI	format = ddmmyy10. informat = ddmmyy10. length = 6
						Label = "DATE DU DERNIER PASSAGE D'UN FAIT GENERATEUR RESANN DGI (CONFIRME ET VALIDE)"   
	DTRESALL	format = ddmmyy10. informat = ddmmyy10. length = 6
						Label = "DATE DU DERNIER PASSAGE D'UN FAIT GENERATEUR RESANN ALL (CONFIRME ET VALIDE)"  
	NBFGRESALL	format = 8.		informat = 8.		length = 8
						Label = "NOMBRE DE FG RESANN D'ORIGINE ALLOCATAIRE ENTRE JAN 2010 et JUIN 2010" 
	NBFGRESSAP	format = 8.		informat = 8.		length = 8
					Label = "NOMBRE DE FG RESECHCAF D'ORIGINE ALLOCATAIRE ENTRE JAN 2010 et JUIN 2010" 
	
	MODPAIDO_dernier format = $ODPAIDO. informat = $2. length = $2. LABEL = "MODE PAIEMENT DOSSIER SUR dernier mois" 
	DTINSCDO_dernier format = ddmmyy10. informat = ddmmyy10. length =  5 LABEL = "DATE INSCRIPTION DOSSIER SUR dernier mois" 
	/*ADRESS_dernier FORMAT = $40. INFORMAT = $40. LENGTH = $40 LABEL = "ADRESSE SUR dernier mois" */

	%do i = 1 %to  24 ; 
	ADRESS&&m&i FORMAT = $40. INFORMAT = $40. LENGTH = $40 LABEL = "ADRESSE SUR &&m&i" 
	%end ; 
	%do i = 1 %to  24 ; 
	VU&&m&i format = 8. informat = 8. length = 8 
						LABEL = "NOMBRE DE CONTACTS PAR NATURE DE CONTACT ACCUEIL OU VISITE SUR &&m&i" 
	TEL&&m&i format = 8. informat = 8. length = 8 
						LABEL = "NOMBRE DE CONTACTS PAR NATURE DE CONTACT TELEPHONE SUR &&m&i" 
	AUT&&m&i format = 8. informat = 8. length = 8 
						LABEL = "NOMBRE DE CONTACTS PAR NATURE DE CONTACT AUTRES SUR &&m&i"   
	%end ; 
	; 

run ; 
%mend ; 

%mpINITAB ; 


/**************************************************/
/* CATEGORIE D'INDICATEURS : CONTROLES ANTERIEURS */
/**************************************************/
%macro mp_FPCO (mvMMAA = ) ; 
	data _null_ ; 
		if  exist("basestat.FPCO&mvMMAA.",'data') 
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
	run ; 
	%if &topexist. = O %then %do ; 
	data work.FPCO&mvMMAA. ; 
		merge work.final (in = a keep =  MATRICUL )
			  basestat.FPCO&mvMMAA. ( in = b keep = matricul MTRAREGC MTRAPSRC DDRAREGC  ) ; 
		by matricul ; 
		if a = 1 and b = 1 ; 	

		if (MTRAREGC = 0  and MTRAPSRC = 0 ) or DDRAREGC = 0 then DDRAREGC = . ; 
		/* on conservera le min des DDRAREGC donc suppressions des 0 non significatifs*/
		if MTRAREGC = 0 then MTRAREGC = . ; 
		if MTRAPSRC = 0 then MTRAPSRC = . ; 
	run ; 
	/* mise � jour de la table final avec donn�es existantes */	
	proc means data = work.FPCO&mvMMAA. noprint nway; 
		class matricul ; 
		output out = work.FPCO&mvMMAA.C 
			max(MTRAREGC) = MTRAREGC  
			max(MTRAPSRC) = MTRAPSRC 
			min(DDRAREGC) = DDRAREGC ; 
	run ; 
	data work.final  ; 
		merge work.final ( in = a ) 
			  work.FPCO&mvMMAA.C (keep = matricul MTRAREGC MTRAPSRC DDRAREGC
								 rename = (MTRAREGC=MTRAREGC&mvMMAA.  MTRAPSRC=MTRAPSRC&mvMMAA. 
									 	   DDRAREGC=DDRAREGC&mvMMAA. )) ; 
		by matricul ; 
		if a = 1 ; 
	run ; 
	%end ; 	
%mend ; 

%mp_FPCO (mvMMAA=0110) ; 
%mp_FPCO (mvMMAA=0210) ; 
%mp_FPCO (mvMMAA=0310) ; 
%mp_FPCO (mvMMAA=0410) ; 
%mp_FPCO (mvMMAA=0510) ; 
%mp_FPCO (mvMMAA=0610) ; 

/**********************************************************/
/* CATEGORIE D'INDICATEURS : INDUS  ANTERIEURS + ADRESSE */
/********************************************************/

%macro mpHISTALCAF (mvANNEE = ); 
	data _null_ ; 
		if &mvANNEE. = 2008 then do ; 
			call symput ("mvPMOIS", 7) ; 
			call symput ("mvDMOIS", 12) ; 
		end ; 
		if &mvANNEE. = 2009 then do ; 
			call symput ("mvPMOIS", 1) ; 
			call symput ("mvDMOIS", 12) ; 
		end ; 
		if &mvANNEE. = 2010 then do ; 
			call symput ("mvPMOIS", 1) ; 
			call symput ("mvDMOIS", 6) ; 
		end ; 
	run ; 
	%do mvMOIS= &mvPMOIS. %to &mvDMOIS. ; 
		data _null_ ; 
			call symput ("aa", substr("&mvANNEE.",3,2)) ; 
			call symput ("mm", put(&mvMOIS.,z2.)) ; 
		run ; 
		data _null_ ; /* contr�le existence des tables */ 
		if  exist("basestat.FRE&mm.&aa.",'data') 
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
		run ; 
		%if &topexist. = O %then %do ; 
		Data work.ALLCAF&mm.&aa. ( drop = NUMVOIED TYPEVOID NOMVOIED);
		  merge work.final  (in = a keep =  MATRICUL ) 
				basestat.FRE&mm.&aa. (in = b keep = MATRICUL SINDTOT SINDFNAL SINDFNPF SINDFNH 
													NUMVOIED TYPEVOID NOMVOIED CATBEN 
													where = (CATBEN ne "99" ) );
				by MATRICUL; 
				if a = 1 and b = 1 ; 
		  ATTRIB ADRESS&mm.&aa. FORMAT = $40. INFORMAT = $40. LENGTH = $40 LABEL = "ADRESSE SUR &mm.&aa." ; 

		  LABEL 
		  	SINDTOT = "SITUATION INDUS TOTAL SUR &mm.&aa." 
			SINDFNAL = "SITUATION INDUS FNAL SUR &mm.&aa." 
			SINDFNPF = "SITUATION INDUS FNPF SUR &mm.&aa." 
			SINDFNH = "SITUATION INDUS FNH SUR &mm. &aa." ; 
		  rename 
		  	SINDTOT = SINDTOT&mm.&aa. 
		  	SINDFNAL = SINDFNAL&mm.&aa.  
		  	SINDFNPF = SINDFNPF&mm.&aa.  
		  	SINDFNH = SINDFNH&mm.&aa. ; 
			ADRESS&mm.&aa. = COMPBL(NUMVOIED!!" "!!TYPEVOID!!" "!!NOMVOIED ) ;
		run ; 
		/* mise � jour de la table final avec donn�es existantes */	
		data work.final  ; 
			merge work.final ( in = a ) 
				  work.ALLCAF&mm.&aa. ; 
			by matricul ; 
			if a = 1 ; 
			/*if (&mm.="06" and &aa.="10") then ADRESS_dernier=ADRESS&mm.&aa.;*/
		run ; 
		%end ; 
	%end ; 
%mend ; 
%mpHISTALCAF (mvANNEE = 2008 );
%mpHISTALCAF (mvANNEE = 2009 );
%mpHISTALCAF (mvANNEE = 2010 ); 
		
/********************************************************************/
/* CATEGORIE D'INDICATEURS : PROJET DATAMINING                      */
/********************************************************************/

%macro mpFGHISTO (mvANNEE = ); 
	data null ; 
		if &mvANNEE. = 2008 then do ; 
			call symput ("mvPMOIS", 7) ; 
			call symput ("mvDMOIS", 12) ; 
		end ; 
		if &mvANNEE. = 2009 then do ; 
			call symput ("mvPMOIS", 1) ; 
			call symput ("mvDMOIS", 12) ; 
		end ; 
		if &mvANNEE. = 2010 then do ; 
			call symput ("mvPMOIS", 1) ; 
			call symput ("mvDMOIS", 06) ; 
		end ; 
	run ; 
	%do mvMOIS= &mvPMOIS. %to &mvDMOIS. ; 
		data _null_ ; 
			call symput ("aa", substr("&mvANNEE.",3,2)) ; 
			call symput ("mm", put(&mvMOIS.,z2.)) ; 
		run ; 
		
		data _null_ ; /* contr�le existence des tables */ 
		if  exist("basefrm.FAI&mm.&aa.",'data') 
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
		run ; 
		%if &topexist. = O %then %do ; 
			Data work.FAI&mm.&aa. ;
			  merge work.final  (in = a keep =  MATRICUL ) 
					basefrm.FAI&mm.&aa. (in = b keep = MATRICUL DTSESLIQ FGELE ETATLIQ ETASSLIQ ORILIQ );
					by MATRICUL; 
					if a = 1 and b = 1 ; 
			  ATTRIB 
			  FGTOT&mm.&aa. format = 8. INFORMAT = 8. length = 8. LABEL = "NB TOTAL DE FG PASSES SUR LE DOSSIER SUR &mm.&aa." 
			  SITFAM&mm.&aa. format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG SITFAM PASSES SUR LE DOSSIER SUR &mm.&aa." 
			  CHASITFAM&mm.&aa. format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG CHASITFAM PASSES SUR LE DOSSIER SUR &mm.&aa." 
			  SITENFAUT&mm.&aa. format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG SITENFAUT PASSES SUR LE DOSSIER SUR &mm.&aa." 
			  RESCHCAF&mm.&aa. format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG RESCHCAF PASSES SUR LE DOSSIER SUR &mm.&aa." 
			  RESANN&mm.&aa. format = 8. INFORMAT = 8. length = 8. LABEL = "NB RESANN FG PASSES SUR LE DOSSIER SUR &mm.&aa." 
			  RESTR&mm.&aa. format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG RESTRIRMI OU RESTRIAPI PASSES SUR LE DOSSIER SUR &mm.&aa." 
			  SITPRO&mm.&aa. format = 8. INFORMAT = 8. length = 8. LABEL = "NB FG SITPRO PASSES SUR LE DOSSIER SUR &mm.&aa." 
			  ; 
			  if ETATLIQ = "03" and ETASSLIQ = "05" 
			     and MDY(&mvMOIS.,01,&mvANNEE.) <= DTSESLIQ <= INTNX('MONTH',MDY(&mvMOIS.,01,&mvANNEE.),0, "end")
			  then do ; 
			  	FGTOT&mm.&aa. = 1 ; 
				if FGELE = "SITFAM" then SITFAM&mm.&aa. = 1 ; 
				if FGELE = "CHASITFAM" then CHASITFAM&mm.&aa. = 1 ; 
				if FGELE = "SITENFAUT" then SITENFAUT&mm.&aa. = 1 ; 
				if FGELE = "RESECHCAF" then RESCHCAF&mm.&aa. = 1 ; 
				if FGELE = "RESANN" then RESANN&mm.&aa. = 1 ; 
				if FGELE in ("RESTRIAPI" "RESTRIRMI" "RESTRIRSA") then RESTR&mm.&aa. = 1 ; 
				if FGELE = "SITPRO" then SITPRO&mm.&aa. = 1 ; 
			  end ; 

			  /*INDICATEUR SUPPLEMENTAIRE POUR LES MOIS DE JAN 2010 A JUIN 2010 */
	
			 %if (1<= &mvMOIS. <=6 and &mvANNEE. = 2010 )
				%then %do ; 
				ATTRIB  NBFGRESALL	format = 8.		informat = 8.		length = 8
						Label = "NOMBRE DE FG RESANN D'ORIGINE ALLOCATAIRE ENTRE JAN 2010 et JUIN 2010" ;
				if FGELE = 'RESANN' and ETATLIQ = "03" and ETASSLIQ = "05" and ORILIQ = "ALL" then 
							NBFGRESALL = 1 ; 
			 %end ; 
			 
			  /* INDICATEURS SUPPLEMENTAIRES POUR LE MOIS DE JUIN 2010 UNIQUEMENT */
			  %if &mvMOIS. = 6 and &mvANNEE. = 2010 %then %do ; 
			  ATTRIB 
			  DTSITFACAD	format = ddmmyy10. informat = ddmmyy10. length = 6
						Label = "DATE DU DERNIER PASSAGE D'UN FAIT GENERATEUR SITFAM CAD (CONFIRME ET VALIDE)" 
			  DTSITFALL	format = ddmmyy10. informat = ddmmyy10. length = 6
						Label = "DATE DU DERNIER PASSAGE D'UN FAIT GENERATEUR SITFAM ALL (CONFIRME ET VALIDE)"  
			  DTRESADGI	format = ddmmyy10. informat = ddmmyy10. length = 6
						Label = "DATE DU DERNIER PASSAGE D'UN FAIT GENERATEUR RESANN DGI (CONFIRME ET VALIDE)"   
			  DTRESALL	format = ddmmyy10. informat = ddmmyy10. length = 6
						Label = "DATE DU DERNIER PASSAGE D'UN FAIT GENERATEUR RESANN ALL (CONFIRME ET VALIDE)" ; 

			  if FGELE = 'SITFAM' and ETATLIQ = "03" and ETASSLIQ = "05" and ORILIQ = "CAD" then DTSITFACAD = DTSESLIQ ; 
			  if FGELE = 'SITFAM' and ETATLIQ = "03" and ETASSLIQ = "05" and ORILIQ = "ALL" then DTSITFALL = DTSESLIQ ; 
			  if FGELE = 'RESANN' and ETATLIQ = "03" and ETASSLIQ = "05" and ORILIQ = "DGI" then DTRESADGI = DTSESLIQ ; 
			  if FGELE = 'RESANN' and ETATLIQ = "03" and ETASSLIQ = "05" and ORILIQ = "ALL" then DTRESALL = DTSESLIQ ; 
			  %end ; 
			  run ; 
			  proc means data =work.FAI&mm.&aa. noprint nway ; 
			  	var FGTOT&mm.&aa. SITFAM&mm.&aa. CHASITFAM&mm.&aa. SITENFAUT&mm.&aa. 
					RESCHCAF&mm.&aa. RESANN&mm.&aa. RESTR&mm.&aa. SITPRO&mm.&aa. ; 
				class  MATRICUL ; 
				output out = work.NBFG&mm.&aa. (drop = _type_ _freq_ )  sum() = ; 
			  run ; 
		  
			/* mise � jour de la table final avec donn�es existantes */	
			data work.final  ; 
				merge work.final ( in = a ) 
					  work.NBFG&mm.&aa. ; 
				by matricul ; 
				if a = 1 ; 
			run ; 
		%end ; 
	%end ; 
%mend ;
%mpFGHISTO (mvANNEE= 2008) ; 
%mpFGHISTO (mvANNEE= 2009) ; 
%mpFGHISTO (mvANNEE= 2010) ; 

/********************************/

%put &sysdate;
%put &systime;

%macro mpFG ; 
	data _null_ ; 
		if  exist("work.FAI0610",'data') 
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
	run ; 
	%if &topexist. = O %then %do ; 
		proc means data = work.FAI0610 noprint nway;
		class  matricul  ; 
		output out = work.DTDERFG(drop = _type_ _freq_ ) 
			max (DTSITFACAD DTSITFALL DTRESADGI DTRESALL) = DTSITFACAD DTSITFALL DTRESADGI DTRESALL	 ; 
		run ; 
		data work.final ; 
			merge work.final (in = a ) 
				  work.DTDERFG ; 
			by matricul ; 
			if a = 1 ; 
		run ; 
	%end ;  
	/* VARIABLE NBFGRESALL */
	data _null_ ; 
		if  exist("work.FAI0110",'data') 
			and exist("work.FAI0210",'data') and  exist("work.FAI0310",'data') and  exist("work.FAI0410",'data')
			and exist("work.FAI0510",'data') and  exist("work.FAI0610",'data')  
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
	run ; 
	%if &topexist. = O %then %do ;
		data work.FAI01100610 ; 
		 	set work.FAI0110
				work.FAI0210
				work.FAI0310
				work.FAI0410 
				work.FAI0510
				work.FAI0610 ; 
		run ; 
		proc means data = work.FAI01100610 noprint nway;
			class  matricul  ; 
			output out = work.NBFGRESALL(drop = _type_ _freq_ ) sum (NBFGRESALL) = NBFGRESALL ; 
		run ; 
		data work.final ; 
			merge work.final ( in = a ) 
				  work.NBFGRESALL ; 
			by matricul ; 
		run ; 
	%end ; 
	/* VARIABLE NBFGRESERCHCAF */
	data _null_ ; 
		if  exist("work.FAI0110",'data') 
			and exist("work.FAI0210",'data') and  exist("work.FAI0310",'data') and  exist("work.FAI0410",'data')
			and exist("work.FAI0510",'data') and  exist("work.FAI0610",'data')   
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
	run ; 
	%if &topexist. = O %then %do ;
		data work.FAI01100610 ; 
		 	set basefrm.FAI0110 (keep = MATRICUL DTSESLIQ FGELE ETATLIQ ETASSLIQ ORILIQ) 
				basefrm.FAI0210 (keep = MATRICUL DTSESLIQ FGELE ETATLIQ ETASSLIQ ORILIQ) 
				basefrm.FAI0310 (keep = MATRICUL DTSESLIQ FGELE ETATLIQ ETASSLIQ ORILIQ) 
				basefrm.FAI0410 (keep = MATRICUL DTSESLIQ FGELE ETATLIQ ETASSLIQ ORILIQ) 
				basefrm.FAI0510 (keep = MATRICUL DTSESLIQ FGELE ETATLIQ ETASSLIQ ORILIQ) 
				basefrm.FAI0610 (keep = MATRICUL DTSESLIQ FGELE ETATLIQ ETASSLIQ ORILIQ) ; 
			 /*INDICATEUR SUPPLEMENTAIRE POUR LES MOIS DE JAN 2010 A JUIN 2010 */
			ATTRIB  NBFGRESSAP	format = 8.		informat = 8.		length = 8
					Label = "NOMBRE DE FG RESECHCAF D'ORIGINE ALLOCATAIRE ENTRE JAN 2010 et JUIN 2010" ;
			if FGELE = 'RESECHCAF' and ETATLIQ = "03" and ETASSLIQ = "05" then 	NBFGRESSAP = 1 ; 
		run ; 
		proc sort data = work.fai01100610 ; 
			by matricul ; 
		run ; 
		data work.fai01100610 ; 
			 merge work.final ( in = a keep = matricul ) 
				  work.fai01100610 ( in = b  ) ; 
			by matricul ; 
			if a = 1  ; 
		run ; 
		data work.EFP ; 
			merge work.final ( in = a keep = matricul ) 
				  basefrm.EFP ( in = b keep = MATRICUL NMINPEDG ECHARESS DTECHARE where = (ECHARESS in ('C' 'P') and 
									 DTECHARE >= INTNX('MONTH',&mvDTBASEFRM.,-5,"beginning" ) )) ;
			by matricul ; 
			if a = 1 and b = 1 ; 
		run ; 
		proc sort data = work.EFP nodupkey ; 
			by matricul NMINPEDG DTECHARE ; 
		run ; 
		proc means data = work.EFP noprint nway; 
			var  DTECHARE ; 
			class matricul ; 
			output out = work.DTECHARMIN min(DTECHARE)=DTECHARMIN ; 
		run ; 
		data work.FAI01100610 ; 
			merge work.FAI01100610 (in = a ) 
				  work.DTECHARMIN (in = b keep = MATRICUL DTECHARMIN ) ; 
			by matricul ; 
			if a = 1 ; 
			if b = 0 then NBFGRESSAP = 0 ;
			else do ; 
				if DTECHARMIN <= DTSESLIQ then NBFGRESSAP = 1; 
			end ; 
		run ; 
		proc means data = work.FAI01100610 noprint nway ; 
			var NBFGRESSAP ; 
			class matricul ; 
			output out = work.NBFGRESSAP sum() = ; 
		run ; 
		data work.final ; 
			merge work.final (in = a ) 
				  work.NBFGRESSAP ; 
			by matricul ; 
			if a = 1 ; 
		run ; 
	%end ; 
%mend ; 
%mpFG ; 
/********************************************************************/
/* CATEGORIE D'INDICATEURS : PROJET DATAMINING : DONNEES CONTACT    */
/********************************************************************/

%macro mp_HISTGCA (mvANNEE = ); 
	data _null_ ; 
		if &mvANNEE. = 2008 then do ; 
			call symput ("mvPMOIS", 7) ; 
			call symput ("mvDMOIS", 12) ; 
		end ; 
		if &mvANNEE. = 2009 then do ; 
			call symput ("mvPMOIS", 1) ; 
			call symput ("mvDMOIS", 12) ; 
		end ; 
		if &mvANNEE. = 2010 then do ; 
			call symput ("mvPMOIS", 1) ; 
			call symput ("mvDMOIS", 6) ; 
		end ; 
	run ; 
	%do mvMOIS= &mvPMOIS. %to &mvDMOIS. ; 
		data _null_ ; 
			call symput ("aa", substr("&mvANNEE.",3,2)) ; 
			call symput ("mm", put(&mvMOIS.,z2.)) ; 
		run ; 
		data _null_ ; /* contr�le existence des tables */ 
		if  exist("GRA.GCA&mm.&aa.",'data') 
			then  call symput ("topexist", "O") ; 	
			else  call symput ("topexist", "N") ; 
		run ; 
		%if &topexist. = O %then %do ; 
			proc sort data = GRA.GCA&mm.&aa. out = work.GCA&mm.&aa. ; 
				by matricul ; 
			run ; 
			Data work.gca&mm.&aa. ;
					merge work.final  (in = a keep =  MATRICUL ) 
						  work.GCA&mm.&aa. (in = b keep = MATRICUL NATCONTA  );
					by MATRICUL; 
					if a = 1 and b = 1 ; 
				ATTRIB 
				VU&mm.&aa. format = 8. informat = 8. length = 8 
						LABEL = "NOMBRE DE CONTACTS PAR NATURE DE CONTACT ACCUEIL OU VISITE SUR &mm.&aa." 
				TEL&mm.&aa. format = 8. informat = 8. length = 8 
						LABEL = "NOMBRE DE CONTACTS PAR NATURE DE CONTACT TELEPHONE SUR &mm.&aa." 
				AUT&mm.&aa. format = 8. informat = 8. length = 8 
						LABEL = "NOMBRE DE CONTACTS PAR NATURE DE CONTACT AUTRES SUR &mm.&aa."  ; 
	
				if natconta in ("A" "I" "N" "P" "V" ) then VU&mm.&aa. = 1 ; 
				if natconta in ("D" "R" ) then TEL&mm.&aa. = 1 ; 
				if natconta in ("B" "T" ) then AUT&mm.&aa. = 1 ; 
			run ; 
			proc means data = work.gca&mm.&aa. noprint nway; 
				VAR VU&mm.&aa. TEL&mm.&aa. AUT&mm.&aa. ; 
				class matricul ; 
				output out = work.CONTA&mm.&aa. (drop= _type_ _freq_) sum() = ; 
			run ; 		  
			/* mise � jour de la table final avec donn�es existantes */	
			data work.final  ; 
				merge work.final ( in = a ) 
					  work.CONTA&mm.&aa. ; 
				by matricul ; 
				if a = 1 ; 
			run ; 
		%end ; 
	%end ; 
%mend ; 
%mp_HISTGCA (mvANNEE =2008 ); 
%mp_HISTGCA (mvANNEE =2009 ); 
%mp_HISTGCA (mvANNEE =2010 ); 

/********************************************************************/
/* CATEGORIE D'INDICATEURS : DIVERS SUR BASE INFOCENRE 				*/
/********************************************************************/
/* BAILLEUR */
Data work.LGT ; 
	merge work.final ( in = a keep = matricul ) 
		  basefrm.LGT  ( in = b keep = MATRICUL TYPEBAI) ; 
	by matricul ; 
	if a = 1 and b = 1 ; 
	LABEL TYPEBAI = "TYPE BAILLEUR SUR dernier mois " ; 
	rename TYPEBAI = TYPEBAI_dernier ; 
run ; 
/*DONNEES DOS **/
Data work.DOS ; 
	merge work.final ( in = a keep = matricul ) 
		  basefrm.DOS  ( in = b keep = MATRICUL MODPAIDO DTINSCDO) ; 
	by matricul ; 
	if a = 1 and b = 1 ; 
	rename MODPAIDO = MODPAIDO_dernier 
		   DTINSCDO = DTINSCDO_dernier ; 
run ; 
/*NIR*/
data work.mon (where = (NIRNONCER_dernier = 1 ) )  ; 
	merge work.final ( in = a keep = matricul ) 
		  basefrm.MON  ( in = b keep = MATRICUL VALNIRMO) ; 
	by matricul ; 
	if a = 1 and b = 1 ; 
	ATTRIB NIRNONCER_dernier format = 3. informat = 3. length = 3 ; 
	if VALNIRMO in ("3" "4" "5") then NIRNONCER_dernier = 1; 
run ; 
data work.mme (where = (NIRNONCER_dernier = 1 ) ) ; 
	merge work.final ( in = a keep = matricul ) 
		  basefrm.mme  ( in = b keep = MATRICUL VALNIRMM) ; 
	by matricul ; 
	if a = 1 and b = 1 ; 
	ATTRIB NIRNONCER_dernier format = 3. informat = 3. length = 3 ; 
	if VALNIRMM in ("3" "4" "5") then NIRNONCER_dernier = 1; 
run ; 
data work.enf (where = (NIRNONCER_dernier = 1 ) ) ; 
	merge work.final ( in = a keep = matricul ) 
		  basefrm.enf  ( in = b keep = MATRICUL VALNIREN DDRADOEN DFRADOEN) ; 
	by matricul ; 
	if a = 1 and b = 1 ; 
	ATTRIB NIRNONCER_dernier format = 3. informat = 3. length = 3 ; 
	if DDRADOEN <= &mvDTBASEFRM. <= DFRADOEN  and VALNIREN in ("3" "4" "5") then NIRNONCER_dernier = 1; 
run ; 
data work.aut (where = (NIRNONCER_dernier = 1 ) ) ; 
	merge work.final ( in = a keep = matricul ) 
		  basefrm.AUT  ( in = b keep = MATRICUL VALNIRAU DDRATAUT DFRATAUT ) ; 
	by matricul ; 
	if a = 1 and b = 1 ; 
	ATTRIB NIRNONCER_dernier format = 3. informat = 3. length = 3 ; 
	if DDRATAUT <= &mvDTBASEFRM. <= DFRATAUT  and VALNIRAU in ("3" "4" "5") then NIRNONCER_dernier = 1; 
run ; 
data work.NIR ; 
	set work.MON ( keep = MATRICUL NIRNONCER_dernier ) 
		work.MME ( keep = MATRICUL NIRNONCER_dernier ) 
		work.enf ( keep = MATRICUL NIRNONCER_dernier ) 
		work.aut ( keep = MATRICUL NIRNONCER_dernier ) ;
	LABEL NIRNONCER_dernier = "NOMBRE DE NIR NON CERTIFIES SUR FRS_BREG_datamining (CONJOINTS, ENFANTS, ET AUTRES PERSONNES DU DOSSIER)" ; 
run ; 
proc means data = work.NIR noprint nway ; 
	var NIRNONCER_dernier ; 
	class matricul ; 
	output out = NIRNONCER (drop = _type_ _freq_ )  sum() = ;
run ;

data work.LISTMODEREC  ; 
	merge work.final ( in = a keep = matricul ) 
		  basefrm.CRE  ( in = b keep = MATRICUL NATCRE DTIMPLCR MTSOLREE MODEREC 
							where = (NATCRE like 'I%' )) ; 
	by matricul ; 
	if a = 1 and b = 1 ;
run ; 
Proc sort data = work.LISTMODEREC ; 
by MATRICUL descending DTIMPLCR descending mtsolree  ; 
run ; 
data work.MODEREC ( keep = matricul MODEREC_dernier); 
	set work.LISTMODEREC ; 
	by matricul ; 
	if first.matricul ; 
	LABEL MODEREC = "MODE DE RECOUVREMENT DE L'INDUS LE PLUS RECENT" ;
	rename MODEREC = MODEREC_dernier ; 

run ; 

data work.final ; 
	merge work.final ( in = a ) 
		  work.MODEREC 
		  work.LGT 
		  work.NIRNONCER 
		  work.DOS  ; 
	by matricul ; 
	if a = 1 ; 
run ; 



%put &systime;


/************************************************************************************************/
/**************				PARTIE II- Alimentation de la base d'�tude				*************/
/**************						avec les donn�es historiques Allnat				*************/
/************************************************************************************************/

 
%macro recup (date);
data jointure_&date; set basestat.fre&date 
(keep= MATRICUL SITFAM ACTCONJ ACTRESPD CODEPOSD SITFAM ABANEURE ABANEUCO RESTRACO RESTRACE 
rename=(SITFAM=FAM&date ACTCONJ=ACJ&date ACTRESPD=ACR&date CODEPOSD=POST&date RESTRACO=RTRO&date RESTRACE=RTRE&date ABANEURE=ABNE&date ABANEUCO=ABNO&date ) );
run;
/*Recup donn�es contr�les administratifs clos*/
data jointc_&date; set basestat.fpco&date 
(keep=MATRICUL TYPECONT CIBCONTR MTINREGC
rename=(TYPECONT=TC&date CIBCONTR=CIBC&date MTINREGC=INDC&date) );
run;
/*Recup donn�es verif comptable apr*/
data jointv_&date; set basestat.fvap&date 
(keep=MATRICUL CIBVERAP MTFVERAP
rename=(CIBVERAP=CIBV&date MTFVERAP=REGV&date) );
run;
proc sort data=jointure_&date nodupkey;by MATRICUL;run;
proc sort data=jointc_&date nodupkey;by MATRICUL;run;
proc sort data=jointv_&date nodupkey;by MATRICUL;run;
data jointure_&date;
merge jointure_&date (in=a) 	jointc_&date (in=b) 	jointv_&date (in=c);
by MATRICUL;
if a;
run;
%mend;

%recup(0708);%recup(0808);%recup(0908);%recup(1008);%recup(1108);%recup(1208);
%recup(0109);%recup(0209);%recup(0309);%recup(0409);%recup(0509);%recup(0609);
%recup(0709);%recup(0809);%recup(0909);%recup(1009);%recup(1109);%recup(1209);
%recup(0110);%recup(0210);%recup(0310);%recup(0410);%recup(0510);%recup(0610);


data jointure;
merge jointure_0708 	jointure_0808 	jointure_0908 	jointure_1008 
	  jointure_1108 	jointure_1208 	jointure_0109 	jointure_0209
	  jointure_0309   	jointure_0409	jointure_0509 	jointure_0609
	  jointure_0709   	jointure_0809   jointure_0909 	jointure_1009 
	  jointure_1109   	jointure_1209   jointure_0110 	jointure_0210
	  jointure_0310   	jointure_0410	jointure_0510	jointure_0610;	
by MATRICUL;
run;



/*****************************************************/
/*****************************************************/
/*** FUSION ALLCAF + HISTORIQUE + BASE ETUDE CNEDI****/
/*** Cr�ation des labels des variables de l'enqu�te***/
/*****************************************************/
/*****************************************************/

data temp;
set &mvFREM (where= (presfres in (2 4 5 6)));
run;

data datamining&mvMOISFRM.&mvANNEEFRM; merge temp (in=a) jointure (in=b) work.final (in=c);
by MATRICUL;if a=1;
label mtind_red = "Montant d'indu d�tect�";
label durind_red = "Dur�e de l'indu d�tect�";
label indu = "Top indu sur le compte";
label qualif= "Top qualification de fraude";
label commission= "Commission de qualification des suspicions de fraude";
label mtrapreg_red = "Montant de rappel d�tect� (hors rap. suite suspension)";
label durap_red = "Dur�e du rappel d�tect�";
label rappel= "Top rappel sur le compte";
label MTINCAFCF = "Montant d'indu AF CF d�tect�";
label MTINCPAJE = "Montant d'indu PAJE d�tect�";
label MTINCAPI  = "Montant d'indu API d�tect�";
label MTINCRMI = "Montant d'indu RMI d�tect�";
label MTINCAL= "Montant d'indu AL d�tect�";
label MTINCASF= "Montant d'indu ASF d�tect�";
label MTINCFNPF= "Montant d'indu sur le fond FNPF";
label INCAFCF= "Top indu AF CF";
label INCPAJE= "Top indu PAJE";
label INCAPI= "Top indu API";
label INCRMI= "Top indu RMI";
label INCAL= "Top indu AL";
label INCASF= "Top indu ASF";
label INCFNPF= "Top indu sur le fond FNPF";
label mod_connue= "Motif d'indu connu";
label mod_sitpro= "Indu modif. Situation professionnelle (yc incarc�ration, hosp.)";
label mod_log= "Indu modif. Logement";
label mod_ress= "Indu modif. Ressources (yc subsidiarit� API ASF)";
label mod_enf= "Indu modif. Charge d'enfant";
label mod_sitfam= "Indu modif. Situation familiale";
label mod_RMI= "Indu modif RMI";
label mod_Autre_mod= "Indu autre modification";
label resp_erreural= "Indu cause erreur allocataire";
label resp_meco= "Indu cause m�connaissance l�gislation";
label resp_absdem= "Indu cause absence de demande";
label resp_caf= "Indu cause erreur CAF";
label resp_oubli= "Indu cause oubli allocataire (yc retard signalement ou non pris en compte caf)";
label resp_tiers= "Indu cause erreur tiers";
label resp_fraud= "Indu frauduleux";
label resp_al= "Indu toutes origines allocataire";
label TETRREGC= "Top etranger regularisee ";
label TOPMADRC= "Top changement adresse ";
label TOPMPERC= "Top mouvement enfant autre personne ";
label TOPMFAMC= "Top modification situation famille ";
label TOPMRESC= "Top modification ressources ";
label TOPMAPIC= "Top modification assiette api ";
label TOPMRMIC= "Top modification assiette rmi ";
label TOPMACTC= "Top modification activite ";
label TOPMRSAC= "Top modification assiette rsa ";
label TOPMLOYC= "Top modification loyer remboursement ";
label TMSIREGC= "Top modification situation rmi ";
label pondech= "Variable de pond�ration sur allocataires de l'�chantillon";
label pondalloc= "Variable de pond�ration sur l'ensemble des allocataires CAF";
run;
proc sort data= datamining&mvMOISFRM.&mvANNEEFRM  nodupkey ;by MATRICUL;run;




/************************************************************************************************/
/************************************************************************************************/
/**************				PARTIE III- Alimentation de la base d'�tude				*************/
/**************					avec variables Allnat recod�es et cr��es			*************/
/************************************************************************************************/
/************************************************************************************************/


proc format;

value nbenf
0 ="0 "
1 ="1 "
2-high="2+";

value couv2_
0 ="0  "
1 ="1  "
2 ="2  "
3 ="3  "
4 ="4  "
5-high="5+";

value disc2f
0 ="0 "
1 ="1 "
2-high ="2+";

value disc4f
0 ="0 "
1 ="1 "
2 ="2 "
3 ="3 "
4-high ="4+";

value disc5f
0 ="0  "
1 ="1  "
2 ="2  "
3,4 ="3-4 "
5-high ="5+";

value onta
0,1 ="0-1"
2,3 ="2-3"
4,5,6,7 ="4-7"
8-high ="8+";

value ontatel
0 ="0  "
1 ="1  "
2,3 ="2-3"
4-high ="4+";

value ontavu
0 ="0  "
1,2 ="1-2"
3,4 ="3-4"
5-high ="5+";

value MOINSC(fuzz=0.5 )
0-11.4='INFERIEUR � 11.4'
11.4-58.2='DE 11.4 � 58.2'
58.2-105='DE 58.2 � 105'
105-high ='SUP � 105';

value MTLOY2f
0 -< 0.01 ='montant nul  '
0.01-< 400 ='moins de 400 '
400 -< 600 ='de 400 � 600 '
600 - high ='sup � 600 ';

value MTTRIA_F(fuzz=0.175 )
0='0'
0.00001-<9995='plus de 0'
9995-high='Inconnu';

value $occlo2_
"20", "21", "22", "23", "24", "40", "41", "80", "81", "82", "83", "84","70", "71", "72", "73", "74", "75", "76" ="ACCESSION"
"01", "03", "04", "05", "06", "09", "13", "14", "15", "16", "17","18", "19", "30", "31", "32", "77", "78", "79", 
"5A", "5X", "5Y", "50", "51", "53", "55", "56", "57", "58", "60", "61", "62", "63" ="LOCATION"
"00" ="NC"
other="AUTRE";

value $CINCAAH
"0" ="0"
"1" ="sup80"
"2", "9" ="inf80";

value $_sitfam
'1','5','6'=" CELIB+DIV+SEP"
'2','7','3'="MARIES+PACS+VIE_MARITALE"
'4'=" VEUF";

value ageresp
low-29="RESPDOS<30ANS"
30-59="30<=RESPDOS<=59ANS"
60-high="RESPDOS>=60ANS";

value agecon
.="PAS DE CONJ"
0-29="CONJ<30ANS"
30-59="30<=CONJ<=59ANS"
60-high="CONJ>=60ANS";

value ageaine
.="AGEAINE-NC"
0-18="AGEAINE-0-18"
19-high="AGEAINE-19+";

value agebenj
.="AGEBENJ-NC"
0-18="AGEBENJ 0-18"
19-high="AGEBENJ 19+";

value $qualnir
'0','1','2'="NIR_COMPLET+NC"
'3','4'="NIR_IMCOMPLET";

value $_valnir
'3' ,'4' ,'5'="VALNIR_NONID_SNGI"
other="VALNIR_AUT";

value nirnon
0,.="0 NIRNONCER"
1-high=">=1 NIRNOCER";

value $modpaid
"  ","VB", "VP"="VIR"
other="AUTRE";
/*
value $typebai
" "="NC ou INC"
"M"="PERS. MORALE"
"P"="PERS. PHYS";
*/
value $typebai
"P"="PERS. PHYS"
" ","M",other="NC ou INC";

value $_ACT
/* Activit� normale */"ACR", "AMA", "AMT", "APP", "CCV", "CES", "CIA", "CIS", "CGP", "CSA", "DNL", "INT", "MAL", "MAT", "MOA", "PIL", "SAL", "SFP", "TSA", "VRP" ,"ETS",
/*Activit� en milieu prot�g� avec AAH */ "AAP", "CAT" , "RAC"="ACT NORMALE+ESAT"
/* entrepreneur, travailleur ind�pendant */"ETI", "CJT", "EXP", "MAR", "GSA" , "EXS"="ETI"
/* Ch�mage*/ "ABA", "AFC", "ADA", "CDA", "CPL", "FDA", "MMC" ,"ASP","ASS", "AFD", "AIN", "ADN", "CDN", "CNI", "ANI", "FDN" , "MMI"="CHOMAGE"
/*ETUDIANT*/ "EBO" , "ETU" = "ETUDIANT"
/*INACTIFetinconnu*/ "SSA", "AFA", "MOC", "SNR", "SAB", "CSS", "SAV", "SIN", "CAR" ,"CHO"="INACTIF_INCONNU"
/*AUTRES*/ 
"00","000","99"="0CONJ"
other="ACTIVITE_AUTRES";

value _abaneu
0="SANSABATNEUTRAL"
1="ABATTEMENT"
2="NEUTRAL";

value $_sexe
'1'=" HOMME"
'2'="FEMME";

value $prescj
'0'=" SANS_CONJOINT"
'2'="AV_CONJOINT";

value $resaltd
'1'=" RESALT-DOS PF"
other="RESALT_Autre";

value $tiepaal
'0'=" TIERSP_PASDAL"
'1','2'="SSTIERSP"
'3','4'=" AVTIERSP";


value NBMODMSF(fuzz=0.5 )
0-<1='0'
1-<10='de 1 � 10'
10-<21 ='de 10 � 20'
21-high ='Plus de 21';


value $DECENC
'AU','DA','DL'="DECENCE_LGT"
'IC','PI'=" INDECENCE_LGT"
' '=" DECENCE_NC";


value $MOTSITD
'AF','AM','AP'=" SITDOS_Aff(yc mut part)"
'AR', 'AT', 'RD', 'RF', 'TE','RG', 'RM', 'RO', 'RR','RS' = "SITDOS_Autre";


value dur6m
0="0"
1-12="1 � 12 mois"
12-high="12 mois et plus";

value nb24m
0,.=" 0"
1-4="de 1 � 4"
4-7=" de 4 � 6"
8-11=" de 7 � 9"
12-high="Plus de 12";

value nbprest
0,1,2="0 � 2"
3,4="3 � 4"
5-high=" 5 et plus";

value couv
0-5="moins de 6"
6-high="plus de 6";

value nben
0="0"
1="1"
2,3="2 � 3"
4-high="4 et plus";

picture sepmil 
 low-<0='000 000 000 009' (prefix='-')
 0-high='000 000 000 009' (prefix=' ');
picture sepmild
 low-<0='000 000 000 009,0' (decsep=',' prefix='-')
 0-high='000 000 000 009,0' (decsep=',' prefix=' ');

value nb35_f
0="0"
1="1"
2-high="Plus de 2";

value nb18p_f
0="0"
1="1"
2-high="Plus de 2";

value telouvu
0 - 2="0 � 2"
3-7="3 � 7"
8-15="8 � 15"
15-high="15 et plus";
run;

proc format; value RTRUC_RF(fuzz=4.440892E-16 )
0='0'
1-<100 ='1 � 100 '
100-< 200='100 � 200'
200-< 300 ='200 � 300'
300-< 99999 ='300 et plus'
99999='0 MS';
run;

proc format; value MTPFUC_F(fuzz=1.776357E-15 )
0-<213.69591859 ='de 1 � 213.7 '
213.69591859 -< 454.43788174 ='de 213.7 � 454.4 '
454.43788174 -< 695.17984489 ='de 454.4 � 695.2 '
695.17984489 -high ='sup�rieur � 695.2 '
; run;

proc format; value MTLOY_F(fuzz=0.005 )
0-<1='0'
1-<250 ='de 1 � 250'
250-<500 ='de 250 � 500'
500-high='sup�rieur � 500';
run;

proc format; value MTLOY2F (fuzz=0.005 )
0-<1='montant nul'
1-<400 ='moins de 400'
400-<600 ='400 � 600'
600-high='sup � 400';
run;

proc format; value RESTR_RF(fuzz=0.02 )
0-<1='0'
1-<200='de 1 � 200'
200-<400='de 200 � 400'
400-<99999='sup�rieur � 400'
99999 ='PASDEMS';
; run;

proc format; value RUC_RF(fuzz=1.110223E-16 )
0-<1='0'
1-<500='de 1 � 500 eur par UC'
500-<1000='de 500 � 1 000 eur par UC' 
1000-high='Plus de 1 000 eur par UC'
; run;

proc format; value MOICON_F(fuzz=0.5 )
0='0'
1-6='1�6'
7-high='7 et plus';
run;

proc format; value NMOAC12F(fuzz=0.5 )
0='0'
1-2='1�2'
3-high='3 ET PLUS';
run;

proc format; value MMONEU_F(fuzz=0.5 )
0-3='0 � 3'
4-high='4 et Plus';
run;

proc format; value MMOACT_F(fuzz=0.5 )
0-7='0 � 7'
8-high ='SUP�RIEUR � 8'
; run;

proc format; value MACT12_F(fuzz=0.5 )
0-<1='0'
1-<6 ='de 1 � 6'
6 -< 12 ='de 6 � 12'
12-high ='sup�rieur � 12'
; run;
proc format; value INDTO24F(fuzz=0.5 )
0-1='0 OU 1'
2-high='PLUS DE 2';
run;



proc format; value INFPF24F(fuzz=0.5 )
0='0'
1-high='1 ou plus';
run;
proc format; value INDAL24F(fuzz=0.5 )
0='0'
1-high='1 ou plus';
run;
proc format; value MINDTOTF(fuzz=0.5 )
0='0'
1-6='1�6'
7-12='7�12'
13-high='13 et plus';
run;

proc format; value MINDFPFF(fuzz=0.5 )
0-12='0�12'
13-high='13 ET PLUS';
run;
 
proc format; value MINDAL_F(fuzz=0.5 )
0='0'
1-6='1�6'
7-12='7�12'
13-high='13 et plus';
run;

proc format; value FGTOT24F(fuzz=0.5 )
0-<12='0�12'
12-<30 ='12�30'
30-<50='30�50'
50-high='50 et plus'
; run;
proc format; value FGFAM24F(fuzz=0.5 )
0='0'
1='1'
2-high='2 et plus';
run;

proc format; value FGENF24F(fuzz=0.5 )
0='0'
1='1'
2-high='2 et plus';
run;

proc format; value FGRES24F(fuzz=0.5 )
0='0'
1='1'
2-high='2 et plus';
run;

proc format; value FGRTR24F(fuzz=0.5 )
0='0'
1-2='1'
3-6='3�6'
7-high='7 et plus';
run;

proc format; value FGPRO24F(fuzz=0.5 )
0='0'
1='1'
2='2'
3-high='3 et plus';
run;

proc format; value MFGTOT_F(fuzz=0.5 )
0-1='0 ou 1'
2='2'
3-5='3�5'
6-high='plus de 5';
run;

proc format; value MFGFAM_F(fuzz=0.5 )
0='0'
1-4='1�4'
5-12='5�12'
13-high='13 et plus';
run;

proc format; value MFGENF_F(fuzz=0.5 )
0='0'
1-4='1�4'
5-12='5�12'
13-high='13 et plus';
run;

proc format; value MFGRES_F(fuzz=0.5 )
0='0'
1-4='1�4'
5-12='5�12'
13-high='13 et plus';
run;

proc format; value MFGRTR_F(fuzz=0.5 )
0='0'
1-6='1�6'
7-12='7�12'
13-high='13 et plus';
run;

proc format; value MFGPRO_F(fuzz=0.5 )
0='0'
1-4='1�4'
5-12='5�12'
13-high='13 et plus';
run;

proc format; value TFGRAL_F(fuzz=0.5 )
0-<1='0'
1 -high ='sup�rieur � 1 '
; run;



proc format; value NADR24_F(fuzz=0.5 )
0='0'
1='1'
2-high='2 et plus';
run;

proc format; value MADR_F(fuzz=0.5 )
0='0'
1-5='1�5'
6-12='6�12'
13-high='13 et plus';
run;

proc format; value NBCON24F(fuzz=0.5 )
0='0'
1-3='1�3'
4-12='4�12'
13-high='13 et plus';
run;

proc format; value NBVU24F(fuzz=0.5 )
0='0'
1-2='1�2'
3-10='3�10'
11-high='11 ET PLUS';
run;

proc format; value NBTEL24F(fuzz=0.5 )
0='0'
1-2='1�2'
3-7='3�7'
8-high='8 et plus';
run;

proc format; value NBAUT24F(fuzz=0.5 )
0='0'
1-high ='sup�rieur � 1'
; run;

proc format; value MCONTA_F(fuzz=0.5 )
0-12='0�12'
13-high='13 ET PLUS'
run;

proc format; value MOIVU_F(fuzz=0.5 )
0='0'
1-6='1�6'
7-12='7�12'
13-high='13 et plus';
run;

proc format; value MOITEL_F(fuzz=0.5 )
0='0'
1-6='1�6'
7-12='7�12'
13-high='13 et plus';
run;

proc format; value MOIAUT_F(fuzz=0.5 )
0='0'
1-6='1�6'
7-high='7etplus';
run;

proc format; value RMENS_RF(fuzz=0.00024735 )
0-<1='0'
1-<500 =' de 1 � 500'
500-<1000 =' de 500 � 1000'
1000 -< 2000 =' de 1 000 � 2000'
2000 -high =' sup�rieur � 2000'
; run;

proc format; value RBRU_RF(fuzz=0.004884875 )
0-<1='0'
1-<8000 ='de 1 � 8000 '
8000 -< 24000 ='de 8000 � 24000 '
24000 -high ='sup�rieur � 24000'
; run;

proc format; value MTPF_F(fuzz=0.005 )
0-<1='0'
1-<300 ='de 1 � 300 '
300 -< 700 ='de 300 � 700 '
700 -< 1200 ='de 700 � 1200 '
1200 -high ='sup�rieur � 1200 '
; run;

proc format; value MTLOGC_F(fuzz=0.005 )
0-<1='0'
1-<150 ='de 1 � 150'
150 -< 300 ='de 150 � 300'
300-high ='sup�rieur � 300'; 
run;

Proc Format;
value MOINSC(fuzz=0.5 )
low-< 11.428996785 ='inf�rieur � 11.4 '
11.428996785 -< 58.22710459 ='de 11.4 � 58.2 '
58.22710459 -< 105.02521239 ='de 58.2 � 105 '
105.02521239 - high ='sup � 105'; 

value FGRTR24B(fuzz=0.5 )
0-2='0�2'
3-6='3�6'
7-high='7 et plus';

value NBMODMSB(fuzz=0.5 )
0-<10='0 � 10'
10-<21 ='de 10 � 20'
21-47='de 20 � 48'
48-high ='Plus de 48';

value difage_B
.="0 Conj    "
-2,-1,0,1,2="-2 � 2 (H-F)"
8-high="8 et plus (H-F)"
other="Autres (H-f)";

value PPRPPU_B (fuzz=1.110223E-16 )
0='0AL'
1='PUBLIC'
2,9='PRIVE';


value $OCLOG_B
"20", "21", "22", "23", "24", "40", "41", "80", "81", "82", "83", "84","70", "71", "72", "73", "74", "75", "76" ="ACCESSION"
/*"01", "03", "04", "05", "06", "09", "13", "14", "15", "16", "17","18", "19", "30", "31", "32", "77", "78", "79", 
"5A", "5X", "5Y", "50", "51", "53", "55", "56", "57", "58", "60", "61", "62", "63" ="LOCATION"
"00" ="NC"*/
other="AUTRE";
run;


/*NOUVEaUX FORMaTS UTILISES DaNS LES MODELES aU 1307*/
/*TOUS aVEC DES NOMS deMOINS de8 CaR*/
/*Ils finissent tous par _P*/

proc format; 

value nben_p
0,1="0ou1"
2,3="2 � 3"
4-high="4 et plus";

value LOGCaL_P(fuzz=0.005 )
0-<1='0'
1-<150 ='de1a150'
150 -< 300 ='de150a300'
300-high ='superieura300'; 

value PREST_P(fuzz=0.005 )
0-<1='0'
1-<300 ='de1a300'
300 -< 700 ='de300a700'
700 -< 1200 ='de700a1200'
1200 -high ='plusde1200';

value difage_P
.="0Conj"
-2,-1,0,1,2="-2a2(H-F)"
8-high="8etplus(H-F)"
other="autres(H-f)";

value $resalt
'1'="RESALT_PF"
other="RESALT_AUT";

value FGTOTM_P
0-<0.3="0a0.3"
0.3-<0.7="0.3a0.7"
0.7-<1.5="0.7a1.4"
1.5-high="1.5etplus";

value MFGTOT_P(fuzz=0.5 )
 0='0'
 1='1'
 2-high='2etplus';

value MINSDO_P(fuzz=0.5 )
0-24='0a24'
25-48='25a48'
49-high ='49etplus'; 

value NONTaM_P(fuzz=0.0009057971 )
 0='0 '
 0.00000000000001-0.12='de0a0.12'
 0.1200000000001-0.335='de0.13a0.33 '
 0.3350000000001-high='0.33etplus';

 value TELVU_P
0 - 2="0a2"
3-7="3a7"
8-15="8a15"
15-high="15etplus";

 value MONTa_P(fuzz=0.5 )
0-12='0a12'
13-high='13etplus';

value $MOPaI_P
"VB","VP"="VIR"
other="aUTRE";

value $OCLRM_P
'ACC','PRO' ="PROPRIO"
'000'="0_autre"
other="0_autre";

value $OCLOG_P
"20", "21", "22", "23", "24", "40", "41", "80", "81", "82", "83", "84","70", "71", "72", "73", "74", "75", "76" ="aCCESSION"
/*"01", "03", "04", "05", "06", "09", "13", "14", "15", "16", "17","18", "19", "30", "31", "32", "77", "78", "79", 
"5a", "5X", "5Y", "50", "51", "53", "55", "56", "57", "58", "60", "61", "62", "63" ="LOCaTION"
"00" ="NC"*/
other="aUTRE";

value $TYPCa_P
'0'="aSSL_PaSDaL "
'F','D','I'="aUTRE TYPCaL"
'N'="CaLC SYS"
other="aUTRE TYPCaL";

value INTO24_P(fuzz=0.5 )
0,1='0 ou 1'
2-high='Plus de2';

value MINDT_P(fuzz=0.5 )
0='0'
1-6='1a6'
7-12='7a12'
13-high='13etplus';


value MINFNP_P(fuzz=0.5 )
0-12='0a12'
12-high='13etplus';

value MINaL_P(fuzz=0.5 )
0-12='0a12'
12-high='13etplus';

value MCONT_P(fuzz=0.5 )
0-6='0a6'
7-high='7etplus';

value NaCT12_P(fuzz=0.5 )
0='0'
1-2='1a2'
3-high='3etplus';

value MFGPRO_P(fuzz=0.5 )
0='0'
1-4='1a4'
5-12='5a12'
13-high='13etplus';

value MMODaC_P(fuzz=0.5 )
0-7 ='0a7'
8-high ='superieura8';

value MFGRTR_P(fuzz=0.5 )
0-6='0a6'
7-high='7etplus';

value NBODMS_P(fuzz=0.5 )
0='0MS'
1-<7='1a7'
8-high ='plus de8a20';

value MFGFaM_P(fuzz=0.5 )
0-4='0a4'
5-12='5a12'
13-high='13etplus';

value FGFaMM_P
0="0"
0.000000001-<0.1="0a0.1"
0.1-<0.2="0.1a0.2"
0.2-high="0.2etplus";

value MFGRES_P(fuzz=0.5 )
0='0'
1-4='1a4'
5-12='5a12'
13-high='13etplus';

value MMaBN_P(fuzz=0.5 )
0-3='0a3'
4-high='4etplus';

value $aSF_P
"0"  = "0asf"
"1", "8" = "asfR"
"3" = "asfNR"
other="PB";

value MaDRES_P(fuzz=0.5 )
0-10='0a10'
11-high='11et plus';

value PPRPPU_P(fuzz=1.110223E-16 )
0='0aL'
1='PUBLIC'
2,9='PRIVE';

value TXEFF_P(fuzz=1.110223E-16 )
-1,-2,0-<35='0aLettxeff 0a35'
35-high='35etplus';

value RaNRED_P(fuzz=0.004884875 )
0-<1='0'
1-<8000 ='de1a8000'
8000 -< 24000 ='de8000a24000'
24000 -high ='superieura24000'; 

value RMRED_P(fuzz=0.00024735 )
0-<1='0'
1-<500 ='de1a500'
500-<1000 ='de500a1000'
1000 -< 2000 ='de1000a2000'
2000 -high ='2000PLUS';

run;



/*DEFINITION CIBLE DATAMINING --> production de mod�le*/
/*%let cib=Indu6mois;*/
/*%let def= indu=1 and durind_red>=6;*/

/*VARIABLE D'ETUDES A GARDER POUR LE MODELE*/
%let varcible = MS AL PF pondech	pondalloc	mtind_red 	durind_red 	indu 	qualif	commission	mtrapreg_red
durap_red 	rappel	MTINCAFCF 	MTINCPAJE 	MTINCAPI  	MTINCRMI 	MTINCAL	MTINCASF	MTINCFNPF	INCAFCF
INCPAJE	INCAPI	INCRMI	INCAL	INCASF	INCFNPF	mod_connue	mod_sitpro	mod_log	mod_ress	mod_enf	mod_sitfam
mod_RMI	mod_Autre_mod	resp_erreural	resp_meco	resp_absdem	resp_caf	resp_oubli	resp_tiers	resp_fraud
resp_al	TETRREGC
TOPMADRC	TOPMPERC	TOPMFAMC	TOPMRESC	TOPMAPIC	TOPMRMIC	TOPMACTC	TOPMRSAC	TOPMLOYC	TMSIREGC;

%let var_ab = ABANEU FGENFAUT24_F FGFAM12_F FGRESTRI24_F2 MOICONTCAF_F
MOIINDFNPF_F MOIMODACT_F MTLOYREM_F2 MTPREST_F NBENF02_F NBM_ODMS_F2 NBONTATEL12_F NBONTAVU12_F
NBPREST_F NMODACT12_F  PERSCOUV_F2 PPRPPU_B  TOPEN18 TUT
CONTOT12_F MTLOYREM_F2 NBPREST_F NIRNONCER_F TOPASF FGTOT24_F SITFAM2 AGERESP_F MOIMODNEU_F
NBENF02_F MOINSCDO_F2 MOICONTCAF_F
TELOUVU_F NBONTATEL24_F  MODPAIDO_F NBENFCHA_F DIFAGE_B INDTOT24_F NBONTAVU24_F TOP16SCO
TOPASF MOIONTA_F MTLOYREM_F MTPREST_F NBENF02_F NBENF35_F NBPREST_F PERSCOUV_F2 TOP16SSA TOPAAH
FGFAM24_f FGTOT24_F CMG2 TELOUVU_F MOINSCDO DTINSCDO_dernier DTREFFRE MS AL;

%let var_p=ACTRESCON_P   ageresp_f agecon_f difage_P
	nirnoncer_P	nbenfcha_P	perscouv_P	resaltdo_P	agebenj_f ageaine_f TOPENF02_P	
TOPENF35_P	TOPENF611_P	TOPENF1218_P topenf18p	top16act_P	top16ssa_P	
FGTOTMOI_P	MOIFGTOT_P MOINSCDO_P		TELOUVU_P	MOIONTA_P	MODPAIDO_P	
 TYPCALAL_P	INDTOT24_P	MOIINDTOT_P	MOIINDFNPF_P MOIINDAL_P	MOICONTCAF_P	
NMODACT12_P	MOIFGSITPRO_P	MOIMODACT_P	MS	MOIFGRESTRI_P nbm_odms_P	MOIFGFAM_P	 FGFAMMOI_P 
TOPSIT12_P	MOIFGRESAN_P topfgresall_P	MOIMODNEU_P	ABANEU	TDISASFA	ASFVERS_P	MOIADRESS_P	
PPRPPU_P tut_P AAH_COMPAAH_P	ZONEGEO	MTPREST_P TXEFF_P MTREVBRU_RED_P	
MTPREST MTREVBRU_RED ;

data  datamining&mvMOISFRM.&mvANNEEFRM
(keep= MATRICUL NUMCAF NORDALLC MS &var_p &var_ab &varcible);
set  datamining&mvMOISFRM.&mvANNEEFRM (where = (mtpfvers>0 or mtpremve>0));/*Allocs avec PF vers�es*/

PF=0;
if ((AFVERS ne "0") or (CFVERS ne "0") or (PAJEVERS ne "0") or (ASFVERS ne "0") or (AESVERS ne "0")
              or (ARSVERS ne "0") or (AJPPVERS ne "0") or (NBENAADO ne 0)) then PF=1;



/*Remplacement de certaines observations vides*/
if dtnaires=. then dtnaires=mdy(1,1,1970);

/************************************************************************/
/*		0- Modification de variables suite � �volution du DII			*/
/************************************************************************/

if RSAVERS ne '0' then ANODRMI=year(RSDTDORI);
if RSAVERS ne '0' then moisodrm=month(RSDTDORI);
if TOPRETUT>'0' then TUT="TUTELLE"; else TUT="0";
if RSAVERS ne 'C' then MTPREMVE=0;


/***********************************************************************/
/*I CREATION DE VARIABLES SUR VARIABLES SOURCES DIRECTES ALLCAF DU MOIS*/
/***********************************************************************/

/*DEFINITION CIBLE DATAMINING --> uniquement ^pour production de mod�les*/ 
/*if &def then CIBLE="&cib"; else CIBLE="Horscible";
if CIBLE="Horscible" and indu>0 then delete;*/


/*Cr�ation de variables explicatives par application de formats*/
sexe_f=put(sexe,$_sexe.);
presconj_f=put(presconj,$prescj.);
ACTRESPD_f=put(ACTRESPD,$_act.);
ACTCONJ_f=put(ACTCONJ,$_act.);
ABANEURE_f=put(ABANEURE,_abaneu.);
ABANEUCO_f=put(ABANEUCO,_abaneu.);
TIEPAALI_f=put(TIEPAALI,$Tiepaal.);
DECENCLO_f=put(DECENCLO,$DECENC.);
MOTISITD_f=put(MOTISITD,$MOTSITD.);
resaltdo_f=put(resaltdo,$resaltd.);
perscouv_f=put(perscouv,couv.);
nbenfcha_f=put(nbenfcha,nben.);
nirnoncer_f=put(nirnoncer_dernier,nirnon.);
modpaido_f=put(MODPAIDO_dernier,$modpaid.);
typebai_f=put(TYPEBAI_dernier,$typebai.);
/*Ajout 14/06*/
CINCAAH_f=put(CINCAAH,$CINCAAH.);
MTLOYREM_f= put(MTLOYREM,MTLOY_f.);
MTLOGCAL_f= put(MTLOGCAL,MTLOGC_f.);


/*Cr�ation de variable explicatives en code sur variables existantes*/


/*SITFAM 2 et 3 : Recodage classique (ajout 14/06): iso, coups, coupa, monop*/
attrib Sitfam2 label="Sitfam recod� simple"	format=$6. informat=$6.;
if presconj="0" and nbenlefa=0 and sexe="1" then sitfam2="iso_h";
if presconj="0" and nbenlefa=0 and sexe="2" then sitfam2="iso_f";
if presconj="0" and nbenlefa>0 then sitfam2="monop";
if presconj="2" and nbenlefa=0 then sitfam2="coups";
if presconj="2" and nbenlefa>0 then sitfam2="coupa";

attrib Sitfam3 label="Sitfam recod� simple"	format=$6. informat=$6.;
if presconj="0" and nbenlefa=0 and sexe="1" then sitfam3="iso_h";
if presconj="0" and nbenlefa=0 and sexe="2" then sitfam3="iso_f";
if presconj="0" and nbenlefa>0 then sitfam3="monop";
if presconj="2" then sitfam3="coupl";

/*Age resp. et conjoint*/ 
if dtnaires ne . then ageresp=round ( INTCK("MONTH",dtnaires,dtreffre) /12 );
if dtnaicon ne . then agecon=round ( INTCK("MONTH",dtnaicon,dtreffre) /12 );

/*Age recod� en classes*/
ageresp_f=put(ageresp,ageresp.);
agecon_f=put(agecon,agecon.);  /* . si pas de conjoint*/

/*Ecart d'�ge entre responsable et conjoint*/
attrib Difage label="Diff�rence Age entre homme et femme" format=3. informat=3.;
	if agecon = . then Difage=.; 
		else do;
			if sexe='1' then difage=ageresp-agecon;
			else difage=agecon-ageresp;
		end;

/*Age du plus jeune enfant*/ 
/*Age du plus ag�*/
array ann {12} annnen1 annnen2 annnen3 annnen4 annnen5 annnen6 annnen7 annnen8 annnen9 annnen10 annnen11  annnen12;
topen18=0; topen19=0; 
do i=1 to 12; 
	if (ann(i) not in (0 .))  then do; 
		ageaine=year(dtreffre)-ann(i); 
		if (year(dtreffre)-ann(i)) >= 18 then   topen18=1;  
		if (year(dtreffre)-ann(i)) >= 19 then   topen19=1;  
	end;
end;
	if annnen1 not in (0 .) then agebenj=year(dtreffre)-annnen1;
/*Recodage de l'�ge en classes*/
ageaine_f=put(ageaine,ageaine.);
agebenj_f=put(agebenj,agebenj.);

/*Age des enfants*/
/*ANNNEN1-12 et MOINEN1-12 : Alimentation des compteurs Age enfant*/
attrib Nbenf02 label="Compteur : nb de 0 � 2 ans r�volus" format=1. informat=1.;
attrib Nbenf35 label="Compteur : nb de 3 � 5 ans r�volus" format=1. informat=1.;
attrib Nbenf611 label="Compteur : nb de 6 � 11 ans r�volus" format=1. informat=1.;
attrib Nbenf1218 label="Compteur : nb de 12 � 18 ans r�volus" format=1. informat=1.;
attrib Nbenf18p label="Compteur : nb de plus de 18 ans" format=1. informat=1.;
attrib datenai format=yymmdd10.;
array mois[12] moinen1-moinen12;
array annee[12] annnen1-annnen12;
Nbenf02=0; Nbenf35=0; Nbenf611=0; Nbenf1218=0; Nbenf18p=0;
do i=1 to min(nbenlefa,12);
	if annee[i] in (0,.) then datenai=.;
	else do;
		if mois[i] in (0,.) then mois[i]=6;
   		datenai=mdy(mois[i],1,annee[i]);
   		age=int(datdif(datenai,dtreffre,'ACT/ACT')/365);
		end;
if (age < 3) then Nbenf02=Nbenf02+1;
if (age >= 3 and age < 6) then Nbenf35=Nbenf35+1;
if (age >= 6 and age < 11) then Nbenf611=Nbenf611+1;
if (age >= 12 and age < 18) then Nbenf1218=Nbenf1218+1;
if (age >= 18) then Nbenf18p=Nbenf18p+1;
end;

if nbenf02 > 0 then TOPENF02='1'; else TOPENF02='0'; 
if nbenf35 > 0 then TOPENF35='1'; else TOPENF35='0'; 
if nbenf611 > 0 then TOPENF611='1'; else TOPENF611='0'; 
if nbenf1218 > 0 then TOPENF1218='1'; else TOPENF1218='0'; 
if nbenf18p > 0 then TOPENF18p='1'; else TOPENF18p='0'; 


/*Nombre d'unit�s de consommation*/
NBUC=0; 
label NBUC="Nb unit�s de conso.";
array tab{12} annnen1 annnen2 annnen3 annnen4 annnen5 annnen6 annnen7 annnen8 annnen9 annnen10 annnen11 annnen12;
n13=nbnaimoi;n14=0;n611=0;n1214=0;n1518=0;
do i=1 to 12;
	select(tab{i});
	when (0);
	otherwise do;
		if 2008-tab{i} <= 13 then n13=n13+1;
		if 2008-tab{i} >13 then n14=n14+1;
		end;end;end;

If (PRESCONJ='0') then do;
	If (n13+n14=0) then NBUC=1.0;
	else NBUC=1.2 +(0.3*n13) +(0.5*(n14));
end; 
else NBUC=1.0 +(0.3*n13) +(0.5 * (1+ n14)); 

/*Activit� des enfants*/
attrib top16act label="Top enfant +16 ans actif" format=1. informat=1.;
attrib top16sco label="Top enfant +16 ans scolaire" format=1. informat=1.;
attrib top16ssa label="Top enfant +16 ans sans activit� ou inconnus" format=1. informat=1.;
array TABENF1[12] qualen1 qualen2 qualen3 qualen4 qualen5 qualen6 qualen7 qualen8 qualen9 qualen10 qualen11 qualen12 ;
top16act=0; top16sco=0; top16ssa=0;
do i=1 to min(nbenlefa,12) ;
  		if (tabenf1[i] in ("2")) then top16sco=1;
		if (tabenf1[i] in ("3" "5" "6" "A")) then top16act=1;
		if (tabenf1[i] in ("4" "7" "8" "9")) then top16ssa=1;
end;

/*GESTION DES REVENUS INCONNUS*/
if MTREVBRU=. then mtrevbru=999999;
if restrrmi=. then restrrmi=9999;
if restrapi=. then restrapi=9999;
if MTLOYREM>9000 then mtloyrem=9999;
if mtassiai>90000 then mtassiai=999999;

/* Calcul des revenus hors PF*/
/* Pour les allocataires API et RMI, revenus trim, pour tous les autres rev annuels */
if (RMIVERS in ('1','2') or APIVERS  in ('1' '2')) then do;
	if (restrrmi> 9000 and restrapi>9000) then do; revmens=99999; end;
	if (restrrmi<=9000 and restrapi>9000) then do; revmens=restrrmi/3; end;
	if (restrrmi> 9000 and restrapi<=9000) then do; revmens=restrapi/3; end;
	if (restrrmi<=9000 and restrapi<=9000) then do; revmens=max(restrapi,restrrmi)/3; end;
end;

/*AJOUT CODE POUR RESTRRSA*/
else if RSAVERS ne '0' then do;
 if restrrsa<=9000 then revmens=max(RESTRRSA,MTREMRSV*3)/3; 
 else if restrrsa>9000 then revmens=99999; 
end;
 else if mtrevbru>900000 then REVMENS = 99999; 
 else revmens=round(mtrevbru/12); 


/*Revenus trimestriels d'activit�*/
if (RMIVERS > '0' or APIVERS >'0' or RSAVERS ne "0") then do;
	MTTRIACT=sum(0,( (restraco<9998)*restraco ) + ( (restrace<9998)*restrace ) ); 
	if restraco>=9998 and restrace>=9998 then MTTRIACT=99999;
end;
else MTTRIACT=99999;

MTTRIACT_f=put(MTTRIACT,MTTRIA_F.);

/*Montant prestations total Pf+ARE*/ 
mtprest=sum(mtpremve, mtpfvers); if mtprest=. then mtprest=0;
MTPREST_f= put(MTPREST,MTPF_f.);
/*Montant de prestation par UC*/
mtprestuc=mtprest/nbuc;
mtprestuc_f=put(mtprestuc,mtpfuc_f.);

/*Nombre de prestations vers�es et regroupement de prest*/
NBPREST=(ADIVERS>'0')+(PAJDROB>'0')+(PAJCATY>'0')+(PAJCOLCA>'0')+(PAJNREA1>'0')+(PAJNRED1>'0')+(PAJCMAS1>'0') + 
(ASFVERS>'0') + (AESVERS>'0') + (AFVERS>'0') + (CFVERS>'0') + (APLVERS>'0') + 
(ALFVERS>'0') + (ALSVERS>'0') + (RMIVERS>'0') + (APIVERS>'0') + (AAHVERS>'0') + (COMPLAAH>'0') + (RSOVERS>'0')
+ (PREMVMON>'0') + (PREMVMME>'0') + (PREMVAUT>'0') +(RSAACT>'0') +(RSASOCL>'0') + (RMAVERS>'0') + (CAVVERS>'0');
NBPREST_f=put(nbprest,nbprest.);


/*Groupe de prest: AF et CF, PAJE, AL, MS hors aah, AUTRES*/
if ((ALFVERS>'0')  or (ALSVERS>'0') or  (APLVERS>'0')) then AL="AL"; else AL="0";
if (apivers in ('1' '2') or rmivers in ('1' '2') or rsovers>'0' or CAVVERS>'0' or PREMVAUT>'0' or PREMVMME>'0' or PREMVMON>'0' or RMAVERS>'0' or RSAVERS in ("C" "L")) then MS="MS (+ARE)"; else MS="0";
if  ASFVERS>'0' then ASF="ASF";else ASF="0";
if (pajevers>'0' ) then PAJE_AFEAGED="PAJE";else PAJE_AFEAGED="0";
if afvers>'0' or cfvers>'0' then AF_CF="AF CF";else AF_CF="0";
if aahvers>'0' or complaah>'0' then AAH_COMPAAH="AAH"; else AAH_COMPAAH="0"; 
if aahvers>'0' or complaah>'0' then AAH_COMPAAH_P="AAH"; else AAH_COMPAAH_P="0"; 

/*Type d'aide au logement*/
attrib Plogvers label="Prestation logement versable" format=$10. informat=$10.;
if (alsvers="0" and alfvers="0" and aplvers="0") then plogvers="0PL"; else
if alsvers = "1" then plogvers="ALS"; else
if alfvers = "1" then plogvers="ALF"; else 
if aplvers = "1" then plogvers="APL"; else
if (aplvers = "2" or alsvers = "2" or alfvers = "2") then plogvers="Infseuil";

/*PAJE CLCA et COLCA*/
attrib CLCA label="CLCA versable recod�" format=$4. informat=$4.;
if pajcaty in ("2" "3" "4")  then CLCA="TRed"; else
if pajcaty in ("1" "5") or pajcolca="1" then CLCA="TPl"; else
CLCA="0CLCA";

/*PAJE CMG*/
attrib CMG label="CMG versable recod�" format=$6. informat=$6.; 
if (pajnrea1 ne "0") then CMG="Assmat"; else
if (pajnred1 ne "0") then CMG="Adom"; else
if PAJCMAS1>'0' then CMG="Autaf"; else CMG="0CMG";
/*V2*/
attrib CMG2 label="CMG versable recod�" format=$6. informat=$6.;
if (pajnrea1 ne "0") then CMG2="Assmat"; else
if (pajnred1 ne "0" or PAJCMAS1>'0') then CMG2="Autre"; else CMG2="0CMG";

/*Suivi des prestations*/

/*Nb mois depuis OD RMI*/if moisodrm>0 then NBM_ODRMI=INTCK("MONTH",mdy(moisodrm,1,anodrmi),dtreffre); else NBM_ODRMI=0;
/*Nb mois depuis OD API*/if dtdemapi>0 then NBM_ODAPI=INTCK("MONTH",dtdemapi,dtreffre); else NBM_ODAPI=0;
/*Anciennet� en classes*/
NBM_ODMS=max(nbm_odrmi,nbm_odapi);
NBM_ODMS_f=put(nbm_odms,nbmodmsf.);
NBM_ODMS_f2=put(NBM_ODMS,nbmodmsB.);

/*Tutelle*/
/*if NATTUT>'0' then tut="TUTELLE"; else tut="0";*/

/*Abattement ou neutral*/
if ( ( abaneure in (1 2)) or (abaneuco in  (1 2)) ) then ABANEU="Ab. ou neutral."; else ABANEU="Aucun";
if  abaneure=2 and abaneuco=2 then ABANEU="Double neutral";

/*Labels des variables cr��es*/
label ageresp= "AGE RESP. DOS";
label agecon= "AGE CONJOINT";
label ageresp_f= "AGE RESP. DOS";
label agecon_f= "AGE CONJOINT";
label sexe_f= "SEXE";
label presconj_f= "SITUATION FAMILIALE";
label nirnoncer_f="TOP NIR NON CER";
label nbenfcha= "NB ENFANTS CHARGE PF";
label nbenfcha_f= "NB ENFANTS CHARGE PF";
label perscouv_f= "NB PRESONNES COUVERTES";
label resaltdo_f= "RESIDENCE ALTERNEE";
label ageaine= "AGE DE L'AIN�";
label agebenj= "AGE BENJAMIN";
label ageaine_f= "AGE DE L'AIN� EN CLASSES";
label agebenj_f= "AGE BENJ EN CLASSES";
label TOPEN18= "Top enfant >18 ans";
label TOPEN19= "Top enfant >19 ans";
label ACTRESPD_f= "ACTIVIT� RESP DOS.";
label ACTCONJ_f= "ACTIVIT� CONJOINT";
label ABANEURE_f= "ABATTEMENT OU NEUTRAL RESP.";
label ABANEUCO_f= "ABATTEMENT OU NEUTRAL CONJOINT";
label ABANEU= "TOP ABATTEMENT OU NEUTRAL POUR ALLOC OU CONJOINT";
label REVMENS= "REVENUS MENSUALISES TRIM OU AN2";
label MTASSIAI= "MT ASSIETTE AIDE AU LOGEMENT";
label NBPREST= "NB PRESTATIONS VERS�ES";
label NBPREST_f= "NB PRESTATIONS VERS�ES";
label AF_CF= "DROIT AUX AF ou CF";
label PAJE_AFEAGED= "DROIT PAJE";
label AL= "DROIT AUX AL";
label MS= "DROIT � UN MINIMUM SOCIAL (YC ARE)";
label ASF= "DROIT ASF";
label AAH_COMPAAH= "DROIT AAH";
label MTPREST= "SOMME MT PF+ARE";
label TIEPAALI_f= "TIERS PAYANT AL";
label NBM_ODRMI= "NB MOIS DEPUIS OD RMI";
label NBM_ODAPI= "NB MOIS DEPUIS OD API";
label NBM_ODMS= "NB MOIS DEPUIS OD MS (API ou RMI)";
label NBM_ODMS_f= "NB MOIS DEPUIS OD MS (API ou RMI)";
label TUT= "TUTELLE";
label DECENCLO_f= "D�CENCE LOGEMENT";
label MOTISITD_f= "SITUATION DU DOSSIER";
label RESTRRMI= "REVENUS TRIMESTRIELS (MS seulement)";

/***********************************************************************/
/*II CREATION DE VARIABLES SUR VARIABLES HISTORIQUE ALLCAF             */
/***********************************************************************/
IF NBFGRESALL>0 THEN topfgresall=1; else topfgresall=0;

array typec tc0610 tc0510 tc0410 tc0310 tc0210 tc0110 tc1209 tc1109 tc1009 tc0909 tc0809 tc0709 tc0609 tc0509 tc0409 tc0309 tc0209 tc0109 tc1208 tc1108 tc1008 tc0908 tc0808 tc0708;
array cibv CIBV0610 CIBV0510 CIBV0410 CIBV0310 CIBV0210 CIBV0110 CIBV1209 CIBV1109 CIBV1009 CIBV0909 CIBV0809 CIBV0709 CIBV0609 CIBV0509 CIBV0409 CIBV0309 CIBV0209 CIBV0109 CIBV1208 CIBV1108 CIBV1008 CIBV0908 CIBV0808 CIBV0708;
array cibc CIBC0610 CIBC0510 CIBC0410 CIBC0310 CIBC0210 CIBC0110 CIBC1209 CIBC1109 CIBC1009 CIBC0909 CIBC0809 CIBC0709 CIBC0609 CIBC0509 CIBC0409 CIBC0309 CIBC0209 CIBC0109 CIBC1208 CIBC1108 CIBC1008 CIBC0908 CIBC0808 CIBC0708;
array fam fam0610 fam0510 fam0410 fam0310 fam0210 fam0110 fam1209 fam1109 fam1009 fam0909 fam0809 fam0709 fam0609 fam0509 fam0409 fam0309 fam0209 fam0109 fam1208 fam1108 fam1008 fam0908 fam0808 fam0708;
array post post0610 post0510 post0410 post0310 post0210 post0110 post1209 post1109 post1009 post0909 post0809 post0709 post0609 post0509 post0409 post0309 post0209 post0109 post1208 post1108 post1008 post0908 post0808 post0708;
array actcon ACJ0610 ACJ0510 ACJ0410 ACJ0310 ACJ0210 ACJ0110 ACJ1209 ACJ1109 ACJ1009 ACJ0909 ACJ0809 ACJ0709 ACJ0609 ACJ0509 ACJ0409 ACJ0309 ACJ0209 ACJ0109 ACJ1208 ACJ1108 ACJ1008 ACJ0908 ACJ0808 ACJ0708;
array actresp ACR0610 ACR0510 ACR0410 ACR0310 ACR0210 ACR0110 ACR1209 ACR1109 ACR1009 ACR0909 ACR0809 ACR0709 ACR0609 ACR0509 ACR0409 ACR0309 ACR0209 ACR0109 ACR1208 ACR1108 ACR1008 ACR0908 ACR0808 ACR0708;
array abno ABNO0610 ABNO0510 ABNO0410 ABNO0310 ABNO0210 ABNO0110 ABNO1209 ABNO1109 ABNO1009 ABNO0909 ABNO0809 ABNO0709 ABNO0609 ABNO0509 ABNO0409 ABNO0309 ABNO0209 ABNO0109 ABNO1208 ABNO1108 ABNO1008 ABNO0908 ABNO0808 ABNO0708;
array abne ABNE0610 ABNE0510 ABNE0410 ABNE0310 ABNE0210 ABNE0110 ABNE1209 ABNE1109 ABNE1009 ABNE0909 ABNE0809 ABNE0709 ABNE0609 ABNE0509 ABNE0409 ABNE0309 ABNE0209 ABNE0109 ABNE1208 ABNE1108 ABNE1008 ABNE0908 ABNE0808 ABNE0708;
/*Liste des variables historique ALLNAT*/

label MOICONTCAF="NB MOIS DEPUIS DERNIER CONTR�LE CAF, DONN�E ENTRANTES OU V�RIFICATION (HORS �CHANGES AUTOMATIQUES)";
label TOPSIT12="AU MOINS UNE MODIFICATION DE SITUATION FAMILLE SUR 12 MOIS";
label NMODACT12="NB DE MODIFICATION ACTIVIT� SUR 12 MOIS (RESP+CONJ)";
label MOIMODNEU="NOMBRE DE MOIS DEPUIS LA DERNI�RE MODIFICATION RESSOURCES ANNUELLES CONJ+RESP (ABAT/NEUTRA) -HORS AAH";
label TOPAD12="TOP MODIFICATION ADRESSE SUR DOSSIERS AFFILI�S SUR 12 MOIS";
label MACT12="NB MOIS EN ACTIVIT� SUR 12 MOIS (RESP et CONJ)";

/*MOICONTAG=0; */ 
do i=1 to 16; if typec(i) in ("AG" "FR") then do; MOICONTAG=i; i=16; end;end;
/*MOICONTPI=0; */
do i=1 to 16; if typec(i)="PI" then do; MOICONTPI=i; i=16; end;end;
/*MOICONTVER=0; */
do i=1 to 16; if cibv(i) >'0' then do; MOICONTVER=i; i=16; end;end;
/*Cr�ation d'une dur�e depuis un contr�le caf par agent / pi�ce / v�rif*/
MOICONTCAF=min(MOICONTAG, MOICONTPI, MOICONTVER);
if MOICONTCAF=. then MOICONTCAF=0;
/*Cr�ation d'un nombre de contr�les en 12 mois*/
CONTOT12=0; do i=1 to 12; if typec(i) in ("AG" "PI" "FR") or cibv(i)>'0' then CONTOT12=CONTOT12+1; end; 

MOIMODSIT=0; histosit=fam&mvMOISFRM.&mvANNEEFRM; do i=1 to 16; if fam(i) ne histosit and fam(i) ne " " then do; MOIMODSIT=i-1; i=16; end;end;
histosit=fam&mvMOISFRM.&mvANNEEFRM; NMODSIT12=0; do i=1 to 12; if fam(i) ne histosit and fam(i) ne " " then do; histosit=fam(i); NMODSIT12=NMODSIT12+1; end;end;
TOPSIT12=NMODSIT12>0;

histoactr=acr&mvMOISFRM.&mvANNEEFRM;  do i=1 to 16; if ACTRESP(i) ne histoactr and actresp(i) ne " " then do;  MOIMODACTR=i-1; i=16; end;end;
histoactc=acj&mvMOISFRM.&mvANNEEFRM; do i=1 to 16; if ACTCON(i) ne histoactc and actcon(i) ne " " then do; MOIMODACTC=i-1; i=16; end;end;
MOIMODACT=min(MOIMODACTC,MOIMODACTR);
if MOIMODACT=. then MOIMODACT=0;

histoactr=acr&mvMOISFRM.&mvANNEEFRM; NMODACT12R=0;  do i=1 to 12; if ACTRESP(i) ne histoactr and actresp(i) ne " " then do; histoactr=ACTRESP(i); NMODACT12R=NMODACT12R+1; end;end;
histoactc=acj&mvMOISFRM.&mvANNEEFRM; NMODACT12C=0;  do i=1 to 12; if ACTCON(i) ne histoactc and actcon(i) ne " " then do; histoactc=ACTCON(i); NMODACT12C=NMODACT12C+1; end;end;
NMODACT12=sum(NMODACT12R, NMODACT12C);

histoneur=abne&mvMOISFRM.&mvANNEEFRM;  do i=1 to 16; if abne(i) ne histoneur and abne(i) ne . then do; MOIMODNEUR=i-1; i=16; end;end;
histoneuc=abno&mvMOISFRM.&mvANNEEFRM;  do i=1 to 16; if abno(i) ne histoneuc and abno(i) ne . then do; MOIMODNEUC=i-1; i=16; end;end;
MOIMODNEU=min(MOIMODNEUC, MOIMODNEUR);
if MOIMODNEU=. then MOIMODNEU=0;

histoneur=abne&mvMOISFRM.&mvANNEEFRM; MODNEUR12=0;  do i=1 to 12; if abne(i) ne histoneur and abne(i) ne . then do; histoneur=abne(i); MODNEUR12=MODNEUR12+1; end;end;
histoneuc=abno&mvMOISFRM.&mvANNEEFRM; MODNEUC12=0;  do i=1 to 12; if abno(i) ne histoneuc and abno(i) ne . then do; histoneuc=abno(i); MODNEUC12=MODNEUC12+1; end;end;
MODNEU12=sum(MODNEUR12,MODNEUC12);

histoad=post&mvMOISFRM.&mvANNEEFRM; NMODAD12=0; do i=1 to 12; if post(i) ne histoad and post(i) ne . then do; histoad=post(i); NMODAD12=NMODAD12+1; end;end;
topad12=nmodad12>0;

MACT12R=0; do i=1 to 12; if ACTRESP(i) in (/* Activit� normale */'ACR', 'AMA', 'AMT', 'APP', 'CCV', 'CES', 'CIA', 'CIS', 'CGP', 'CSA', 'DNL', 'INT', 'MAL', 'MAT', 'MOA', 'PIL', 'SAL', 'SFP', 'TSA', 'VRP' ,'ETS',
/*Activit� en milieu prot�g� avec AAH */ 'AAP', 'CAT' , 'RAC',
/* entrepreneur, travailleur ind�pendant */'ETI', 'CJT', 'EXP', 'MAR', 'GSA' , 'EXS')  then do; MACT12R=MACT12R+1; end;end;
MACT12C=0; do i=1 to 12; if ACTCON(i) in (/* Activit� normale */'ACR', 'AMA', 'AMT', 'APP', 'CCV', 'CES', 'CIA', 'CIS', 'CGP', 'CSA', 'DNL', 'INT', 'MAL', 'MAT', 'MOA', 'PIL', 'SAL', 'SFP', 'TSA', 'VRP' ,'ETS',
/*Activit� en milieu prot�g� avec AAH */ 'AAP', 'CAT' , 'RAC',
/* entrepreneur, travailleur ind�pendant */'ETI', 'CJT', 'EXP', 'MAR', 'GSA' , 'EXS')  then do; MACT12C=MACT12C+1; end;end;
MACT12=MACT12R+MACT12C;

/*******************************************************************************/
/*III CREATION DE VARIABLES EXPLICATIVES SUR BASE ENRICHIE SID ENTREPOTS CAF   */
/*******************************************************************************/
array INDTOT SINDTOT0610 SINDTOT0510 SINDTOT0410 SINDTOT0310 SINDTOT0210 SINDTOT0110 SINDTOT1209 SINDTOT1109 SINDTOT1009 SINDTOT0909 SINDTOT0809 SINDTOT0709 SINDTOT0609 SINDTOT0509 SINDTOT0409 SINDTOT0309 SINDTOT0209 SINDTOT0109 SINDTOT1208 SINDTOT1108 SINDTOT1008 SINDTOT0908 SINDTOT0808 SINDTOT0708;
array INDFNPF SINDFNPF0610 SINDFNPF0510 SINDFNPF0410 SINDFNPF0310 SINDFNPF0210 SINDFNPF0110 SINDFNPF1209 SINDFNPF1109 SINDFNPF1009 SINDFNPF0909 SINDFNPF0809 SINDFNPF0709 SINDFNPF0609 SINDFNPF0509 SINDFNPF0409 SINDFNPF0309 SINDFNPF0209 SINDFNPF0109 SINDFNPF1208 SINDFNPF1108 SINDFNPF1008 SINDFNPF0908 SINDFNPF0808 SINDFNPF0708;
array INDFNAL SINDFNAL0610 SINDFNAL0510 SINDFNAL0410 SINDFNAL0310 SINDFNAL0210 SINDFNAL0110 SINDFNAL1209 SINDFNAL1109 SINDFNAL1009 SINDFNAL0909 SINDFNAL0809 SINDFNAL0709 SINDFNAL0609 SINDFNAL0509 SINDFNAL0409 SINDFNAL0309 SINDFNAL0209 SINDFNAL0109 SINDFNAL1208 SINDFNAL1108 SINDFNAL1008 SINDFNAL0908 SINDFNAL0808 SINDFNAL0708;
array INDFNH SINDFNH0610 SINDFNH0510 SINDFNH0410 SINDFNH0310 SINDFNH0210 SINDFNH0110 SINDFNH1209 SINDFNH1109 SINDFNH1009 SINDFNH0909 SINDFNH0809 SINDFNH0709 SINDFNH0609 SINDFNH0509 SINDFNH0409 SINDFNH0309 SINDFNH0209 SINDFNH0109 SINDFNH1208 SINDFNH1108 SINDFNH1008 SINDFNH0908 SINDFNH0808 SINDFNH0708;
array FAITOT FGTOT0610 FGTOT0510 FGTOT0410 FGTOT0310 FGTOT0210 FGTOT0110 FGTOT1209 FGTOT1109 FGTOT1009 FGTOT0909 FGTOT0809 FGTOT0709 FGTOT0609 FGTOT0509 FGTOT0409 FGTOT0309 FGTOT0209 FGTOT0109 FGTOT1208 FGTOT1108 FGTOT1008 FGTOT0908 FGTOT0808 FGTOT0708;
array FAIFAM SITFAM0610 SITFAM0510 SITFAM0410 SITFAM0310 SITFAM0210 SITFAM0110 SITFAM1209 SITFAM1109 SITFAM1009 SITFAM0909 SITFAM0809 SITFAM0709 SITFAM0609 SITFAM0509 SITFAM0409 SITFAM0309 SITFAM0209 SITFAM0109 SITFAM1208 SITFAM1108 SITFAM1008 SITFAM0908 SITFAM0808 SITFAM0708;
array FAIENFAUT SITENFAUT0610 SITENFAUT0510 SITENFAUT0410 SITENFAUT0310 SITENFAUT0210 SITENFAUT0110 SITENFAUT1209 SITENFAUT1109 SITENFAUT1009 SITENFAUT0909 SITENFAUT0809 SITENFAUT0709 SITENFAUT0609 SITENFAUT0509 SITENFAUT0409 SITENFAUT0309 SITENFAUT0209 SITENFAUT0109 SITENFAUT1208 SITENFAUT1108 SITENFAUT1008 SITENFAUT0908 SITENFAUT0808 SITENFAUT0708;
array FAISANN RESANN0610 RESANN0510 RESANN0410 RESANN0310 RESANN0210 RESANN0110 RESANN1209 RESANN1109 RESANN1009 RESANN0909 RESANN0809 RESANN0709 RESANN0609 RESANN0509 RESANN0409 RESANN0309 RESANN0209 RESANN0109 RESANN1208 RESANN1108 RESANN1008 RESANN0908 RESANN0808 RESANN0708;
array FAIRESTR RESTR0610 RESTR0510 RESTR0410 RESTR0310 RESTR0210 RESTR0110 RESTR1209 RESTR1109 RESTR1009 RESTR0909 RESTR0809 RESTR0709 RESTR0609 RESTR0509 RESTR0409 RESTR0309 RESTR0209 RESTR0109 RESTR1208 RESTR1108 RESTR1008 RESTR0908 RESTR0808 RESTR0708;
array FAIPRO SITPRO0610 SITPRO0510 SITPRO0410 SITPRO0310 SITPRO0210 SITPRO0110 SITPRO1209 SITPRO1109 SITPRO1009 SITPRO0909 SITPRO0809 SITPRO0709 SITPRO0609 SITPRO0509 SITPRO0409 SITPRO0309 SITPRO0209 SITPRO0109 SITPRO1208 SITPRO1108 SITPRO1008 SITPRO0908 SITPRO0808 SITPRO0708;
array ADR ADRESS0610 ADRESS0510 ADRESS0410 ADRESS0310 ADRESS0210 ADRESS0110 ADRESS1209 ADRESS1109 ADRESS1009 ADRESS0909 ADRESS0809 ADRESS0709 ADRESS0609 ADRESS0509 ADRESS0409 ADRESS0309 ADRESS0209 ADRESS0109 ADRESS1208 ADRESS1108 ADRESS1008 ADRESS0908 ADRESS0808 ADRESS0708;
array CONVU VU0610 VU0510 VU0410 VU0310 VU0210 VU0110 VU1209 VU1109 VU1009 VU0909 VU0809 VU0709 VU0609 VU0509 VU0409 VU0309 VU0209 VU0109 VU1208 VU1108 VU1008 VU0908 VU0808 VU0708;
array CONTEL TEL0610 TEL0510 TEL0410 TEL0310 TEL0210 TEL0110 TEL1209 TEL1109 TEL1009 TEL0909 TEL0809 TEL0709 TEL0609 TEL0509 TEL0409 TEL0309 TEL0209 TEL0109 TEL1208 TEL1108 TEL1008 TEL0908 TEL0808 TEL0708;
array CONAUT AUT0610 AUT0510 AUT0410 AUT0310 AUT0210 AUT0110 AUT1209 AUT1109 AUT1009 AUT0909 AUT0809 AUT0709 AUT0609 AUT0509 AUT0409 AUT0309 AUT0209 AUT0109 AUT1208 AUT1108 AUT1008 AUT0908 AUT0808 AUT0708;


FGFAM12=0; do i=1 to 12; if (FAIFAM(i) ne .  and FAIFAM(i)>0) then do; FGFAM12=FGFAM12+FAIFAM(i); end;end;
FGRESTRI12=0; do i=1 to 12; if (FAIRESTR(i) ne .  and FAIRESTR(i)>0) then do; FGRESTRI12=FGRESTRI12+FAIRESTR(i); end;end;
FGSITPRO12=0; do i=1 to 12; if (FAIPRO(i) ne .  and FAIPRO(i)>0) then do; FGSITPRO12=FGSITPRO12+FAIPRO(i); end;end;
NBONTAVU12=0; do i=1 to 12; if (CONVU(i) ne .  and CONVU(i)>0) then do; NBONTAVU12=NBONTAVU12+CONVU(i); end;end;
NBONTATEL12=0; do i=1 to 12; if (CONTEL(i) ne .  and CONTEL(i)>0) then do; NBONTATEL12=NBONTATEL12+CONTEL(i); end;end;
NBONTAAUT12=0; do i=1 to 12; if (CONAUT(i) ne .  and CONAUT(i)>0) then do; NBONTAAUT12=NBONTAAUT12+CONAUT(i); end;end;
NBONTA12=NBONTAVU12+NBONTATEL12+NBONTAAUT12;

label FGFAM12="NB FG SITFAM CONFIRMES VALIDES SUR 12 MOIS";
label FGRESTRI12="NB FG RESTRI CONFIRMES VALIDES SUR 12 MOIS";
label FGSITPRO12="NB FG SITPRO CONFIRMES VALIDES SUR 12 MOIS";
label NBONTAVU12="NOMBRE DE CONTACT ACCUEIL EN 12 MOIS";
label NBONTATEL12="NOMBRE DE CONTACT TEL EN 12 MOIS";
label NBONTAAUT12="NOMBRE DE CONTACT AUT EN 12 MOIS";
label NBONTA12="NOMBRE DE CONTACT TOTAL EN 12 MOIS";

INDTOT24=0; do i=1 to 24; if (INDTOT(i) ne .  and INDTOT(i)>0) then do; INDTOT24=INDTOT24+INDTOT(i); end;end;
INDFNPF24=0; do i=1 to 24; if (INDFNPF(i) ne .  and INDFNPF(i)>0) then do; INDFNPF24=INDFNPF24+INDFNPF(i); end;end;
INDFNAL24=0; do i=1 to 24; if (INDFNAL(i) ne .  and INDFNAL(i)>0) then do; INDFNAL24=INDFNAL24+INDFNAL(i); end;end;
INDFNH24=0; do i=1 to 24; if (INDFNH(i) ne .  and INDFNH(i)>0) then do; INDFNH24=INDFNH24+INDFNH(i); end;end;
INDAL24=INDFNH24+INDFNAL24;
MOIINDTOT=0; do i=1 to 24; if (INDTOT(i) ne .  and INDTOT(i)>0) then do; MOIINDTOT=i; i=24; end;end;
MOIINDFNPF=0; do i=1 to 24; if (INDFNPF(i) ne .  and INDFNPF(i)>0) then do; MOIINDFNPF=i; i=24; end;end;
MOIINDFNAL=0; do i=1 to 24; if (INDFNAL(i) ne .  and INDFNAL(i)>0) then do; MOIINDFNAL=i; i=24; end;end;
MOIINDFNH=0; do i=1 to 24; if (INDFNH(i) ne .  and INDFNH(i)>0) then do; MOIINDFNH=i; i=24; end;end;
MOIINDAL=0; do i=1 to 24; if ( (INDFNAL(i) ne .  and INDFNAL(i)>0) or (INDFNH(i) ne .  and INDFNH(i)>0) ) then do; MOIINDAL=i; i=24; end;end;
FGTOT24=0; do i=1 to 24; if (FAITOT(i) ne .  and FAITOT(i)>0) then do; FGTOT24=FGTOT24+FAITOT(i); end;end;
FGFAM24=0; do i=1 to 24; if (FAIFAM(i) ne .  and FAIFAM(i)>0) then do; FGFAM24=FGFAM24+FAIFAM(i); end;end;
FGENFAUT24=0; do i=1 to 24; if (FAIENFAUT(i) ne .  and FAIENFAUT(i)>0) then do; FGENFAUT24=FGENFAUT24+FAIENFAUT(i); end;end;
FGRESAN24=0; do i=1 to 24; if (FAISANN(i) ne .  and FAISANN(i)>0) then do; FGRESAN24=FGRESAN24+FAISANN(i); end;end;
FGRESTRI24=0; do i=1 to 24; if (FAIRESTR(i) ne .  and FAIRESTR(i)>0) then do; FGRESTRI24=FGRESTRI24+FAIRESTR(i); end;end;
FGSITPRO24=0; do i=1 to 24; if (FAIPRO(i) ne .  and FAIPRO(i)>0) then do; FGSITPRO24=FGSITPRO24+FAIPRO(i); end;end;
MOIFGTOT=0; do i=1 to 24; if (FAITOT(i) ne .  and FAITOT(i)>0) then do; MOIFGTOT=i; i=24; end;end;
MOIFGFAM=0; do i=1 to 24; if (FAIFAM(i) ne .  and FAIFAM(i)>0) then do; MOIFGFAM=i; i=24; end;end;
MOIFGENFAUT=0; do i=1 to 24; if (FAIENFAUT(i) ne .  and FAIENFAUT(i)>0) then do; MOIFGENFAUT=i; i=24; end;end;
MOIFGRESAN=0; do i=1 to 24; if (FAISANN(i) ne .  and FAISANN(i)>0) then do; MOIFGRESAN=i; i=24; end;end;
MOIFGRESTRI=0; do i=1 to 24; if (FAIRESTR(i) ne .  and FAIRESTR(i)>0) then do; MOIFGRESTRI=i; i=24; end;end;
MOIFGSITPRO=0; do i=1 to 24; if (FAIPRO(i) ne .  and FAIPRO(i)>0) then do; MOIFGSITPRO=i; i=24; end;end;
NADRESS24=0; ADRESS_dernier=adress&mvMOISFRM.&mvANNEEFRM; do i=1 to 24; if (ADR(i) ne " " and ADR(i) ne ADRESS_dernier)  then do; NADRESS24=NADRESS24+1; ADRESDEP=ADR(i);  end;end;
MOIADRESS=0; ADRESS_dernier=adress&mvMOISFRM.&mvANNEEFRM; do i=1 to 24; if  (ADR(i) ne " " and ADR(i) ne ADRESS_dernier) then do; MOIADRESS=i; i=24; end;end;
NBONTAVU24=0; do i=1 to 24; if (CONVU(i) ne .  and CONVU(i)>0) then do; NBONTAVU24=NBONTAVU24+CONVU(i); end;end;
NBONTATEL24=0; do i=1 to 24; if (CONTEL(i) ne .  and CONTEL(i)>0) then do; NBONTATEL24=NBONTATEL24+CONTEL(i); end;end;
NBONTAAUT24=0; do i=1 to 24; if (CONAUT(i) ne .  and CONAUT(i)>0) then do; NBONTAAUT24=NBONTAAUT24+CONAUT(i); end;end;
NBONTA24=NBONTAVU24+NBONTATEL24+NBONTAAUT24;
MOIONTAVU=0; do i=1 to 24; if (CONVU(i) ne .  and CONVU(i)>0) then do; MOIONTAVU=i; i=24; end;end;
MOIONTATEL=0; do i=1 to 24; if (CONTEL(i) ne .  and CONTEL(i)>0) then do; MOIONTATEL=i; i=24; end;end;
MOIONTAAUT=0; do i=1 to 24; if (CONAUT(i) ne .  and CONAUT(i)>0) then do; MOIONTAAUT=i; i=24; end;end;
MOIONTA=0; do i=1 to 24; if( (CONAUT(i) ne .  and CONAUT(i)>0) or (CONVU(i) ne .  and CONVU(i)>0) or (CONTEL(i) ne .  and CONTEL(i)>0) )then do; MOIONTA=i; i=24; end;end;
MOINSCDO=intck("MONTH",DTINSCDO_dernier,DTREFFRE); if MOINSCDO=. then MOINSCDO=0;

label INDTOT24="NB INDUS TOTAL SUR 24 MOIS";
label INDFNPF24="NB INDUS FNPF SUR 24 MOIS";
label INDFNAL24="NB INDUS FNAL SUR 24 MOIS";
label INDFNH24="NB INDUS FNH SUR 24 MOIS";
label INDAL24="NB INDUS FNAL OU FNH SUR 24 MOIS";
label MOIINDTOT="NB MOIS DEPUIS DERNIER INDU";
label MOIINDFNPF="NB MOIS DEPUIS DERNIER INDU FNPF";
label MOIINDFNAL="NB MOIS DEPUIS DERNIER INDU AL";
label MOIINDFNH="NB MOIS DEPUIS DERNIER INDU FNH";
label MOIINDAL="NB MOIS DEPUIS DERNIER INDU FNH OU FNAL";
label typebai_f="TYPE DE BAILLEUR";
label MODPAIDO_f="MODE DE PAIEMENT DOSSIER";
label FGTOT24="NB FG TOTAL CONFIRMES VALIDES SUR 24 MOIS";
label FGFAM24="NB FG SITFAM CONFIRMES VALIDES SUR 24 MOIS";
label FGENFAUT24="NB FG SITENFAUT CONFIRMES VALIDES SUR 24 MOIS";
label FGRESAN24="NB FG RESANN CONFIRMES VALIDES SUR 24 MOIS";
label FGRESTRI24="NB FG RESTRI CONFIRMES VALIDES SUR 24 MOIS";
label FGSITPRO24="NB FG SITPRO CONFIRMES VALIDES SUR 24 MOIS";
label MOIFGTOT="NB MOIS DEPUIS LE DERNIER FG CONFIRMES VALIDES";
label MOIFGFAM="NB MOIS DEPUIS LE DERNIER FG SITFAM CONFIRMES VALIDES";
label MOIFGENFAUT="NB MOIS DEPUIS LE DERNIER FG SITENFAUT CONFIRMES VALIDES";
label MOIFGRESAN="NB MOIS DEPUIS LE DERNIER FG RESANN CONFIRMES VALIDES";
label MOIFGRESTRI="NB MOIS DEPUIS LE DERNIER FG RESTRI CONFIRMES VALIDES";
label MOIFGSITPRO="NB MOIS DEPUIS LE DERNIER FG SITPRO CONFIRMES VALIDES";
label DTSITFALL="DATE DERNIER FGSITFAM ALL (CONFIRM� ET VALID�)";
label DTRESALL="DATE DERNIER FGRESANN ALL (CONFIRM� ET VALID�)";
label TOPFGRESALL="NB FG RESANN  D'ORIGINE ALLOCATAIRE DEPUIS 6 MOIS";
label NBFGRESSAP="NB DE FG RESECHCAF SUITE � APPEL DE RESSOURCES COMPL�MENTAIRES DEPUIS 6 MOIS ";
label MODPAIDO_dernier="MODE DE PAIEMENT dernier mois";
label NADRESS24="NB CHGT ADRESSE 24 MOIS";
label MOIADRESS="NB MOIS DEPUIS CHGT ADRESSE";
label NBONTAVU24="NOMBRE DE CONTACT ACCUEIL EN 24 MOIS";
label NBONTATEL24="NOMBRE DE CONTACT TEL EN 24 MOIS";
label NBONTAAUT24="NOMBRE DE CONTACT AUT EN 24 MOIS";
label MOIONTAVU="NB MOIS DEPUIS CONTACT ACCUEIL";
label MOIONTATEL="NB MOIS DEPUIS CONTACT TEL";
label MOIONTAAUT="NB MOIS DEPUIS CONTACT AUT";
label NBONTA24="NOMBRE DE CONTACT EN 24 MOIS";
label MOIONTA="NB MOIS DEPUIS CONTACT TOUS TYPES";
label MOINSCDO="NB MOIS DEPUIS AFFILIATION PAR RAPPORT dernier mois";

/*******************************************************************************/
/*IV    VARIABLES HISTORIQUES RECODEES                                         */
/*******************************************************************************/

MOICONTCAF_f = put(MOICONTCAF,MOICON_f.);
MOIFGRESTRI_f = put(MOIFGRESTRI,MFGRTR_f.);
MOIINDFNPF_f = put(MOIINDFNPF,MINDFPFf.);
MOIFGSITPRO_f = put(MOIFGSITPRO,MFGPRO_f.);
MOIINDTOT_f = put(MOIINDTOT,MINDTOTf.);
MOIADRESS_f = put(MOIADRESS,MADR_f.);
MOIONTA_f = put(MOIONTA,MCONTA_f.);
MOIINDAL_f = put(MOIINDAL,MINDAL_f.);
MOIFGTOT_f = put(MOIFGTOT,MFGTOT_f.);
MOIMODNEU_f = put(MOIMODNEU,MMONEU_f.);
MOIONTAVU_f = put(MOIONTAVU,MOIVU_f.);
MOIONTATEL_f = put(MOIONTATEL,MOITEL_f.);
MOIONTAAUT_f = put(MOIONTAAUT,MOIAUT_f.);
MOIFGFAM_f = put(MOIFGFAM,MFGFAM_f.);
MOIFGENFAUT_f = put(MOIFGENFAUT,MFGENF_f.);
MOIFGRESAN_f = put(MOIFGRESAN,MFGRES_f.);

/*Nombre d'�vn�nements sur une p�riode*/
NMODACT12_f= put(NMODACT12,NMOAC12f.);
MACT12_f= put(MACT12,MACT12_f.);
INDTOT24_f= put(INDTOT24,INDTO24f.);
INDFNPF24_f= put(INDFNPF24,INFPF24f.);
INDAL24_f= put(INDAL24,INDAL24f.);
FGTOT24_f= put(FGTOT24,FGTOT24f.);
FGFAM24_f= put(FGFAM24,FGFAM24f.);
FGENFAUT24_f= put(FGENFAUT24,FGENF24f.);
FGRESAN24_f= put(FGRESAN24,FGRES24f.);
FGRESTRI24_f= put(FGRESTRI24,FGRTR24f.);
FGRESTRI24_f2= put(FGRESTRI24,FGRTR24B.);
FGSITPRO24_f= put(FGSITPRO24,FGPRO24f.);
NBONTA24_f= put(NBONTA24,NBCON24f.);
NBONTAVU24_f= put(NBONTAVU24,NBVU24f.);
NBONTATEL24_f= put(NBONTATEL24,NBTEL24f.);
NBONTAAUT24_f= put(NBONTAAUT24,NBAUT24f.);
NADRESS24_f=put(NADRESS24,NADR24_f.);
MOIMODACT_f=put(MOIMODACT,MMOACT_f.);

label MOICONTCAF_f= "nb mois depuis dernier contr�le CAF, donn�e entrantes ou v�rification (hors �changes automatiques)";
label TOPSIT12= "Au moins une modification de situation famille sur 12 mois";
label NMODACT12_f= "Nb de modification activit� sur 12 mois (resp+conj)";
label MOIMODNEU_f= "Nombre de mois depuis la derni�re modification ressources annuelles conj+resp (abat/neutra) -hors AAH";
label TOPAD12= "TOP modification adresse sur dossiers affili�s sur 12 mois";
label MACT12_f= "Nb mois en activit� sur 12 mois (RESP et CONJ)";
label INDTOT24_f= "NB INDUS TOTAL SUR 24 MOIS";
label INDFNPF24_f= "NB INDUS FNPF SUR 24 MOIS";
label INDAL24_f= "NB INDUS FNAL OU FNH SUR 24 MOIS";
label MOIINDTOT_f= "NB MOIS DEPUIS DERNIER INDU";
label MOIINDFNPF_f= "NB MOIS DEPUIS DERNIER INDU FNPF";
label MOIINDAL_f= "NB MOIS DEPUIS DERNIER INDU FNH ou FNAL";
label FGTOT24_f= "NB FG TOTAL CONFIRMES VALIDES SUR 24 MOIS";
label FGFAM24_f= "NB FG SITFAM CONFIRMES VALIDES SUR 24 MOIS";
label FGENFAUT24_f= "NB FG SITENFAUT CONFIRMES VALIDES SUR 24 MOIS";
label FGRESAN24_f= "NB FG RESANN CONFIRMES VALIDES SUR 24 MOIS";
label FGRESTRI24_f= "NB FG RESTRI CONFIRMES VALIDES SUR 24 MOIS";
label FGSITPRO24_f= "NB FG SITPRO CONFIRMES VALIDES SUR 24 MOIS";
label MOIFGTOT_f= "NB MOIS DEPUIS LE DERNIER FG CONFIRMES VALIDES";
label MOIFGFAM_f= "NB MOIS DEPUIS LE DERNIER FG SITFAM CONFIRMES VALIDES";
label MOIFGENFAUT_f= "NB MOIS DEPUIS LE DERNIER FG SITENFAUT CONFIRMES VALIDES";
label MOIFGRESAN_f= "NB MOIS DEPUIS LE DERNIER FG RESANN CONFIRMES VALIDES";
label MOIFGRESTRI_f= "NB MOIS DEPUIS LE DERNIER FG RESTRI CONFIRMES VALIDES";
label MOIFGSITPRO_f= "NB MOIS DEPUIS LE DERNIER FG SITPRO CONFIRMES VALIDES";
label MOINSCDO_f2= "NB MOIS DEPUIS AFFILIATION PAR RAPPORT dernier mois";
label NADRESS24_f= "NB CHGT ADRESSE 24 MOIS";
label MOIADRESS_f= "NB MOIS DEPUIS CHGT ADRESSE";
label NBONTA24_f= "NB MOIS DEPUIS CONTACT TOUS TYPES";
label NBONTAVU24_f= "NOMBRE DE CONTACT ACCUEIL EN 24 MOIS";
label NBONTATEL24_f= "NOMBRE DE CONTACT TEL EN 24 MOIS";
label NBONTAAUT24_f= "NOMBRE DE CONTACT AUT EN 24 MOIS";
label MOIONTA_f= "NOMBRE DE CONTACT EN 24 MOIS";
label MOIONTAVU_f= "NB MOIS DEPUIS CONTACT ACCUEIL";
label MOIONTATEL_f= "NB MOIS DEPUIS CONTACT TEL";
label MOIONTAAUT_f= "NB MOIS DEPUIS CONTACT AUT";

/*******************************************************************************/
/*IV-ter    Cr�ation indicateurs d'intensit� de FG sur variables historiques      */
/*******************************************************************************/
TELOUVU=abs(NBONTATEL24-NBONTAVU24);
TELOUVU_f = put(TELOUVU,telouvu.);

if NBM_ODMS>0 then do; 
	if NBM_ODMS>=24 then FGTRIMOI = FGRESTRI24/24; else FGTRIMOI = FGRESTRI24/NBM_ODMS;
end;
else FGTRIMOI=0;

if MOINSCDO>0 then do;
	if MOINSCDO>=24 then do;
		INDTOTMOI=INDTOT24/24;
		FGTOTMOI=FGTOT24/24;
		FGFAMMOI=FGFAM24/24;
		FGENFAUTMOI=FGENFAUT24/24;
		FGRESANMOI=FGRESAN24/24;
		FGSITPROMOI=FGSITPRO24/24;
		NBONTAMOI= NBONTA24/24;
		NBADRESSMOI=NADRESS24/24;
	end; else do;
		INDTOTMOI=INDTOT24/MOINSCDO;
		FGTOTMOI=FGTOT24/MOINSCDO;
		FGFAMMOI=FGFAM24/MOINSCDO;
		FGENFAUTMOI=FGENFAUT24/MOINSCDO;
		FGRESANMOI=FGRESAN24/MOINSCDO;
		FGSITPROMOI=FGSITPRO24/MOINSCDO;
		NBONTAMOI= NBONTA24/MOINSCDO;
		NADRESSMOI=NADRESS24/MOINSCDO;
	end;
	if MOINSCDO>=12 then do;
		MACTMOI=MACT12/12;
		MODNEUMOI=MODNEU12/12;
	end; else do;
		MACTMOI=MACT12/MOINSCDO;
		MODNEUMOI=MODNEU12/MOINSCDO;
	end;
end;
else if MOINSCDO=0 then do;
	INDTOTMOI=0;
	FGTOTMOI=0;
	FGFAMMOI=0;
	FGENFAUTMOI=0;
	FGRESANMOI=0;
	FGSITPROMOI=0;
	NBONTAMOI=0;
	NBADRESSMOI=0;
	MACTMOI=0;
	MODNEUMOI=0;
end;

if INDTOTMOI=. then INDTOTMOI=0;
if FGTOTMOI=. then FGTOTMOI=0;
if FGFAMMOI=. then FGFAMMOI=0;
if FGENFAUTMOI=. then FGENFAUTMOI=0;
if FGRESANMOI=. then FGRESANMOI=0;
if FGSITPROMOI=. then FGSITPROMOI=0;
if NBONTAMOI=. then NBONTAMOI=0;
if NBADRESSMOI=. then NBADRESSMOI=0;
if MACTMOI=. then MACTMOI=0;
if MODNEUMOI=. then MODNEUMOI=0;

/*******************************************************************************/
/*V    Nouvelles variables cr��es int�gr�es au 07/07/2010			           */
/*******************************************************************************/

cmg_f2 = cmg ;
if cmg_f2 in ("Assmat","Adom","Autaf") then cmg_f2 = "Oui" ;

MOINSCDO_f2= put(MOINSCDO,MOINSC.);
nbenf02_f=put(nbenf02,nbenf.);
nbenf35_f=put(nbenf35,nbenf.);
nbenf611_f=put(nbenf611,nbenf.);
nbenf1218_f=put(nbenf1218,nbenf.);
nbenf18p_f=put(nbenf18p,nbenf.);
perscouv_f2=put(perscouv,couv.);
contot12_f=put(contot12,disc2f.);
fgfam12_f=put(fgfam12,disc2f.);
fgrestri12_f=put(fgrestri12,disc4f.);
fgsitpro12_f=put(fgsitpro12,disc5f.);
nbonta12_f=put(nbonta12,onta.);
nbontatel12_f=put(nbontatel12,ontatel.);
nbontavu12_f=put(nbontavu12,ontavu.);
mtloyrem_f2=put(mtloyrem,mtloy2f.);

difage_B=put(difage,difage_B.);
PPRPPU_B=put(PPRPPU,PPRPPU_B.);

/*******************************************************************************/
/*VI    VARIABLES RETENUES DANS LES MODELES 	       		       				*/
/*******************************************************************************/

/*MTPREST*/ 
LOGCAL_P= put(MTLOGCAL,LOGCAL_P.);
MTPREST_P= put(MTPREST,PREST_P.);

/*Activit�: synth�se des activit� du responsable et �ventuellement conjoint*/
if ACTRESPD_f in ("ETI" "INACTIF_INCONNU" "CHOMAGE" "ETUDIANT") and ACTCONJ_f in ("ETI" "INACTIF_INCONNU" "CHOMAGE" "ETUDIANT") then ACTRESCON_P="2 DIFFICULTE";
else if (ACTRESPD_f ="ACT NORMALE+ESAT" and  ACTCONJ_f="ACT NORMALE+ESAT") then ACTRESCON_P="2 ACTIVITES (NORMALE+ESAT)";
else ACTRESCON_P="AUTRES CAS";


/*SITFAM_P : Recodage classique (ajout 14/06): iso, coups, coupa, monop*/
attrib SITFAM_P label="Sitfam recod� v3"	format=$6. informat=$6.;
if presconj="0" and nbenlefa=0 then SITFAM_P="iso  ";
if presconj="0" and nbenlefa>0 then SITFAM_P="monop";
if presconj="2" and nbenlefa=0 then SITFAM_P="coups";
if presconj="2" and nbenlefa>0 then SITFAM_P="coupa";

difage_P=put(difage,difage_P.);

nirnoncer_P=put(nirnoncer_dernier,nirnon.);
/* INCHANGEES */

nbenfcha_P=put(nbenfcha,nben.);
perscouv_P=put(perscouv,couv.); 
resaltdo_P=put(resaltdo,$resalt.); 

if nbenf02 > 0 then TOPENF02_P='1'; else TOPENF02_P='0'; 
if nbenf35 > 0 then TOPENF35_P='1'; else TOPENF35_P='0'; 
if nbenf611 > 0 then TOPENF611_P='1'; else TOPENF611_P='0'; 
if nbenf1218 > 0 then TOPENF1218_P='1'; else TOPENF1218_P='0'; 
if nbenf18p > 0 then TOPENF18_P='1'; else TOPENF18_P='0'; 
 
array TABENFP[12] qualen1 qualen2 qualen3 qualen4 qualen5 qualen6 qualen7 qualen8 qualen9 qualen10 qualen11 qualen12 ;
top16act_P=0; top16ssa_P=0;
do i=1 to min(nbenlefa,12) ;
		if (tabenfP[i] in ("3" "5" "6" "A")) then top16act_P=1;
		if (tabenfP[i] in ("4" "7" "8" "9")) then top16ssa_P=1;
end;

FGTOTMOI_P = put(FGTOTMOI,FGTOTM_P.); 
MOIFGTOT_P = put(MOIFGTOT,MFGTOT_P.);
MOINSCDO_P = put(MOINSCDO,MINSDO_P.);
NBONTAMOI_P = put(NBONTAMOI,NONTAM_P.);
TELOUVU_P = put(TELOUVU,TELVU_P.);
MOIONTA_P = put(MOIONTA,MONTA_P.);
MODPAIDO_P=put(MODPAIDO_dernier,$MOPAI_P.);

/*TYPE calcul assiette logement*/
if TYPCALF ne '0' then typcalal=typcalf; else
if TYPCAPL ne '0' then typcalal=typcapl; else
if TYPCALS ne '0' then typcalal=typcals; else TYPCALAL='0';

TYPCALAL_P=put(TYPCALAL,$TYPCA_P.);

INDTOT24_P= put(INDTOT24,INTO24_P.);
MOIINDTOT_P = put(MOIINDTOT,MINDT_P.);
MOIINDFNPF_P = put(MOIINDFNPF,MINFNP_P.); 
MOIINDAL_P = put(MOIINDAL,MINAL_P.);
MOICONTCAF_P = put(MOICONTCAF,MCONT_P.);

NMODACT12_P= put(NMODACT12,NACT12_P.);	 
MOIFGSITPRO_P = put(MOIFGSITPRO,MFGPRO_P.); 
MOIMODACT_P=put(MOIMODACT,MMODAC_P.);	
if (apivers in ('1' '2') or rmivers in ('1' '2') or rsovers>'0' or CAVVERS>'0' or PREMVAUT>'0' or PREMVMME>'0' or PREMVMON>'0' or NPRLTRIP>0 or RMAVERS>'0' or RSAVERS>'0') then MS="MS (+ARE)"; else MS="0";
MOIFGRESTRI_P = put(MOIFGRESTRI,MFGRTR_P.); 
nbm_odms_P=put(nbm_odms,NBODMS_P.);

MOIFGFAM_P = put(MOIFGFAM,MFGFAM_P.);
FGFAMMOI_P = put(FGFAMMOI,FGFAMM_P.);
TOPSIT12_P=NMODSIT12>0;

MOIFGRESAN_P = put(MOIFGRESAN,MFGRES_P.);
IF NBFGRESALL>0 THEN topfgresall_P=1; else topfgresall_P=0;
MOIMODNEU_P = put(MOIMODNEU,MMABN_P.);
if ( ( abaneure in (1 2)) or (abaneuco in  (1 2)) ) then ABANEU="Ab. ou neutral."; else ABANEU="Aucun";
/*TDISASFA non recod�e FRE DIRECT*/ 
ASFVERS_P=put(ASFVERS,$ASF_P.);

MOIADRESS_P = put(MOIADRESS,MADRES_P.);
PPRPPU_P=put(PPRPPU,PPRPPU_P.);
if TOPRETUT>'0' then tut_P="TUTELLE"; else tut_P="0";
if aahvers>'0' or complaah>'0' then AAH_COMPAAH_P="AAH"; else AAH_COMPAAH_P="0"; 
/*ZONEGEO non recod�e*/

label MTPREST="MONTANT DE PRESTATION PERCUE";
label LOGCAL_P="MONTANT D AL";
label MTPREST_P="MONTANT DE PRESTATION PERCUE";
label ACTRESCON_P="DOUBLE ACTIVITE, DOUBLE INACTIVITE";
label SITFAM_P="SITUATION FAMILIALE";
label difage_P="DIFFERENCE D AGE";
label nirnoncer_P="ETAT DE CERTIFICATION DES NIR";
label nbenfcha_P="NOMBRE D ENFANT A CHARGE DES PF";
label perscouv_P="NOMBRE PERSONNES COUVERTES";
label resaltdo_P="RESIDENCE ALTERNEE";
label TOPENF02_P="TOP PRESENCE ENFANT DE 0 A 2 ANS";
label TOPENF35_P="TOP PRESENCE ENFANT DE 3 A 5 ANS";
label TOPENF611_P="TOP PRESENCE ENFANT DE 6 A 11 ANS";
label TOPENF1218_P="TOP PRESENCE ENFANT DE 12 A 18 ANS";
label nbenf18p="TOP PRESENCE ENFANT DE PLUS DE 18 ANS";
label top16act_P="TOP PRESENCE ENFANT DE PLUS DE 16 ANS EN ACTIVITE";
label top16ssa_P="TOP PRESENCE ENFANT DE PLUS DE 16 ANS SANS ACTIVITE";
label FGTOTMOI_P="NB MENSUEL DE FG DEPUIS L INSCRIPTION OU 24 MOIS";
label MOIFGTOT_P="NB MOIS DEPUIS LE DERNIER FG PASSE SUR LE DOSSIER";
label MOINSCDO_P="NB MOIS DEPUIS L INSCRIPTION DU DOSSIER";
label NBONTAMOI_P="NB DE MOI DEPUIS LA DERNIERE VISITE EN CAF";
label TELOUVU_P="ECART ENTRE LE NB D APPELS TEL ET LE NOMBRE DE VISITES DEPUIS 24 MOIS";
label MOIONTA_P="NB MOI DEPUIS LE DERNIER CONTACT (HORS COURRIER)";
label MODPAIDO_P="MODE DE PAIEMENT";
label TYPCALAL_P="TYPE DE CALCUL DE L ASSIETTE RESSOURCES AL";
label INDTOT24_P="NB INDUS TOTAL DEPUIS 24 MOIS";
label MOIINDTOT_P="NB MOIS DEPUIS LE DERNIER INDU";
label MOIINDFNPF_P="NB MOIS DEPUIS LE DERNIER INDU FNPF";
label MOIINDAL_P="NB MOIS DEPUIS LE DERNIER INDU AL";
label MOICONTCAF_P="NB MOIS DEPUIS LE DERNIER CONTROLE CAF";
label NMODACT12_P="NB MODIFICATIONS DU CODE ACTIVITE DEPUIS 12 MOIS";
label MOIFGSITPRO_P="NB MOIS DEPUIS LE PASSAGE DU DERNIER FG SITPRO";
label MOIMODACT_P="NB MOIS DEPUIS LA DERNIERE MODIFICATION DU CODE ACTIVITE";
label MS="TOP PRECEPTION D UN MINIMUM SOCIAL";
label MOIFGRESTRI_P="NB MOIS DEPUIS LE DERNIER FG RESSOURCES TRIMESTRIELLES";
label nbm_odms_P="NB MOIS DEPUIS L OD AU MINIMUM SOCIAL";
label MOIFGFAM_P="NB MOIS DEPUIS LE DERNIER FG SITFAM";
label FGFAMMOI_P ="NB MENSUEL DE FG SITFAM DEPUIS L INSCRIPTION OU 24 MOIS";
label TOPSIT12_P="TOP CHANGERMENT DE SITUATION FAMILIALE DEPUIS 12 MOIS";
label MOIFGRESAN_P="NB MOIS DEPUIS LE DERNIER FG RESAN DE TYPE ALLOCATAIRE";
label topfgresall_P="TOP FG RESALL DEPUIS 24 MOIS";
label MOIMODNEU_P="NB MOIS DEPUIS LA DERNIERE MODIFICATION DU CODE ABATTEMENT OU NEUTRAL";
label ABANEU="SITUATION D ABATTEMENT OU DE NEUTRALISATION DES RESSOURCES";
label TDISASFA="TOP PENSION ALIMENTAIRE PERCUE";
label ASFVERS_P="TYPE D ASF";
label MOIADRESS_P="NB DE MOIS DEPUIS LA DERNIERE MODIFICATION D ADRESSE";
label PPRPPU_P="TYPE DE PARC DU LOGEMENT";
label tut_P="TOP TUTELLE";
label AAH_COMPAAH_P="TOP AAH OU COMPLEMENT";
label ZONEGEO="ZONE GEOGRAPHIQUE";



/************************************************************************************************/
/************************************************************************************************/
/************************************************************************************************/
/**************				PARTIE IV- Redressement donn�es manquantes				*************/
/************************************************************************************************/
/************************************************************************************************/
/************************************************************************************************/


TOP_1conj0ms0al =((PRESCONJ_f="AV_CONJOINT") and (MS="0") and (AL="0") );
TOP_1conj0ms1al=( (PRESCONJ_f="AV_CONJOINT") and (MS="0") and (AL="AL") );
TOP_1conj1ms0al =( (PRESCONJ_f="AV_CONJOINT") and (MS="MS (+ARE)") and (AL="0") );
TOP_1conj1ms1al =( (PRESCONJ_f="AV_CONJOINT") and (MS="MS (+ARE)") and (AL="AL") );
TOP_0conj0ms0al =((PRESCONJ_f="SANS_CONJOINT") and (MS="0") and (AL="0"));
TOP_0conj0ms1al =((PRESCONJ_f="SANS_CONJOINT") and (MS="0") and (AL="AL"));
TOP_0conj1ms0al=((PRESCONJ_f="SANS_CONJOINT") and (MS="MS (+ARE)") and (AL="0") );
TOP_0conj1ms1al=((PRESCONJ_f="SANS_CONJOINT") and (MS="MS (+ARE)") and (AL="AL") );
/*verif indicatrices*/
/*ver1=sum(TOP_1conj0ms0al ,TOP_1conj0ms1al,TOP_1conj1ms0al,TOP_1conj1ms1al ,TOP_0conj0ms0al ,TOP_0conj0ms1al ,TOP_0conj1ms0al,TOP_0conj1ms1al);*/
chomage=((ACTRESPD_f="CHOMAGE") or (ACTCONJ_f="CHOMAGE"));
toppaje=(PAJE_AFEAGED="PAJE");
topasf=(ASF="ASF");
topaah=(AAH_COMPAAH="AAH");
femme=(SEXE_f="FEMME");
activresp=(ACTRESPD_f="ACT_NORMALE+ETI+ESAT" );
activconj=(ACTCONJ_f="ACT_NORMALE+ETI+ESAT");
/*	Mod�le de redressement de la variable revmens	*/
/*	Voir classeur basetude09red.xls	*/
/*	sur 9290 observations : revmens connus et mtprest> */
/*	R�=0,622783 dl=17 */

if REVMENS=99999 then do;
	REVMENS_RED=max(0,189.972886  + activconj * (679.110808 ) + activresp * (449.804327 ) + ageresp * (9.074702 ) + chomage * (214.096473 ) + femme * (82.879216 ) +
	mtprest * (-0.50641 ) + NBENFCHA * (182.126509 ) + TOP_0conj0ms0al * (-127.991336 ) +
	TOP_0conj0ms1al * (-253.894658 ) + TOP_0conj1ms0al * (-577.076753 ) + TOP_0conj1ms1al * (-482.715358 ) + TOP_1conj0ms0al * (1317.410976 ) +
	TOP_1conj1ms0al * (-684.857514 ) + TOP_1conj1ms1al * (-536.331104 ) + topaah * (-183.599325 ) + topasf * (-124.570829 ) + toppaje * (266.586659 ));
end;
else REVMENS_RED=REVMENS;

/*Revenus par unit� de consommation*/
/*CALCUL DU RUC MENSUEL*/
/*RUC MENSUEL RED*/
RUC_RED=REVMENS_RED/NBUC;
label RUC_RED="RUC Mensuel hors PF";

/*Mod�le de redressement de la variable mtrevbru*/
/*sur 9166 observations : mtrevbru connus */
/*R�=0,595 dl=17 */

if MTREVBRU>=9999998 then do;
	MTREVBRU_RED=max(0,1380.700333  + NBENFCHA * (1836.558261 ) + ageresp * (109.650043 ) + mtprest * (-4.632265 ) + 
	TOP_1conj0ms0al * (16291.540008 ) + TOP_1conj1ms0al * (-6522.248891 ) + TOP_1conj1ms1al * (-4408.085236 ) + 
	TOP_0conj0ms1al * (-2569.735729 ) + TOP_0conj1ms0al * (-5630.168175 ) + TOP_0conj1ms1al * (-4575.663174 ) +
	chomage * (3652.423686 ) + toppaje * (3042.850353 ) + topasf * (-1836.292169 ) + topaah * (-3369.087472 ) + 
	femme * (773.741889 ) + activresp * (5482.94001 ) + activconj * (8673.047286 ));
end;
else MTREVBRU_RED=MTREVBRU;

REVMENS_RED_f= put(REVMENS_RED,RMENS_Rf.);
if (MS="MS (+ARE)") then restri_red=revmens_red; else restri_red=99999;
if (MS="MS (+ARE)") then restriuc_red=revmens_red/nbuc; else restriuc_red=99999;
RESTRI_RED_f= put(RESTRI_RED,RESTR_RF.);
RESTRIUC_RED_f= put(RESTRIUC_RED,RTRUC_Rf.);
MTREVBRU_RED_f= put(MTREVBRU_RED,RBRU_Rf.);
RUC_RED_f= put(RUC_RED,RUC_Rf.);


/*Taux d'effort logement*/
if mtlogcal>0 then do;

	charge=49.14+ 11.12*(nechalog+nautlogv);
	loyer=sum(charge,mtloyrem);
	if mtloyrem=9999 then txeff=-1;
	else if (revmens_red/*+mtprest-mtlogcal*/)=0 then txeff=-1;
	else txeff=((loyer-mtlogcal)/(revmens_red/*+mtprest-mtlogcal*/))*100;
	restaviv=max(0,revmens_red+mtprest-loyer);
end;

else do; txeff=-2; restaviv=-2; loyer=0; charge=0; end; 

/*RESTAVIV_F=put(RESTAVIV,RESTA_f.);*/

TXEFF_P=put(txeff,TXEFF_P.);
MTREVBRU_RED_P= put(MTREVBRU_RED,RANRED_P.);

label TXEFF_P="TAUX D EFFORT LOGEMENT APRES AIDE";
label TXEFF="TAUX EFFORT LOGEMENT APRES AIDE";
label MTREVBRU_RED="REVENUS ANNUELS BRUTS";
label MTREVBRU_RED_P="REVENUS ANNUELS BRUTS";

run;



/************************************************************************************************/
/************************************************************************************************/
/**************						PARTIE V- SCORING SEM							*************/
/************************************************************************************************/
/************************************************************************************************/

 /*--------------------------------------------------------------*/ 
 /*  ENTERPRISE MINER: BEGIN SCORE CODE                          */ 
 /*--------------------------------------------------------------*/ 
    
   /*proc freq data=datamining&mvMOISFRM.&mvANNEEFRM;
   table MS rsavers;
   run;*/


  %macro DMNORLEN; 16 %mend DMNORLEN;  
   
  %macro DMNORMCP(in,out);  
  &out=substr(left(&in),1,min(%dmnorlen,length(left(&in)))); 
  &out=upcase(&out);    
  %mend DMNORMCP;       
   
  %macro DMNORMIP(in);  
  &in=left(&in);        
  &in=substr(&in,1,min(%dmnorlen,length(&in))); 
  &in=upcase(&in);      
  %mend DMNORMIP;       



%macro SCOREGLOB(tablin=,tablout=,garde= );
  DATA &tablout (keep=  &garde ) ; SET &tablin ;

  length _WARN_ $4;                                                             
  label _WARN_ = 'Warnings' ;                                                   
                                                                                
  length I_CIBLE $ 9;                                                           
  label I_CIBLE = 'Into: CIBLE' ;                                               
  *** Target Values;                                                            
  array A5316[2] $ 9 _temporary_ ('INDU6MOIS'  'HORSCIBLE' );                   
  label U_CIBLE = 'Unnormalized Into: CIBLE' ;                                  
  length U_CIBLE $ 9;                                                           
  *** Unnormalized target values;                                               
  array A7202[2] $ 9 _temporary_ ('Indu6mois'  'Horscible' );                   
                                                                                
  *** Generate dummy variables for CIBLE ;                                      
  drop _Y ;                                                                     
  label F_CIBLE = 'From: CIBLE' ;                                               
  length F_CIBLE $ 9;                                                           
  %DMNORMCP( CIBLE , F_CIBLE )                                                  
  if missing( CIBLE ) then do;                                                  
     _Y = .;                                                                    
  end;                                                                          
  else do;                                                                      
     if F_CIBLE = 'HORSCIBLE'  then do;                                         
        _Y = 1;                                                                 
     end;                                                                       
     else if F_CIBLE = 'INDU6MOIS'  then do;                                    
        _Y = 0;                                                                 
     end;                                                                       
     else do;                                                                   
        _Y = .;                                                                 
     end;                                                                       
  end;                                                                          
                                                                                
  drop _DM_BAD;                                                                 
  _DM_BAD=0;                                                                    
                                                                       
                                                                                
  *** Generate dummy variables for TOPENF18p ;                                  
  drop _1_0 ;                                                                   
  if missing( TOPENF18p ) then do;                                              
     _1_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1; 
		t1=1; 
  end;                                                                          
  else do;                                                                      
     length _dm1 $ 1; drop _dm1 ;                                               
     %DMNORMCP( TOPENF18p , _dm1 )                                              
     if _dm1 = '0'  then do;                                                    
        _1_0 = 1;                                                               
     end;                                                                       
     else if _dm1 = '1'  then do;                                               
        _1_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _1_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;   
		t1=1;                                                          
     end;                                                                       
  end;                                                                          
                 
                                                                                
  *** Generate dummy variables for nirnoncer_P ;                                
  drop _4_0 ;                                                                   
  if missing( nirnoncer_P ) then do;                                            
     _4_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1; 
		t4=1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     %DMNORMCP( nirnoncer_P , _dm12 )                                           
     if _dm12 = '0 NIRNONCER'  then do;                                         
        _4_0 = 1;                                                               
     end;                                                                       
     else if _dm12 = '>=1 NIRNOCER'  then do;                                   
        _4_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _4_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;  
		t4=1;                                                                  
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TELOUVU_P ;                                  
  drop _5_0 _5_1 _5_2 ;                                                         
  if missing( TELOUVU_P ) then do;                                              
     _5_0 = .;                                                                  
     _5_1 = .;                                                                  
     _5_2 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;  
		t5=1;                                                              
  end;                                                                          
  else do;                                                                      
     length _dm8 $ 8; drop _dm8 ;                                               
     %DMNORMCP( TELOUVU_P , _dm8 )                                              
     if _dm8 = '0A2'  then do;                                                  
        _5_0 = 1;                                                               
        _5_1 = 0;                                                               
        _5_2 = 0;                                                               
     end;                                                                       
     else if _dm8 = '3A7'  then do;                                             
        _5_0 = 0;                                                               
        _5_1 = 0;                                                               
        _5_2 = 1;                                                               
     end;                                                                       
     else if _dm8 = '8A15'  then do;                                            
        _5_0 = -1;                                                              
        _5_1 = -1;                                                              
        _5_2 = -1;                                                              
     end;                                                                       
     else if _dm8 = '15ETPLUS'  then do;                                        
        _5_0 = 0;                                                               
        _5_1 = 1;                                                               
        _5_2 = 0;                                                               
     end;                                                                       
     else do;                                                                   
        _5_0 = .;                                                               
        _5_1 = .;                                                               
        _5_2 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;  
		t5=1;                                                                 
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MOIONTA_P ;                                  
  drop _6_0 ;                                                                   
  if missing( MOIONTA_P ) then do;                                              
     _6_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;  
		t6=1;                                                              
  end;                                                                          
  else do;                                                                      
     length _dm8 $ 8; drop _dm8 ;                                               
     %DMNORMCP( MOIONTA_P , _dm8 )                                              
     if _dm8 = '0A12'  then do;                                                 
        _6_0 = 1;                                                               
     end;                                                                       
     else if _dm8 = '13ETPLUS'  then do;                                        
        _6_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _6_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;    
		t6=1;                                                               
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MODPAIDO_P ;                                 
  drop _7_0 ;                                                                   
  if missing( MODPAIDO_P ) then do;                                             
     _7_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;   
		t7=1;                                                             
  end;                                                                          
  else do;                                                                      
     length _dm5 $ 5; drop _dm5 ;                                               
     %DMNORMCP( MODPAIDO_P , _dm5 )                                             
     if _dm5 = 'VIR'  then do;                                                  
        _7_0 = 1;                                                               
     end;                                                                       
     else if _dm5 = 'AUTRE'  then do;                                           
        _7_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _7_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;     
		t7=1;                                                               
     end;                                                                       
  end;                                                                          
                                                           
                                                                                
  *** Generate dummy variables for MOICONTCAF_P ;                               
  drop _9_0 ;                                                                   
  if missing( MOICONTCAF_P ) then do;                                           
     _9_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;  
		t9=1;                                                              
  end;                                                                          
  else do;                                                                      
     length _dm7 $ 7; drop _dm7 ;                                               
     %DMNORMCP( MOICONTCAF_P , _dm7 )                                           
     if _dm7 = '0A6'  then do;                                                  
        _9_0 = 1;                                                               
     end;                                                                       
     else if _dm7 = '7ETPLUS'  then do;                                         
        _9_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _9_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;  
		t9=1;                                                                 
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MOIMODNEU_P ;                                
  drop _10_0 ;                                                                  
  if missing( MOIMODNEU_P ) then do;                                            
     _10_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;
		t10=1;                                                                
  end;                                                                          
  else do;                                                                      
     length _dm7 $ 7; drop _dm7 ;                                               
     %DMNORMCP( MOIMODNEU_P , _dm7 )                                            
     if _dm7 = '0A3'  then do;                                                  
        _10_0 = 1;                                                              
     end;                                                                       
     else if _dm7 = '4ETPLUS'  then do;                                         
        _10_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _10_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;  
		t10=1;                                                               
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for tut_P ;                                      
  drop _11_0 ;                                                                  
  if missing( tut_P ) then do;                                                  
     _11_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1; 
		t11=1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm7 $ 7; drop _dm7 ;                                               
     %DMNORMCP( tut_P , _dm7 )                                                  
     if _dm7 = '0'  then do;                                                    
        _11_0 = 1;                                                              
     end;                                                                       
     else if _dm7 = 'TUTELLE'  then do;                                         
        _11_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _11_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;   
		t11=1;                                                               
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TXEFF_P ;                                    
  drop _12_0 ;                                                                  
  if missing( TXEFF_P ) then do;                                                
     _12_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1; 
		t12=1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm15 $ 15; drop _dm15 ;                                            
     %DMNORMCP( TXEFF_P , _dm15 )                                               
     if _dm15 = '0ALETTXEFF 0A35'  then do;                                     
        _12_0 = 1;                                                              
     end;                                                                       
     else if _dm15 = '35ETPLUS'  then do;                                       
        _12_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _12_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1; 
		t12=1;                                                                 
     end;                                                                       
  end;                                                                          
                                                                                
  *** If missing inputs, use averages;                                          
  if _DM_BAD > 0 then do;                                                       
     _P0 = 0.079438808;                                                         
     _P1 = 0.920561192;                                                         
     goto G1228;                                                                
  end;                                                                          
                                                                                
  *** Compute Linear Predictor;                                                 
  drop _TEMP;                                                                   
  drop _LP0;                                                                    
  _LP0 = 0;                                                                     
                                     
  *--- Effect: MODPAIDO_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.11999214699288) * _TEMP * _7_0;                          
  *--- Effect: MOICONTCAF_P ---;                                                
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.16762566710605) * _TEMP * _9_0;                          
  *--- Effect: MOIMODNEU_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.23103101272775) * _TEMP * _10_0;                         
  *--- Effect: MOIONTA_P ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.11196137596139) * _TEMP * _6_0;                          
  *--- Effect: nirnoncer_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.36504519771615) * _TEMP * _4_0;                          
  *--- Effect: TELOUVU_P ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.33723096194006) * _TEMP * _5_0;                          
  _LP0 = _LP0 + (     0.3580132646893) * _TEMP * _5_1;                          
  _LP0 = _LP0 + (   -0.18898282060975) * _TEMP * _5_2;                          
  *--- Effect: TOPENF18p ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.40572150961124) * _TEMP * _1_0;                          
  *--- Effect: tut_P ---;                                                       
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (      0.234573382016) * _TEMP * _11_0;                         
  *--- Effect: TXEFF_P ---;                                                     
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.25258379908258) * _TEMP * _12_0;                         
                                                                                
  *** Naive Posterior Probabilities;                                            
  drop _MAXP _IY _P0 _P1;                                                       
  _TEMP =      -0.569835713633 + _LP0;                                          
  if (_TEMP < 0) then do;                                                       
     _TEMP = exp(_TEMP);                                                        
     _P0 = _TEMP / (1 + _TEMP);                                                 
  end;                                                                          
  else _P0 = 1 / (1 + exp(-_TEMP));                                             
  _P1 = 1.0 - _P0;                                                              
                                                                                
  G1228:                                                                        
                                                                                
  *** Residuals;                                                                
  if (_Y = .) then do;                                                          
     R_CIBLEIndu6mois = .;                                                      
     R_CIBLEHorscible = .;                                                      
  end;                                                                          
  else do;                                                                      
      label R_CIBLEIndu6mois = 'Residual: CIBLE=Indu6mois' ;                    
      label R_CIBLEHorscible = 'Residual: CIBLE=Horscible' ;                    
     R_CIBLEIndu6mois = - _P0;                                                  
     R_CIBLEHorscible = - _P1;                                                  
     select( _Y );                                                              
        when (0)  R_CIBLEIndu6mois = R_CIBLEIndu6mois + 1;                      
        when (1)  R_CIBLEHorscible = R_CIBLEHorscible + 1;                      
     end;                                                                       
  end;                                                                          
                                                                                
                                                                                
  *** Update Posterior Probabilities;                                           
                                                                                
  *** Decision Processing;                                                      
  label D_CIBLE_ = 'Decision: CIBLE' ;                                          
  label EP_CIBLE_ = 'Expected Profit: CIBLE' ;                                  
  label BP_CIBLE_ = 'Best Profit: CIBLE' ;                                      
  label CP_CIBLE_ = 'Computed Profit: CIBLE' ;                                  
                                                                                
  length D_CIBLE_ $ 9;                                                          
                                                                                
  BP_CIBLE_ = .; CP_CIBLE_ = .;                                                 
                                                                                
  *** Compute Expected Consequences and Choose Decision;                        
  _decnum = 1; drop _decnum;                                                    
                                                                                
  D_CIBLE_ = 'Indu6mois' ;                                                      
  EP_CIBLE_ = _P0 * 1 + _P1 * 0;                                                
                                                                                
  *** Decision Matrix;                                                          
  array A68061 [2,1] _temporary_ (                                              
  /* row 1 */  1                                                                
  /* row 2 */  0                                                                
  );                                                                            
                                                                                
  *** Find Index of Target Category;                                            
  drop _tarnum; select( F_CIBLE );                                              
     when('INDU6MOIS' ) _tarnum = 1;                                            
     when('HORSCIBLE' ) _tarnum = 2;                                            
     otherwise _tarnum = 0;                                                     
  end;                                                                          
  if _tarnum <= 0 then goto G57213;                                             
                                                                                
  *** Computed Consequence of Chosen Decision;                                  
  CP_CIBLE_ = A68061 [_tarnum,_decnum];                                         
                                                                                
  *** Best Possible Consequence of Any Decision without Cost;                   
  array A92596 [2] _temporary_ ( 1 0);                                          
  BP_CIBLE_ = A92596 [_tarnum];                                                 
                                                                                
                                                                                
  G57213:;                                                                      
  *** End Decision Processing;                                                  
                                                                                
  *** Posterior Probabilities and Predicted Level;                              
  label SCOREGLOB =  'RISQUE INDU GLOBAL';                       
  label P_CIBLEHorscible = 'Predicted: CIBLE=Horscible' ;                       
  SCOREGLOB = _P0;                                                       
  _MAXP = _P0;                                                                  
  _IY = 1;                                                                      
  P_CIBLEHorscible = _P1;                                                       
  if (_P1 - _MAXP > 1e-8) then do;                                              
     _MAXP = _P1;                                                               
     _IY = 2;                                                                   
  end;                                                                          
  I_CIBLE = A5316[_IY];                                                         
  U_CIBLE = A7202[_IY];                                                         
                                                                                

  
  RUN  ; 
  QUIT ; 
%mend;

  
  %macro SCORELOG(tablin=,tablout=,garde= );
  DATA &tablout (keep=  &garde ) ; SET &tablin ;

  length _WARN_ $4;                                                             
  label _WARN_ = 'Warnings' ;                                                   
                                                                                
  length I_CIBLE $ 9;                                                           
  label I_CIBLE = 'Into: CIBLE' ;                                               
  *** Target Values;                                                            
  array A6277[2] $ 9 _temporary_ ('INDU6MOIS'  'HORSCIBLE' );                   
  label U_CIBLE = 'Unnormalized Into: CIBLE' ;                                  
  length U_CIBLE $ 9;                                                           
  *** Unnormalized target values;                                               
  array A1623[2] $ 9 _temporary_ ('Indu6mois'  'Horscible' );                   
                                                                                
  *** Generate dummy variables for CIBLE ;                                      
  drop _Y ;                                                                     
  label F_CIBLE = 'From: CIBLE' ;                                               
  length F_CIBLE $ 9;                                                           
  %DMNORMCP( CIBLE , F_CIBLE )                                                  
  if missing( CIBLE ) then do;                                                  
     _Y = .;                                                                    
  end;                                                                          
  else do;                                                                      
     if F_CIBLE = 'HORSCIBLE'  then do;                                         
        _Y = 1;                                                                 
     end;                                                                       
     else if F_CIBLE = 'INDU6MOIS'  then do;                                    
        _Y = 0;                                                                 
     end;                                                                       
     else do;                                                                   
        _Y = .;                                                                 
     end;                                                                       
  end;                                                                          
                                                                                
  drop _DM_BAD;                                                                 
  _DM_BAD=0;                                                                    
                               
                                                                                
  *** Generate dummy variables for MOIMODNEU_P ;                                
  drop _3_0 ;                                                                   
  if missing( MOIMODNEU_P ) then do;                                            
     _3_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm7 $ 7; drop _dm7 ;                                               
     %DMNORMCP( MOIMODNEU_P , _dm7 )                                            
     if _dm7 = '0A3'  then do;                                                  
        _3_0 = 1;                                                               
     end;                                                                       
     else if _dm7 = '4ETPLUS'  then do;                                         
        _3_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _3_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TXEFF_P ;                                    
  drop _4_0 ;                                                                   
  if missing( TXEFF_P ) then do;                                                
     _4_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm15 $ 15; drop _dm15 ;                                            
     %DMNORMCP( TXEFF_P , _dm15 )                                               
     if _dm15 = '0ALETTXEFF 0A35'  then do;                                     
        _4_0 = 1;                                                               
     end;                                                                       
     else if _dm15 = '35ETPLUS'  then do;                                       
        _4_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _4_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for agebenj_f ;                                  
  drop _5_0 _5_1 ;                                                              
  if missing( agebenj_f ) then do;                                              
     _5_0 = .;                                                                  
     _5_1 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     %DMNORMCP( agebenj_f , _dm12 )                                             
     if _dm12 = 'AGEBENJ 0-18'  then do;                                        
        _5_0 = 1;                                                               
        _5_1 = 0;                                                               
     end;                                                                       
     else if _dm12 = 'AGEBENJ-NC'  then do;                                     
        _5_0 = -1;                                                              
        _5_1 = -1;                                                              
     end;                                                                       
     else if _dm12 = 'AGEBENJ 19+'  then do;                                    
        _5_0 = 0;                                                               
        _5_1 = 1;                                                               
     end;                                                                       
     else do;                                                                   
        _5_0 = .;                                                               
        _5_1 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for ABANEU ;                                     
  drop _6_0 ;                                                                   
  if missing( ABANEU ) then do;                                                 
     _6_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm15 $ 15; drop _dm15 ;                                            
     %DMNORMCP( ABANEU , _dm15 )                                                
     if _dm15 = 'AUCUN'  then do;                                               
        _6_0 = -1;                                                              
     end;                                                                       
     else if _dm15 = 'AB. OU NEUTRAL.'  then do;                                
        _6_0 = 1;                                                               
     end;                                                                       
     else do;                                                                   
        _6_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                        
                                                                                
  *** Generate dummy variables for resaltdo_P ;                                 
  drop _8_0 ;                                                                   
  if missing( resaltdo_P ) then do;                                             
     _8_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm10 $ 10; drop _dm10 ;                                            
     %DMNORMCP( resaltdo_P , _dm10 )                                            
     if _dm10 = 'RESALT_AUT'  then do;                                          
        _8_0 = 1;                                                               
     end;                                                                       
     else if _dm10 = 'RESALT_PF'  then do;                                      
        _8_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _8_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TOPENF611_P ;                                
  drop _9_0 ;                                                                   
  if missing( TOPENF611_P ) then do;                                            
     _9_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm1 $ 1; drop _dm1 ;                                               
     %DMNORMCP( TOPENF611_P , _dm1 )                                            
     if _dm1 = '0'  then do;                                                    
        _9_0 = 1;                                                               
     end;                                                                       
     else if _dm1 = '1'  then do;                                               
        _9_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _9_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for FGTOTMOI_P ;                                 
  drop _10_0 _10_1 _10_2 ;                                                      
  if missing( FGTOTMOI_P ) then do;                                             
     _10_0 = .;                                                                 
     _10_1 = .;                                                                 
     _10_2 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm9 $ 9; drop _dm9 ;                                               
     %DMNORMCP( FGTOTMOI_P , _dm9 )                                             
     if _dm9 = '0A0.3'  then do;                                                
        _10_0 = 0;                                                              
        _10_1 = 0;                                                              
        _10_2 = 1;                                                              
     end;                                                                       
     else if _dm9 = '0.7A1.4'  then do;                                         
        _10_0 = 0;                                                              
        _10_1 = 1;                                                              
        _10_2 = 0;                                                              
     end;                                                                       
     else if _dm9 = '0.3A0.7'  then do;                                         
        _10_0 = 1;                                                              
        _10_1 = 0;                                                              
        _10_2 = 0;                                                              
     end;                                                                       
     else if _dm9 = '1.5ETPLUS'  then do;                                       
        _10_0 = -1;                                                             
        _10_1 = -1;                                                             
        _10_2 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _10_0 = .;                                                              
        _10_1 = .;                                                              
        _10_2 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for topfgresall_P ;                              
  drop _11_0 ;                                                                  
  if missing( topfgresall_P ) then do;                                          
     _11_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     _dm12 = put( topfgresall_P , BEST12. );                                    
     %DMNORMIP( _dm12 )                                                         
     if _dm12 = '0'  then do;                                                   
        _11_0 = 1;                                                              
     end;                                                                       
     else if _dm12 = '1'  then do;                                              
        _11_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _11_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MOIADRESS_P ;                                
  drop _12_0 ;                                                                  
  if missing( MOIADRESS_P ) then do;                                            
     _12_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm9 $ 9; drop _dm9 ;                                               
     %DMNORMCP( MOIADRESS_P , _dm9 )                                            
     if _dm9 = '0A10'  then do;                                                 
        _12_0 = 1;                                                              
     end;                                                                       
     else if _dm9 = '11ET PLUS'  then do;                                       
        _12_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _12_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** If missing inputs, use averages;                                          
  if _DM_BAD > 0 then do;                                                       
     _P0 = 0.0942021187;                                                        
     _P1 = 0.9057978813;                                                        
     goto G8342;                                                                
  end;                                                                          
                                                                                
  *** Compute Linear Predictor;                                                 
  drop _TEMP;                                                                   
  drop _LP0;                                                                    
  _LP0 = 0;                                                                     
                     
  *--- Effect: MOIMODNEU_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.42387701903304) * _TEMP * _3_0;                          
  *--- Effect: TXEFF_P ---;                                                     
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.05932482475679) * _TEMP * _4_0;                          
  *--- Effect: agebenj_f ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.57810783763145) * _TEMP * _5_0;                          
  _LP0 = _LP0 + (    0.32128725965117) * _TEMP * _5_1;                          
  *--- Effect: ABANEU ---;                                                      
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.52726269589625) * _TEMP * _6_0;                          
                    
  *--- Effect: resaltdo_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (    -1.0313262598656) * _TEMP * _8_0;                          
  *--- Effect: TOPENF611_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.60538962568603) * _TEMP * _9_0;                          
  *--- Effect: FGTOTMOI_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.43692458399686) * _TEMP * _10_0;                         
  _LP0 = _LP0 + (   -0.08010313469363) * _TEMP * _10_1;                         
  _LP0 = _LP0 + (    0.84419151680008) * _TEMP * _10_2;                         
  *--- Effect: topfgresall_P ---;                                               
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.23888467694997) * _TEMP * _11_0;                         
  *--- Effect: MOIADRESS_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.19851707774159) * _TEMP * _12_0;                         
                                                                                
  *** Naive Posterior Probabilities;                                            
  drop _MAXP _IY _P0 _P1;                                                       
  _TEMP =     0.40286791368865 + _LP0;                                          
  if (_TEMP < 0) then do;                                                       
     _TEMP = exp(_TEMP);                                                        
     _P0 = _TEMP / (1 + _TEMP);                                                 
  end;                                                                          
  else _P0 = 1 / (1 + exp(-_TEMP));                                             
  _P1 = 1.0 - _P0;                                                              
                                                                                
  G8342:                                                                        
                                                                                
  *** Residuals;                                                                
  if (_Y = .) then do;                                                          
     R_CIBLEIndu6mois = .;                                                      
     R_CIBLEHorscible = .;                                                      
  end;                                                                          
  else do;                                                                      
      label R_CIBLEIndu6mois = 'Residual: CIBLE=Indu6mois' ;                    
      label R_CIBLEHorscible = 'Residual: CIBLE=Horscible' ;                    
     R_CIBLEIndu6mois = - _P0;                                                  
     R_CIBLEHorscible = - _P1;                                                  
     select( _Y );                                                              
        when (0)  R_CIBLEIndu6mois = R_CIBLEIndu6mois + 1;                      
        when (1)  R_CIBLEHorscible = R_CIBLEHorscible + 1;                      
     end;                                                                       
  end;                                                                          
                                                                                
                                                                                
  *** Update Posterior Probabilities;                                           
                                                                                
  *** Decision Processing;                                                      
  label D_CIBLE_ = 'Decision: CIBLE' ;                                          
  label EP_CIBLE_ = 'Expected Profit: CIBLE' ;                                  
  label BP_CIBLE_ = 'Best Profit: CIBLE' ;                                      
  label CP_CIBLE_ = 'Computed Profit: CIBLE' ;                                  
                                                                                
  length D_CIBLE_ $ 9;                                                          
                                                                                
  BP_CIBLE_ = .; CP_CIBLE_ = .;                                                 
                                                                                
  *** Compute Expected Consequences and Choose Decision;                        
  _decnum = 1; drop _decnum;                                                    
                                                                                
  D_CIBLE_ = 'Indu6mois' ;                                                      
  EP_CIBLE_ = _P0 * 1 + _P1 * 0;                                                
                                                                                
  *** Decision Matrix;                                                          
  array A66331 [2,1] _temporary_ (                                              
  /* row 1 */  1                                                                
  /* row 2 */  0                                                                
  );                                                                            
                                                                                
  *** Find Index of Target Category;                                            
  drop _tarnum; select( F_CIBLE );                                              
     when('INDU6MOIS' ) _tarnum = 1;                                            
     when('HORSCIBLE' ) _tarnum = 2;                                            
     otherwise _tarnum = 0;                                                     
  end;                                                                          
  if _tarnum <= 0 then goto G45972;                                             
                                                                                
  *** Computed Consequence of Chosen Decision;                                  
  CP_CIBLE_ = A66331 [_tarnum,_decnum];                                         
                                                                                
  *** Best Possible Consequence of Any Decision without Cost;                   
  array A64112 [2] _temporary_ ( 1 0);                                          
  BP_CIBLE_ = A64112 [_tarnum];                                                 
                                                                                
                                                                                
  G45972:;                                                                      
  *** End Decision Processing;                                                  
                                                                                
  *** Posterior Probabilities and Predicted Level;                              
  label SCORELOG =  'RISQUE INDU LIE A MODIFICATION INFO LOGEMENT';                     
  label P_CIBLEHorscible = 'Predicted: CIBLE=Horscible' ;                       
  SCORELOG = _P0;                                                       
  _MAXP = _P0;                                                                  
  _IY = 1;                                                                      
  P_CIBLEHorscible = _P1;                                                       
  if (_P1 - _MAXP > 1e-8) then do;                                              
     _MAXP = _P1;                                                               
     _IY = 2;                                                                   
  end;                                                                          
  I_CIBLE = A6277[_IY];                                                         
  U_CIBLE = A1623[_IY];                                                         
                                                                                
  
  RUN  ; 
  QUIT ; 
  %mend;


  %macro SCORESS(tablin=,tablout=,garde= );
  DATA &tablout (keep=  &garde ) ; SET &tablin ;
  

  length _WARN_ $4;                                                             
  label _WARN_ = 'Warnings' ;                                                   
                                                                                
  length I_CIBLE $ 9;                                                           
  label I_CIBLE = 'Into: CIBLE' ;                                               
  *** Target Values;                                                            
  array A2014[2] $ 9 _temporary_ ('INDU6MOIS'  'HORSCIBLE' );                   
  label U_CIBLE = 'Unnormalized Into: CIBLE' ;                                  
  length U_CIBLE $ 9;                                                           
  *** Unnormalized target values;                                               
  array A9906[2] $ 9 _temporary_ ('Indu6mois'  'Horscible' );                   
                                                                                
  *** Generate dummy variables for CIBLE ;                                      
  drop _Y ;                                                                     
  label F_CIBLE = 'From: CIBLE' ;                                               
  length F_CIBLE $ 9;                                                           
  %DMNORMCP( CIBLE , F_CIBLE )                                                  
  if missing( CIBLE ) then do;                                                  
     _Y = .;                                                                    
  end;                                                                          
  else do;                                                                      
     if F_CIBLE = 'HORSCIBLE'  then do;                                         
        _Y = 1;                                                                 
     end;                                                                       
     else if F_CIBLE = 'INDU6MOIS'  then do;                                    
        _Y = 0;                                                                 
     end;                                                                       
     else do;                                                                   
        _Y = .;                                                                 
     end;                                                                       
  end;                                                                          
                                                                                
  drop _DM_BAD;                                                                 
  _DM_BAD=0;                                                                    
                                                                                
                                                                              
  *** Generate dummy variables for TOPENF18p ;                                  
  drop _1_0 ;                                                                   
  if missing( TOPENF18p ) then do;                                              
     _1_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm1 $ 1; drop _dm1 ;                                               
     %DMNORMCP( TOPENF18p , _dm1 )                                              
     if _dm1 = '0'  then do;                                                    
        _1_0 = 1;                                                               
     end;                                                                       
     else if _dm1 = '1'  then do;                                               
        _1_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _1_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                       
  *** Generate dummy variables for nirnoncer_P ;                                
  drop _4_0 ;                                                                   
  if missing( nirnoncer_P ) then do;                                            
     _4_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     %DMNORMCP( nirnoncer_P , _dm12 )                                           
     if _dm12 = '0 NIRNONCER'  then do;                                         
        _4_0 = 1;                                                               
     end;                                                                       
     else if _dm12 = '>=1 NIRNOCER'  then do;                                   
        _4_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _4_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TELOUVU_P ;                                  
  drop _5_0 _5_1 _5_2 ;                                                         
  if missing( TELOUVU_P ) then do;                                              
     _5_0 = .;                                                                  
     _5_1 = .;                                                                  
     _5_2 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm8 $ 8; drop _dm8 ;                                               
     %DMNORMCP( TELOUVU_P , _dm8 )                                              
     if _dm8 = '0A2'  then do;                                                  
        _5_0 = 1;                                                               
        _5_1 = 0;                                                               
        _5_2 = 0;                                                               
     end;                                                                       
     else if _dm8 = '3A7'  then do;                                             
        _5_0 = 0;                                                               
        _5_1 = 0;                                                               
        _5_2 = 1;                                                               
     end;                                                                       
     else if _dm8 = '8A15'  then do;                                            
        _5_0 = -1;                                                              
        _5_1 = -1;                                                              
        _5_2 = -1;                                                              
     end;                                                                       
     else if _dm8 = '15ETPLUS'  then do;                                        
        _5_0 = 0;                                                               
        _5_1 = 1;                                                               
        _5_2 = 0;                                                               
     end;                                                                       
     else do;                                                                   
        _5_0 = .;                                                               
        _5_1 = .;                                                               
        _5_2 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MODPAIDO_P ;                                 
  drop _6_0 ;                                                                   
  if missing( MODPAIDO_P ) then do;                                             
     _6_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm5 $ 5; drop _dm5 ;                                               
     %DMNORMCP( MODPAIDO_P , _dm5 )                                             
     if _dm5 = 'VIR'  then do;                                                  
        _6_0 = 1;                                                               
     end;                                                                       
     else if _dm5 = 'AUTRE'  then do;                                           
        _6_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _6_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
                                              
                                                                                
  *** Generate dummy variables for MOIMODNEU_P ;                                
  drop _8_0 ;                                                                   
  if missing( MOIMODNEU_P ) then do;                                            
     _8_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm7 $ 7; drop _dm7 ;                                               
     %DMNORMCP( MOIMODNEU_P , _dm7 )                                            
     if _dm7 = '0A3'  then do;                                                  
        _8_0 = 1;                                                               
     end;                                                                       
     else if _dm7 = '4ETPLUS'  then do;                                         
        _8_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _8_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TXEFF_P ;                                    
  drop _9_0 ;                                                                   
  if missing( TXEFF_P ) then do;                                                
     _9_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm15 $ 15; drop _dm15 ;                                            
     %DMNORMCP( TXEFF_P , _dm15 )                                               
     if _dm15 = '0ALETTXEFF 0A35'  then do;                                     
        _9_0 = 1;                                                               
     end;                                                                       
     else if _dm15 = '35ETPLUS'  then do;                                       
        _9_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _9_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                         
                                                     
                                                                                
  *** Generate dummy variables for MTPREST_P ;                                  
  drop _11_0 _11_1 _11_2 ;                                                      
  if missing( MTPREST_P ) then do;                                              
     _11_0 = .;                                                                 
     _11_1 = .;                                                                 
     _11_2 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm10 $ 10; drop _dm10 ;                                            
     %DMNORMCP( MTPREST_P , _dm10 )                                             
     if _dm10 = 'DE1A300'  then do;                                             
        _11_0 = 1;                                                              
        _11_1 = 0;                                                              
        _11_2 = 0;                                                              
     end;                                                                       
     else if _dm10 = 'DE300A700'  then do;                                      
        _11_0 = 0;                                                              
        _11_1 = 1;                                                              
        _11_2 = 0;                                                              
     end;                                                                       
     else if _dm10 = 'DE700A1200'  then do;                                     
        _11_0 = 0;                                                              
        _11_1 = 0;                                                              
        _11_2 = 1;                                                              
     end;                                                                       
     else if _dm10 = 'PLUSDE1200'  then do;                                     
        _11_0 = -1;                                                             
        _11_1 = -1;                                                             
        _11_2 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _11_0 = .;                                                              
        _11_1 = .;                                                              
        _11_2 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for ACTRESCON_P ;                                
  drop _12_0 _12_1 ;                                                            
  if missing( ACTRESCON_P ) then do;                                            
     _12_0 = .;                                                                 
     _12_1 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     %DMNORMCP( ACTRESCON_P , _dm12 )                                           
     if _dm12 = 'AUTRES CAS'  then do;                                          
        _12_0 = -1;                                                             
        _12_1 = -1;                                                             
     end;                                                                       
     else if _dm12 = '2 ACTIVITES'  then do;                                    
        _12_0 = 1;                                                              
        _12_1 = 0;                                                              
     end;                                                                       
     else if _dm12 = '2 DIFFICULTE'  then do;                                   
        _12_0 = 0;                                                              
        _12_1 = 1;                                                              
     end;                                                                       
     else do;                                                                   
        _12_0 = .;                                                              
        _12_1 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TYPCALAL_P ;                                 
  drop _13_0 _13_1 ;                                                            
  if missing( TYPCALAL_P ) then do;                                             
     _13_0 = .;                                                                 
     _13_1 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     %DMNORMCP( TYPCALAL_P , _dm12 )                                            
     if _dm12 = 'CALC SYS'  then do;                                            
        _13_0 = 1;                                                              
        _13_1 = 0;                                                              
     end;                                                                       
     else if _dm12 = 'ASSL_PASDAL'  then do;                                    
        _13_0 = 0;                                                              
        _13_1 = 1;                                                              
     end;                                                                       
     else if _dm12 = 'AUTRE TYPCAL'  then do;                                   
        _13_0 = -1;                                                             
        _13_1 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _13_0 = .;                                                              
        _13_1 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** If missing inputs, use averages;                                          
  if _DM_BAD > 0 then do;                                                       
     _P0 = 0.0676781754;                                                        
     _P1 = 0.9323218246;                                                        
     goto G4320;                                                                
  end;                                                                          
                                                                                
  *** Compute Linear Predictor;                                                 
  drop _TEMP;                                                                   
  drop _LP0;                                                                    
  _LP0 = 0;                                                                     
                
 
  *--- Effect: MODPAIDO_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.15388342168562) * _TEMP * _6_0;                          
  *--- Effect: MOIMODNEU_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.17381952175287) * _TEMP * _8_0;                          
  *--- Effect: nirnoncer_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.15813206491045) * _TEMP * _4_0;                          
  *--- Effect: TELOUVU_P ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.42627407281892) * _TEMP * _5_0;                          
  _LP0 = _LP0 + (    0.10504089343795) * _TEMP * _5_1;                          
  _LP0 = _LP0 + (   -0.20875157311172) * _TEMP * _5_2;                          
  *--- Effect: TOPENF18p ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.37039957094606) * _TEMP * _1_0;                          
  *--- Effect: TXEFF_P ---;                                                     
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.24629933279141) * _TEMP * _9_0;                          
                     
  *--- Effect: MTPREST_P ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.10001794179347) * _TEMP * _11_0;                         
  _LP0 = _LP0 + (    0.40810817913216) * _TEMP * _11_1;                         
  _LP0 = _LP0 + (   -0.10344364153264) * _TEMP * _11_2;                         
  *--- Effect: ACTRESCON_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.62830499681988) * _TEMP * _12_0;                         
  _LP0 = _LP0 + (    0.44586403819812) * _TEMP * _12_1;                         
  *--- Effect: TYPCALAL_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (    0.12872073028143) * _TEMP * _13_0;                         
  _LP0 = _LP0 + (   -0.30553215740408) * _TEMP * _13_1;                         
                                                                                
  *** Naive Posterior Probabilities;                                            
  drop _MAXP _IY _P0 _P1;                                                       
  _TEMP =     -0.6143711690419 + _LP0;                                          
  if (_TEMP < 0) then do;                                                       
     _TEMP = exp(_TEMP);                                                        
     _P0 = _TEMP / (1 + _TEMP);                                                 
  end;                                                                          
  else _P0 = 1 / (1 + exp(-_TEMP));                                             
  _P1 = 1.0 - _P0;                                                              
                                                                                
  G4320:                                                                        
                                                                                
  *** Residuals;                                                                
  if (_Y = .) then do;                                                          
     R_CIBLEIndu6mois = .;                                                      
     R_CIBLEHorscible = .;                                                      
  end;                                                                          
  else do;                                                                      
      label R_CIBLEIndu6mois = 'Residual: CIBLE=Indu6mois' ;                    
      label R_CIBLEHorscible = 'Residual: CIBLE=Horscible' ;                    
     R_CIBLEIndu6mois = - _P0;                                                  
     R_CIBLEHorscible = - _P1;                                                  
     select( _Y );                                                              
        when (0)  R_CIBLEIndu6mois = R_CIBLEIndu6mois + 1;                      
        when (1)  R_CIBLEHorscible = R_CIBLEHorscible + 1;                      
     end;                                                                       
  end;                                                                          
                                                                                
                                                                                
  *** Update Posterior Probabilities;                                           
                                                                                
  *** Decision Processing;                                                      
  label D_CIBLE_ = 'Decision: CIBLE' ;                                          
  label EP_CIBLE_ = 'Expected Profit: CIBLE' ;                                  
  label BP_CIBLE_ = 'Best Profit: CIBLE' ;                                      
  label CP_CIBLE_ = 'Computed Profit: CIBLE' ;                                  
                                                                                
  length D_CIBLE_ $ 9;                                                          
                                                                                
  BP_CIBLE_ = .; CP_CIBLE_ = .;                                                 
                                                                                
  *** Compute Expected Consequences and Choose Decision;                        
  _decnum = 1; drop _decnum;                                                    
                                                                                
  D_CIBLE_ = 'Indu6mois' ;                                                      
  EP_CIBLE_ = _P0 * 1 + _P1 * 0;                                                
                                                                                
  *** Decision Matrix;                                                          
  array A24265 [2,1] _temporary_ (                                              
  /* row 1 */  1                                                                
  /* row 2 */  0                                                                
  );                                                                            
                                                                                
  *** Find Index of Target Category;                                            
  drop _tarnum; select( F_CIBLE );                                              
     when('INDU6MOIS' ) _tarnum = 1;                                            
     when('HORSCIBLE' ) _tarnum = 2;                                            
     otherwise _tarnum = 0;                                                     
  end;                                                                          
  if _tarnum <= 0 then goto G54272;                                             
                                                                                
  *** Computed Consequence of Chosen Decision;                                  
  CP_CIBLE_ = A24265 [_tarnum,_decnum];                                         
                                                                                
  *** Best Possible Consequence of Any Decision without Cost;                   
  array A66721 [2] _temporary_ ( 1 0);                                          
  BP_CIBLE_ = A66721 [_tarnum];                                                 
                                                                                
                                                                                
  G54272:;                                                                      
  *** End Decision Processing;                                                  
                                                                                
  *** Posterior Probabilities and Predicted Level;                              
  label SCORESS = 'RISQUE INDU LIE A MODIFICATION INFO RESSOURCES';                    
  label P_CIBLEHorscible = 'Predicted: CIBLE=Horscible' ;                       
  SCORESS = _P0;                                                       
  _MAXP = _P0;                                                                  
  _IY = 1;                                                                      
  P_CIBLEHorscible = _P1;                                                       
  if (_P1 - _MAXP > 1e-8) then do;                                              
     _MAXP = _P1;                                                               
     _IY = 2;                                                                   
  end;                                                                          
  I_CIBLE = A2014[_IY];                                                         
  U_CIBLE = A9906[_IY];                                                         
                                                                                 
  RUN  ; 
  QUIT ; 

  %mend;  
  
  %macro SCORESIT(tablin=,tablout=,garde= );
  DATA &tablout (keep=  &garde ) ; SET &tablin ;

  length _WARN_ $4;                                                             
  label _WARN_ = 'Warnings' ;                                                   
                                                                                
  length I_CIBLE $ 9;                                                           
  label I_CIBLE = 'Into: CIBLE' ;                                               
  *** Target Values;                                                            
  array A5724[2] $ 9 _temporary_ ('INDU6MOIS'  'HORSCIBLE' );                   
  label U_CIBLE = 'Unnormalized Into: CIBLE' ;                                  
  length U_CIBLE $ 9;                                                           
  *** Unnormalized target values;                                               
  array A4549[2] $ 9 _temporary_ ('Indu6mois'  'Horscible' );                   
                                                                                
  *** Generate dummy variables for CIBLE ;                                      
  drop _Y ;                                                                     
  label F_CIBLE = 'From: CIBLE' ;                                               
  length F_CIBLE $ 9;                                                           
  %DMNORMCP( CIBLE , F_CIBLE )                                                  
  if missing( CIBLE ) then do;                                                  
     _Y = .;                                                                    
  end;                                                                          
  else do;                                                                      
     if F_CIBLE = 'HORSCIBLE'  then do;                                         
        _Y = 1;                                                                 
     end;                                                                       
     else if F_CIBLE = 'INDU6MOIS'  then do;                                    
        _Y = 0;                                                                 
     end;                                                                       
     else do;                                                                   
        _Y = .;                                                                 
     end;                                                                       
  end;                                                                          
                                                                                
  drop _DM_BAD;                                                                 
  _DM_BAD=0;                                                                    
                                                                                
                                            
                                                                                
  *** Generate dummy variables for MOIONTA_P ;                                  
  drop _2_0 ;                                                                   
  if missing( MOIONTA_P ) then do;                                              
     _2_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm8 $ 8; drop _dm8 ;                                               
     %DMNORMCP( MOIONTA_P , _dm8 )                                              
     if _dm8 = '0A12'  then do;                                                 
        _2_0 = 1;                                                               
     end;                                                                       
     else if _dm8 = '13ETPLUS'  then do;                                        
        _2_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _2_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MODPAIDO_P ;                                 
  drop _3_0 ;                                                                   
  if missing( MODPAIDO_P ) then do;                                             
     _3_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm5 $ 5; drop _dm5 ;                                               
     %DMNORMCP( MODPAIDO_P , _dm5 )                                             
     if _dm5 = 'VIR'  then do;                                                  
        _3_0 = 1;                                                               
     end;                                                                       
     else if _dm5 = 'AUTRE'  then do;                                           
        _3_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _3_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MOICONTCAF_P ;                               
  drop _4_0 ;                                                                   
  if missing( MOICONTCAF_P ) then do;                                           
     _4_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm7 $ 7; drop _dm7 ;                                               
     %DMNORMCP( MOICONTCAF_P , _dm7 )                                           
     if _dm7 = '0A6'  then do;                                                  
        _4_0 = 1;                                                               
     end;                                                                       
     else if _dm7 = '7ETPLUS'  then do;                                         
        _4_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _4_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for tut_P ;                                      
  drop _5_0 ;                                                                   
  if missing( tut_P ) then do;                                                  
     _5_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm7 $ 7; drop _dm7 ;                                               
     %DMNORMCP( tut_P , _dm7 )                                                  
     if _dm7 = '0'  then do;                                                    
        _5_0 = 1;                                                               
     end;                                                                       
     else if _dm7 = 'TUTELLE'  then do;                                         
        _5_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _5_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TXEFF_P ;                                    
  drop _6_0 ;                                                                   
  if missing( TXEFF_P ) then do;                                                
     _6_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm15 $ 15; drop _dm15 ;                                            
     %DMNORMCP( TXEFF_P , _dm15 )                                               
     if _dm15 = '0ALETTXEFF 0A35'  then do;                                     
        _6_0 = 1;                                                               
     end;                                                                       
     else if _dm15 = '35ETPLUS'  then do;                                       
        _6_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _6_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for ageaine_f ;                                  
  drop _7_0 _7_1 ;                                                              
  if missing( ageaine_f ) then do;                                              
     _7_0 = .;                                                                  
     _7_1 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     %DMNORMCP( ageaine_f , _dm12 )                                             
     if _dm12 = 'AGEAINE-0-18'  then do;                                        
        _7_0 = 1;                                                               
        _7_1 = 0;                                                               
     end;                                                                       
     else if _dm12 = 'AGEAINE-NC'  then do;                                     
        _7_0 = -1;                                                              
        _7_1 = -1;                                                              
     end;                                                                       
     else if _dm12 = 'AGEAINE-19+'  then do;                                    
        _7_0 = 0;                                                               
        _7_1 = 1;                                                               
     end;                                                                       
     else do;                                                                   
        _7_0 = .;                                                               
        _7_1 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TOPENF35_P ;                                 
  drop _8_0 ;                                                                   
  if missing( TOPENF35_P ) then do;                                             
     _8_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm1 $ 1; drop _dm1 ;                                               
     %DMNORMCP( TOPENF35_P , _dm1 )                                             
     if _dm1 = '0'  then do;                                                    
        _8_0 = 1;                                                               
     end;                                                                       
     else if _dm1 = '1'  then do;                                               
        _8_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _8_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TOPENF611_P ;                                
  drop _9_0 ;                                                                   
  if missing( TOPENF611_P ) then do;                                            
     _9_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm1 $ 1; drop _dm1 ;                                               
     %DMNORMCP( TOPENF611_P , _dm1 )                                            
     if _dm1 = '0'  then do;                                                    
        _9_0 = 1;                                                               
     end;                                                                       
     else if _dm1 = '1'  then do;                                               
        _9_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _9_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for top16ssa_P ;                                 
  drop _10_0 ;                                                                  
  if missing( top16ssa_P ) then do;                                             
     _10_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     _dm12 = put( top16ssa_P , BEST12. );                                       
     %DMNORMIP( _dm12 )                                                         
     if _dm12 = '0'  then do;                                                   
        _10_0 = 1;                                                              
     end;                                                                       
     else if _dm12 = '1'  then do;                                              
        _10_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _10_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MOIINDFNPF_P ;                               
  drop _11_0 ;                                                                  
  if missing( MOIINDFNPF_P ) then do;                                           
     _11_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm8 $ 8; drop _dm8 ;                                               
     %DMNORMCP( MOIINDFNPF_P , _dm8 )                                           
     if _dm8 = '0A12'  then do;                                                 
        _11_0 = 1;                                                              
     end;                                                                       
     else if _dm8 = '13ETPLUS'  then do;                                        
        _11_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _11_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MOIMODACT_P ;                                
  drop _12_0 ;                                                                  
  if missing( MOIMODACT_P ) then do;                                            
     _12_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm11 $ 11; drop _dm11 ;                                            
     %DMNORMCP( MOIMODACT_P , _dm11 )                                           
     if _dm11 = '0A7'  then do;                                                 
        _12_0 = 1;                                                              
     end;                                                                       
     else if _dm11 = 'SUPERIEURA8'  then do;                                    
        _12_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _12_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for nbm_odms_P ;                                 
  drop _13_0 _13_1 _13_2 ;                                                      
  if missing( nbm_odms_P ) then do;                                             
     _13_0 = .;                                                                 
     _13_1 = .;                                                                 
     _13_2 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm11 $ 11; drop _dm11 ;                                            
     %DMNORMCP( nbm_odms_P , _dm11 )                                            
     if _dm11 = '0MS'  then do;                                                 
        _13_0 = 0;                                                              
        _13_1 = 1;                                                              
        _13_2 = 0;                                                              
     end;                                                                       
     else if _dm11 = 'PLUS DE8A20'  then do;                                    
        _13_0 = -1;                                                             
        _13_1 = -1;                                                             
        _13_2 = -1;                                                             
     end;                                                                       
     else if _dm11 = '1A7'  then do;                                            
        _13_0 = 0;                                                              
        _13_1 = 0;                                                              
        _13_2 = 1;                                                              
     end;                                                                       
     else if _dm11 = '7'  then do;                                              
        _13_0 = 1;                                                              
        _13_1 = 0;                                                              
        _13_2 = 0;                                                              
     end;                                                                       
     else do;                                                                   
        _13_0 = .;                                                              
        _13_1 = .;                                                              
        _13_2 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TOPSIT12_P ;                                 
  drop _14_0 ;                                                                  
  if missing( TOPSIT12_P ) then do;                                             
     _14_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     _dm12 = put( TOPSIT12_P , BEST12. );                                       
     %DMNORMIP( _dm12 )                                                         
     if _dm12 = '0'  then do;                                                   
        _14_0 = 1;                                                              
     end;                                                                       
     else if _dm12 = '1'  then do;                                              
        _14_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _14_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for ASFVERS_P ;                                  
  drop _15_0 _15_1 ;                                                            
  if missing( ASFVERS_P ) then do;                                              
     _15_0 = .;                                                                 
     _15_1 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm5 $ 5; drop _dm5 ;                                               
     %DMNORMCP( ASFVERS_P , _dm5 )                                              
     if _dm5 = '0ASF'  then do;                                                 
        _15_0 = 1;                                                              
        _15_1 = 0;                                                              
     end;                                                                       
     else if _dm5 = 'ASFNR'  then do;                                           
        _15_0 = 0;                                                              
        _15_1 = 1;                                                              
     end;                                                                       
     else if _dm5 = 'ASFR'  then do;                                            
        _15_0 = -1;                                                             
        _15_1 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _15_0 = .;                                                              
        _15_1 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** If missing inputs, use averages;                                          
  if _DM_BAD > 0 then do;                                                       
     _P0 = 0.0730183853;                                                        
     _P1 = 0.9269816147;                                                        
     goto G8919;                                                                
  end;                                                                          
                                                                                
  *** Compute Linear Predictor;                                                 
  drop _TEMP;                                                                   
  drop _LP0;                                                                    
  _LP0 = 0;                                                                     
                         
  *--- Effect: MODPAIDO_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.13125863309315) * _TEMP * _3_0;                          
  *--- Effect: MOICONTCAF_P ---;                                                
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.41357356147579) * _TEMP * _4_0;                          
  *--- Effect: MOIONTA_P ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.22388664724973) * _TEMP * _2_0;                          
  *--- Effect: tut_P ---;                                                       
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (    0.79386605090346) * _TEMP * _5_0;                          
  *--- Effect: TXEFF_P ---;                                                     
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.35612483676597) * _TEMP * _6_0;                          
  *--- Effect: ageaine_f ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (    0.00622458638143) * _TEMP * _7_0;                          
  _LP0 = _LP0 + (    1.12943383534444) * _TEMP * _7_1;                          
  *--- Effect: TOPENF35_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (     0.3300445904089) * _TEMP * _8_0;                          
  *--- Effect: TOPENF611_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.12793575801007) * _TEMP * _9_0;                          
  *--- Effect: top16ssa_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.22067978171533) * _TEMP * _10_0;                         
  *--- Effect: MOIINDFNPF_P ---;                                                
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (    -0.2526977767653) * _TEMP * _11_0;                         
  *--- Effect: MOIMODACT_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (    0.45381293835457) * _TEMP * _12_0;                         
  *--- Effect: nbm_odms_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -1.12549013271756) * _TEMP * _13_0;                         
  _LP0 = _LP0 + (   -0.12919174869877) * _TEMP * _13_1;                         
  _LP0 = _LP0 + (    0.74200415748111) * _TEMP * _13_2;                         
  *--- Effect: TOPSIT12_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.19934279622817) * _TEMP * _14_0;                         
  *--- Effect: ASFVERS_P ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.16357752520474) * _TEMP * _15_0;                         
  _LP0 = _LP0 + (   -0.46585236534958) * _TEMP * _15_1;                         
                                                                                
  *** Naive Posterior Probabilities;                                            
  drop _MAXP _IY _P0 _P1;                                                       
  _TEMP =    -2.44478776280353 + _LP0;                                          
  if (_TEMP < 0) then do;                                                       
     _TEMP = exp(_TEMP);                                                        
     _P0 = _TEMP / (1 + _TEMP);                                                 
  end;                                                                          
  else _P0 = 1 / (1 + exp(-_TEMP));                                             
  _P1 = 1.0 - _P0;                                                              
                                                                                
  G8919:                                                                        
                                                                                
  *** Residuals;                                                                
  if (_Y = .) then do;                                                          
     R_CIBLEIndu6mois = .;                                                      
     R_CIBLEHorscible = .;                                                      
  end;                                                                          
  else do;                                                                      
      label R_CIBLEIndu6mois = 'Residual: CIBLE=Indu6mois' ;                    
      label R_CIBLEHorscible = 'Residual: CIBLE=Horscible' ;                    
     R_CIBLEIndu6mois = - _P0;                                                  
     R_CIBLEHorscible = - _P1;                                                  
     select( _Y );                                                              
        when (0)  R_CIBLEIndu6mois = R_CIBLEIndu6mois + 1;                      
        when (1)  R_CIBLEHorscible = R_CIBLEHorscible + 1;                      
     end;                                                                       
  end;                                                                          
                                                                                
                                                                                
  *** Update Posterior Probabilities;                                           
                                                                                
  *** Decision Processing;                                                      
  label D_CIBLE_ = 'Decision: CIBLE' ;                                          
  label EP_CIBLE_ = 'Expected Profit: CIBLE' ;                                  
  label BP_CIBLE_ = 'Best Profit: CIBLE' ;                                      
  label CP_CIBLE_ = 'Computed Profit: CIBLE' ;                                  
                                                                                
  length D_CIBLE_ $ 9;                                                          
                                                                                
  BP_CIBLE_ = .; CP_CIBLE_ = .;                                                 
                                                                                
  *** Compute Expected Consequences and Choose Decision;                        
  _decnum = 1; drop _decnum;                                                    
                                                                                
  D_CIBLE_ = 'Indu6mois' ;                                                      
  EP_CIBLE_ = _P0 * 1 + _P1 * 0;                                                
                                                                                
  *** Decision Matrix;                                                          
  array A16066 [2,1] _temporary_ (                                              
  /* row 1 */  1                                                                
  /* row 2 */  0                                                                
  );                                                                            
                                                                                
  *** Find Index of Target Category;                                            
  drop _tarnum; select( F_CIBLE );                                              
     when('INDU6MOIS' ) _tarnum = 1;                                            
     when('HORSCIBLE' ) _tarnum = 2;                                            
     otherwise _tarnum = 0;                                                     
  end;                                                                          
  if _tarnum <= 0 then goto G55576;                                             
                                                                                
  *** Computed Consequence of Chosen Decision;                                  
  CP_CIBLE_ = A16066 [_tarnum,_decnum];                                         
                                                                                
  *** Best Possible Consequence of Any Decision without Cost;                   
  array A56617 [2] _temporary_ ( 1 0);                                          
  BP_CIBLE_ = A56617 [_tarnum];                                                 
                                                                                
                                                                                
  G55576:;                                                                      
  *** End Decision Processing;                                                  
                                                                                
  *** Posterior Probabilities and Predicted Level;                              
  label SCORESIT =  'RISQUE INDU LIE A MODIFICATION INFO SITFAM/CHARGENF';                   
  label P_CIBLEHorscible = 'Predicted: CIBLE=Horscible' ;                       
  SCORESIT = _P0;                                                       
  _MAXP = _P0;                                                                  
  _IY = 1;                                                                      
  P_CIBLEHorscible = _P1;                                                       
  if (_P1 - _MAXP > 1e-8) then do;                                              
     _MAXP = _P1;                                                               
     _IY = 2;                                                                   
  end;                                                                          
  I_CIBLE = A5724[_IY];                                                         
  U_CIBLE = A4549[_IY];                                                         

  
  RUN  ; 
  QUIT ; 
  %mend;


  %macro SCOREPRO(tablin=,tablout=,garde= );
  DATA &tablout (keep=  &garde ) ; SET &tablin ;

  length _WARN_ $4;                                                             
  label _WARN_ = 'Warnings' ;                                                   
                                                                                
  length I_CIBLE $ 9;                                                           
  label I_CIBLE = 'Into: CIBLE' ;                                               
  *** Target Values;                                                            
  array A5746[2] $ 9 _temporary_ ('INDU6MOIS'  'HORSCIBLE' );                   
  label U_CIBLE = 'Unnormalized Into: CIBLE' ;                                  
  length U_CIBLE $ 9;                                                           
  *** Unnormalized target values;                                               
  array A4532[2] $ 9 _temporary_ ('Indu6mois'  'Horscible' );                   
                                                                                
  *** Generate dummy variables for CIBLE ;                                      
  drop _Y ;                                                                     
  label F_CIBLE = 'From: CIBLE' ;                                               
  length F_CIBLE $ 9;                                                           
  %DMNORMCP( CIBLE , F_CIBLE )                                                  
  if missing( CIBLE ) then do;                                                  
     _Y = .;                                                                    
  end;                                                                          
  else do;                                                                      
     if F_CIBLE = 'HORSCIBLE'  then do;                                         
        _Y = 1;                                                                 
     end;                                                                       
     else if F_CIBLE = 'INDU6MOIS'  then do;                                    
        _Y = 0;                                                                 
     end;                                                                       
     else do;                                                                   
        _Y = .;                                                                 
     end;                                                                       
  end;                                                                          
                                                                                
  drop _DM_BAD;                                                                 
  _DM_BAD=0;                                                                    
                                                                                
                                                                               
  *** Generate dummy variables for TOPENF18p ;                                  
  drop _1_0 ;                                                                   
  if missing( TOPENF18p ) then do;                                              
     _1_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm1 $ 1; drop _dm1 ;                                               
     %DMNORMCP( TOPENF18p , _dm1 )                                              
     if _dm1 = '0'  then do;                                                    
        _1_0 = 1;                                                               
     end;                                                                       
     else if _dm1 = '1'  then do;                                               
        _1_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _1_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                                        
  *** Generate dummy variables for TELOUVU_P ;                                  
  drop _4_0 _4_1 _4_2 ;                                                         
  if missing( TELOUVU_P ) then do;                                              
     _4_0 = .;                                                                  
     _4_1 = .;                                                                  
     _4_2 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm8 $ 8; drop _dm8 ;                                               
     %DMNORMCP( TELOUVU_P , _dm8 )                                              
     if _dm8 = '0A2'  then do;                                                  
        _4_0 = 1;                                                               
        _4_1 = 0;                                                               
        _4_2 = 0;                                                               
     end;                                                                       
     else if _dm8 = '3A7'  then do;                                             
        _4_0 = 0;                                                               
        _4_1 = 0;                                                               
        _4_2 = 1;                                                               
     end;                                                                       
     else if _dm8 = '8A15'  then do;                                            
        _4_0 = -1;                                                              
        _4_1 = -1;                                                              
        _4_2 = -1;                                                              
     end;                                                                       
     else if _dm8 = '15ETPLUS'  then do;                                        
        _4_0 = 0;                                                               
        _4_1 = 1;                                                               
        _4_2 = 0;                                                               
     end;                                                                       
     else do;                                                                   
        _4_0 = .;                                                               
        _4_1 = .;                                                               
        _4_2 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MOIONTA_P ;                                  
  drop _5_0 ;                                                                   
  if missing( MOIONTA_P ) then do;                                              
     _5_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm8 $ 8; drop _dm8 ;                                               
     %DMNORMCP( MOIONTA_P , _dm8 )                                              
     if _dm8 = '0A12'  then do;                                                 
        _5_0 = 1;                                                               
     end;                                                                       
     else if _dm8 = '13ETPLUS'  then do;                                        
        _5_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _5_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for MOIMODNEU_P ;                                
  drop _6_0 ;                                                                   
  if missing( MOIMODNEU_P ) then do;                                            
     _6_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm7 $ 7; drop _dm7 ;                                               
     %DMNORMCP( MOIMODNEU_P , _dm7 )                                            
     if _dm7 = '0A3'  then do;                                                  
        _6_0 = 1;                                                               
     end;                                                                       
     else if _dm7 = '4ETPLUS'  then do;                                         
        _6_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _6_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TXEFF_P ;                                    
  drop _7_0 ;                                                                   
  if missing( TXEFF_P ) then do;                                                
     _7_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm15 $ 15; drop _dm15 ;                                            
     %DMNORMCP( TXEFF_P , _dm15 )                                               
     if _dm15 = '0ALETTXEFF 0A35'  then do;                                     
        _7_0 = 1;                                                               
     end;                                                                       
     else if _dm15 = '35ETPLUS'  then do;                                       
        _7_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _7_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for ABANEU ;                                     
  drop _8_0 ;                                                                   
  if missing( ABANEU ) then do;                                                 
     _8_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm15 $ 15; drop _dm15 ;                                            
     %DMNORMCP( ABANEU , _dm15 )                                                
     if _dm15 = 'AUCUN'  then do;                                               
        _8_0 = -1;                                                              
     end;                                                                       
     else if _dm15 = 'AB. OU NEUTRAL.'  then do;                                
        _8_0 = 1;                                                               
     end;                                                                       
     else do;                                                                   
        _8_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TOPENF1218_P ;                               
  drop _9_0 ;                                                                   
  if missing( TOPENF1218_P ) then do;                                           
     _9_0 = .;                                                                  
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm1 $ 1; drop _dm1 ;                                               
     %DMNORMCP( TOPENF1218_P , _dm1 )                                           
     if _dm1 = '0'  then do;                                                    
        _9_0 = 1;                                                               
     end;                                                                       
     else if _dm1 = '1'  then do;                                               
        _9_0 = -1;                                                              
     end;                                                                       
     else do;                                                                   
        _9_0 = .;                                                               
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TYPCALAL_P ;                                 
  drop _10_0 _10_1 ;                                                            
  if missing( TYPCALAL_P ) then do;                                             
     _10_0 = .;                                                                 
     _10_1 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     %DMNORMCP( TYPCALAL_P , _dm12 )                                            
     if _dm12 = 'CALC SYS'  then do;                                            
        _10_0 = 1;                                                              
        _10_1 = 0;                                                              
     end;                                                                       
     else if _dm12 = 'ASSL_PASDAL'  then do;                                    
        _10_0 = 0;                                                              
        _10_1 = 1;                                                              
     end;                                                                       
     else if _dm12 = 'AUTRE TYPCAL'  then do;                                   
        _10_0 = -1;                                                             
        _10_1 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _10_0 = .;                                                              
        _10_1 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for NMODACT12_P ;                                
  drop _11_0 _11_1 ;                                                            
  if missing( NMODACT12_P ) then do;                                            
     _11_0 = .;                                                                 
     _11_1 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm7 $ 7; drop _dm7 ;                                               
     %DMNORMCP( NMODACT12_P , _dm7 )                                            
     if _dm7 = '0'  then do;                                                    
        _11_0 = 1;                                                              
        _11_1 = 0;                                                              
     end;                                                                       
     else if _dm7 = '1A2'  then do;                                             
        _11_0 = 0;                                                              
        _11_1 = 1;                                                              
     end;                                                                       
     else if _dm7 = '3ETPLUS'  then do;                                         
        _11_0 = -1;                                                             
        _11_1 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _11_0 = .;                                                              
        _11_1 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** Generate dummy variables for TOPSIT12_P ;                                 
  drop _12_0 ;                                                                  
  if missing( TOPSIT12_P ) then do;                                             
     _12_0 = .;                                                                 
     substr(_warn_,1,1) = 'M';                                                  
     _DM_BAD = 1;                                                               
  end;                                                                          
  else do;                                                                      
     length _dm12 $ 12; drop _dm12 ;                                            
     _dm12 = put( TOPSIT12_P , BEST12. );                                       
     %DMNORMIP( _dm12 )                                                         
     if _dm12 = '0'  then do;                                                   
        _12_0 = 1;                                                              
     end;                                                                       
     else if _dm12 = '1'  then do;                                              
        _12_0 = -1;                                                             
     end;                                                                       
     else do;                                                                   
        _12_0 = .;                                                              
        substr(_warn_,2,1) = 'U';                                               
        _DM_BAD = 1;                                                            
     end;                                                                       
  end;                                                                          
                                                                                
  *** If missing inputs, use averages;                                          
  if _DM_BAD > 0 then do;                                                       
     _P0 = 0.0814661585;                                                        
     _P1 = 0.9185338415;                                                        
     goto G8870;                                                                
  end;                                                                          
                                                                                
  *** Compute Linear Predictor;                                                 
  drop _TEMP;                                                                   
  drop _LP0;                                                                    
  _LP0 = 0;                                                                     
                                          
  *--- Effect: MOIMODNEU_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.27415271901918) * _TEMP * _6_0;                          
  *--- Effect: MOIONTA_P ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.15352235514342) * _TEMP * _5_0;                          
  *--- Effect: TELOUVU_P ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.45598491841411) * _TEMP * _4_0;                          
  _LP0 = _LP0 + (    0.40951549630729) * _TEMP * _4_1;                          
  _LP0 = _LP0 + (   -0.38927544024691) * _TEMP * _4_2;                          
  *--- Effect: TOPENF18p ---;                                                   
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (    -0.2473999117591) * _TEMP * _1_0;                          
  *--- Effect: TXEFF_P ---;                                                     
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.20239963394374) * _TEMP * _7_0;                          
  *--- Effect: ABANEU ---;                                                      
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (    0.31716446806853) * _TEMP * _8_0;                          
  *--- Effect: TOPENF1218_P ---;                                                
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.25078162484385) * _TEMP * _9_0;                          
  *--- Effect: TYPCALAL_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.41820835721814) * _TEMP * _10_0;                         
  _LP0 = _LP0 + (   -0.14800144998501) * _TEMP * _10_1;                         
  *--- Effect: NMODACT12_P ---;                                                 
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (   -0.03238248642677) * _TEMP * _11_0;                         
  _LP0 = _LP0 + (   -0.21416325280767) * _TEMP * _11_1;                         
  *--- Effect: TOPSIT12_P ---;                                                  
  _TEMP = 1;                                                                    
  _LP0 = _LP0 + (    0.35205183778619) * _TEMP * _12_0;                         
                                                                                
  *** Naive Posterior Probabilities;                                            
  drop _MAXP _IY _P0 _P1;                                                       
  _TEMP =    -0.63675403304377 + _LP0;                                          
  if (_TEMP < 0) then do;                                                       
     _TEMP = exp(_TEMP);                                                        
     _P0 = _TEMP / (1 + _TEMP);                                                 
  end;                                                                          
  else _P0 = 1 / (1 + exp(-_TEMP));                                             
  _P1 = 1.0 - _P0;                                                              
                                                                                
  G8870:                                                                        
                                                                                
  *** Residuals;                                                                
  if (_Y = .) then do;                                                          
     R_CIBLEIndu6mois = .;                                                      
     R_CIBLEHorscible = .;                                                      
  end;                                                                          
  else do;                                                                      
      label R_CIBLEIndu6mois = 'Residual: CIBLE=Indu6mois' ;                    
      label R_CIBLEHorscible = 'Residual: CIBLE=Horscible' ;                    
     R_CIBLEIndu6mois = - _P0;                                                  
     R_CIBLEHorscible = - _P1;                                                  
     select( _Y );                                                              
        when (0)  R_CIBLEIndu6mois = R_CIBLEIndu6mois + 1;                      
        when (1)  R_CIBLEHorscible = R_CIBLEHorscible + 1;                      
     end;                                                                       
  end;                                                                          
                                                                                
                                                                                
  *** Update Posterior Probabilities;                                           
                                                                                
  *** Decision Processing;                                                      
  label D_CIBLE_ = 'Decision: CIBLE' ;                                          
  label EP_CIBLE_ = 'Expected Profit: CIBLE' ;                                  
  label BP_CIBLE_ = 'Best Profit: CIBLE' ;                                      
  label CP_CIBLE_ = 'Computed Profit: CIBLE' ;                                  
                                                                                
  length D_CIBLE_ $ 9;                                                          
                                                                                
  BP_CIBLE_ = .; CP_CIBLE_ = .;                                                 
                                                                                
  *** Compute Expected Consequences and Choose Decision;                        
  _decnum = 1; drop _decnum;                                                    
                                                                                
  D_CIBLE_ = 'Indu6mois' ;                                                      
  EP_CIBLE_ = _P0 * 1 + _P1 * 0;                                                
                                                                                
  *** Decision Matrix;                                                          
  array A33460 [2,1] _temporary_ (                                              
  /* row 1 */  1                                                                
  /* row 2 */  0                                                                
  );                                                                            
                                                                                
  *** Find Index of Target Category;                                            
  drop _tarnum; select( F_CIBLE );                                              
     when('INDU6MOIS' ) _tarnum = 1;                                            
     when('HORSCIBLE' ) _tarnum = 2;                                            
     otherwise _tarnum = 0;                                                     
  end;                                                                          
  if _tarnum <= 0 then goto G34057;                                             
                                                                                
  *** Computed Consequence of Chosen Decision;                                  
  CP_CIBLE_ = A33460 [_tarnum,_decnum];                                         
                                                                                
  *** Best Possible Consequence of Any Decision without Cost;                   
  array A76086 [2] _temporary_ ( 1 0);                                          
  BP_CIBLE_ = A76086 [_tarnum];                                                 
                                                                                
                                                                                
  G34057:;                                                                      
  *** End Decision Processing;                                                  
                                                                                
  *** Posterior Probabilities and Predicted Level;                              
  label SCOREPRO =  'RISQUE INDU LIE A MODIFICATION SITUATION PRO';                  
  label P_CIBLEHorscible = 'Predicted: CIBLE=Horscible' ;                       
  SCOREPRO = _P0;                                                       
  _MAXP = _P0;                                                                  
  _IY = 1;                                                                      
  P_CIBLEHorscible = _P1;                                                       
  if (_P1 - _MAXP > 1e-8) then do;                                              
     _MAXP = _P1;                                                               
     _IY = 2;                                                                   
  end;                                                                          
  I_CIBLE = A5746[_IY];                                                         
  U_CIBLE = A4532[_IY];                                                         
                                                                                
  RUN  ; 
  QUIT ; 
%mend;


/************************************************************************************************/
/************************************************************************************************/
/**************				PARTIE VI- CREATION DES TABLES DE SCORE					*************/
/************************************************************************************************/
/************************************************************************************************/


/*SCORING PAR APPLICATION DES MACRO-PROGRAMMES DE SCORING*/
/*SCOREGLOB SCORESIT SCOREPRO SCORELOG SCORESS*/
%SCOREGLOB	(tablin	=datamining&mvMOISFRM.&mvANNEEFRM,tablout=modeles_typerisque,garde=NORDALLC NUMCAF MATRICUL SCORE:  &var_p);
%SCORELOG	(tablin	=modeles_typerisque,tablout=modeles_typerisque,garde=NORDALLC NUMCAF MATRICUL SCORE:  &var_p);
%SCORESS	(tablin	=modeles_typerisque,tablout=modeles_typerisque,garde=NORDALLC NUMCAF MATRICUL SCORE:  &var_p);
%SCORESIT	(tablin	=modeles_typerisque,tablout=modeles_typerisque,garde=NORDALLC NUMCAF MATRICUL SCORE:  &var_p);
%SCOREPRO	(tablin	=modeles_typerisque,tablout=modeles_typerisque,garde=NORDALLC NUMCAF MATRICUL SCORE:  &var_p);
/*SCORING PAR LE MODELE MAX DES SCORES PAR FAMILLE*/
proc sort data=modeles_typerisque;
by matricul; run;

/*Merge des diff�rents Scoring*/
data baseloc.RISQUINDU&mvMOISFRM.&mvANNEEFRM(keep=NORDALLC NUMCAF MATRICUL SCORE: &var_p &var_ab);
retain NORDALLC NUMCAF MATRICUL SCOREMAX SCOREGLOB SCORESIT SCOREPRO SCORELOG SCORESS;
merge 	modeles_typerisque
		datamining&mvMOISFRM.&mvANNEEFRM;
by matricul;
SCOREMAX=max(SCOREGLOB, SCORESIT, SCOREPRO, SCORELOG, SCORESS);
run;

proc sort data=Baseloc.RISQUINDU&mvMOISFRM.&mvANNEEFRM;
by descending score ;
run;
